/*
 *   Project: The SPD Image correction and azimuthal regrouping
 *			http://forge.epn-campus.eu/projects/show/azimuthal
 *
 *   Copyright (C) 2001-2010 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: R. Wilcke (wilcke@esrf.fr)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

/*
Update 12/10/2011 P. Boesecke (boesecke@esrf.fr)
                  print_memlist() added
Update 30/09/2009 R. Wilcke (wilcke@esrf.fr)
                  scan_argument(): for "%s" format, return an empty string
                  buffer if the input argument is empty (left the "result"
                  argument unchanged before).
Update 24/04/2009 R. Wilcke (wilcke@esrf.fr)
                  outname(): increase size of output name buffer from 512 to
                  1024 characters;
                  outname(): duplicate result buffer before passing it to the
                  "basename()" routine (the routine can change its argument);
                  outname(): add new input argument "outdir" to the argument
                  list and code to add it in front of the output file name.
Update 21/01/2008 R. Wilcke (wilcke@esrf.fr)
                  _pmalloc(), _prealloc() and _pfree(): slight modifications of
                  error messages.
Update 11/12/2007 R. Wilcke (wilcke@esrf.fr)
                  fnampat(): initialize filename buffer only after test for NULL
                  pointer.
Update 06/12/2007 R. Wilcke (wilcke@esrf.fr)
                  add function fnampat().
Update 24/08/2004 R. Wilcke (wilcke@esrf.fr)
                  _pfree(), print_memsize(), byte_swap2N():
                  add "return(0)" at the end of the routine;
                  bench(): add "return(iret)" at the end of the routine;
                  isbigendian(): return -1 if type cannot be determined;
                  scan_argument(): add "u" format conversion and size modifiers
                  "h" and "l".
Update 23/08/2004 R. Wilcke (wilcke@esrf.fr)
                  scan_argument(): add "g" format conversion, write all input
                  arguments into a special history buffer.
Update 17/09/2002 R. Wilcke (wilcke@esrf.fr)
                  bench(): return error code of gettimeofday() or 0 if no error.
Update 16/09/2002 R. Wilcke (wilcke@esrf.fr)
                  reallocate the message levels for the prmsg() calls.
Update 20/11/2001 R. Wilcke (wilcke@esrf.fr)
                  outname(): handle the case when a name is an empty string.
Update 11/12/2001 R. Wilcke (wilcke@esrf.fr)
                  protect against taking strlen() from NULL pointer.
Update 20/11/2001 R. Wilcke (wilcke@esrf.fr)
                  outname(): protect against infile = NULL pointer.
                  outname(): handle the cases when "inext" or "outext" are NULL;
Update 08/11/2001 R. Wilcke (wilcke@esrf.fr)
                  split the code of "util.c" in two files:
                  - "inout.c" contains all input/output related routines,
                  - "util.c" contains the other routines of the old "util.c".
Update 09/08/2001 R. Wilcke (wilcke@esrf.fr)
                  declare struct *pmem_head as "static".
*/

#include "spd.h"

#if MY_MALLOC
/*==============================================================================
 * If MY_MALLOC is defined, an alternative set of routines for memory
 * management is used. The following routines are defined:
 *
 * - pmalloc(), prealloc(), pfree() replace the standard routines malloc(),
 *   realloc() and free();
 * - print_memsize() prints the total allocated memory size.
 *
 * The differences to the standard routines are:
 * - the alternative set provides error printout indicating the current file
 *   and line number where the error occurred;
 * - the alternative set operates on a linked list of data structures of type
 *   "pmem". Each structure contains the size of its data segment, a pointer to
 *   the next element in the list, and a pointer to its own data segment. This
 *   allows to keep track of the overall memory usage by the program with the
 *   routine print_memsize().
 *
 * Otherwise, the input parameters and return values of the routines in the
 * alternative set are the same as the ones in the standard set.
 *
 * If MY_MALLOC is not defined, pmalloc(), prealloc() and pfree() are defined
 * to be the standard set of memory management routines, and print_memsize()
 * is a dummy.
 */

static struct pmem *pmem_head = NULL;

void *_pmalloc(int size,char *file,int line)
{
  struct pmem *ptr;
  /*
   * Allocate the new memory segment, put the address of the last memory
   * segment in the link pointer, and save the address of the new memory
   * segment for use in the link pointer at the next call.
   */
  if((ptr = (struct pmem *)malloc(size + sizeof(struct pmem))) == NULL)
    __prmsg(FATAL,file,line,("cannot allocate %d bytes\n",size));
  ptr->next = pmem_head;
  pmem_head = ptr;
  ptr->data = (void *)(((char *)ptr) + sizeof(struct pmem));
  ptr->size = size;

  return ptr->data;
}

void *_prealloc(void *ptr,int size,char *file,int line)
{
  struct pmem *loop_ptr,*new_ptr,**ref_ptr;

  /*
   * Loop throuth the linked list until the requested data segment is found or
   * the end of the list is reached (then loop_ptr == NULL).
   */
  for(loop_ptr = pmem_head, ref_ptr = &pmem_head; loop_ptr;
    loop_ptr = *(ref_ptr = &loop_ptr->next))
    if(ptr == loop_ptr->data)
      break;
  if(loop_ptr == NULL)
    __prmsg(FATAL,file,line,("pointer was never allocated: 0x%x \n",ptr));

  /*
   * Reallocate the requested data segment.
   */
  if((new_ptr = (struct pmem *)
    realloc(loop_ptr,size + sizeof(struct pmem))) == NULL)
    __prmsg(FATAL,file,line,("cannot reallocate %d bytes \n",
      size + sizeof(struct pmem)));

  *ref_ptr = new_ptr;
  new_ptr->data  = (void*)(((char*)new_ptr) + sizeof(struct pmem));
  new_ptr->size = size;

  return(new_ptr->data);
}

int _pfree(void *ptr,char *file,int line)
{
  struct pmem *loop_ptr, **ref_ptr;
  /*
   * Loop through the linked list until the requested data segment is found or
   * the end of the list is reached (then loop_ptr == NULL).
   */

  for(loop_ptr = pmem_head, ref_ptr = &pmem_head; loop_ptr;
    loop_ptr = *(ref_ptr = &loop_ptr->next)) {
    if(ptr == loop_ptr->data)
      break;
  }
  if(loop_ptr == NULL)
    __prmsg(FATAL,file,line,("pointer was never allocated: 0x%x\n",ptr));
  /*
   * Free the requested data segment, and re-adjust the link pointer.
   */
  *ref_ptr = loop_ptr->next;
  free(loop_ptr);
  return(0);
}

/*==============================================================================
 * Print the total memory size of all allocated data segments.
 */
int print_memsize()
{
  struct pmem *loop_ptr;
  float sum = 0;

  for(loop_ptr = pmem_head; loop_ptr; loop_ptr = loop_ptr->next)
    sum += loop_ptr->size;
  prmsg(DMSG,("Memory usage is now: %7.2f MiB\n",sum / 1024 / 1024));
  return(0);
}

/*==============================================================================
 * Print the list of all allocated data segments.
 */
int print_memlist()
{
  struct pmem *loop_ptr;
  int loop_cnt;

  printf("================================================================================\n");

  for(loop_ptr=pmem_head,loop_cnt=1; loop_ptr; loop_ptr=loop_ptr->next,loop_cnt++)
  {
    printf(" memory segment %3d: %p\n",loop_cnt,loop_ptr);
    printf("           data %3s: %p\n"," ",loop_ptr->data);
    printf("           size %3s: %d\n"," ",loop_ptr->size);
    printf("           next %3s: %p\n"," ",loop_ptr->next);
    printf("--------------------------------------------------------------------------------\n");
  }
}
#else
int print_memsize()
{
  return(0);
}

int print_memlist()
{
  return(0);
}
#endif /* MY_MALLOC */

/*==============================================================================
 * Switch the byte order in the input array "s" containing "n" elements of type
 * short.
 *
 * Input : s: input data array, type short
 *         n: number of data items in the input data array
 * Output: s: input data array with bytes swapped
 * Return: 0
 */

union swap2_data {
  unsigned char  c[2];
  unsigned short x;
};

int byte_swap2N(register union swap2_data *s,int n)
{
  register int i;
  register unsigned char t;

  for(i = 0; i < n; i++, s++) {
    t = s->c[0];
    s->c[0] = s->c[1];
    s->c[1] = t;
  }
  return(0);
}

/*==============================================================================
 * Determines the minimum and the maximum of the four input values.
 *
 * Input : f1, f2, f3, f4: the four values to be searched for miminum and
 *                         maximum
 * Output: p_minf: the minimum of the four input values
 *         p_maxf: the maximum of the four input values
 * Return: 0
 */

int minmax4(float f1,float f2,float f3,float f4,float *p_minf,float *p_maxf)
{
  int i;
  float ft[3];
  float minf = f1;
  float maxf = f1;

  ft[0] = f2; ft[1] = f3; ft[2] = f4;
  for(i=0; i<3; i++) {
    if(minf > ft[i])
      minf = ft[i];
    if(maxf < ft[i])
      maxf = ft[i];
  }
  *p_minf = minf;
  *p_maxf = maxf;
  return(0);
}

/*==============================================================================
 * Obtains a parameter value from an input argument string.
 *
 * It searches the input argument "arg" for a string of the form
 *
 *   parameter_name=parameter_value
 *
 * where parameter_name is the string contained in the input argument "str".
 *
 * If found, it prints the parameter value according to the format given in the
 * input argument "format" and returns the value in "result".
 *
 * Special case for "%s" format: if "parameter_value" is an empty string, the
 * routine retuns an empty string in "result".
 *
 * Input : arg:    string to be searched for the parameter name and value
 *         str:    string with the parameter name
 *         format: format for the parameter's value, composed of
 *                 - a '%' in the first character
 *                 - optionally followed by the size modifier 'h', or 'l'
 *                 - followed by the conversion letter 's', 'd', 'u', 'f' or 'g'
 * Output: result: the value of the requested parameter
 * Return: 1  if the requested parameter was found
 *         0  otherwise
 */

int scan_argument(char *arg,char *str,char *format,void *result)
{
  char *pfmt,*rescop;
  char str_eq[256];
  static char bfhisnam[EdfMaxValLen + 1];
  int retval;

  /*
   * Test for valid format specification.
   */
  pfmt = format;
  if(*pfmt++ != '%' || strspn(pfmt + strspn(pfmt,"hl"),"sdufg") != 1) {
    prmsg(ERROR,("unknown parameter format %s wrong\n",format));
    return(0);
  }

  /*
   * Add a "=" (equal sign) to "str" and then search for the string in "arg".
   */
  strcpy(str_eq,str);
  strcat(str_eq,"=");

  if(strncmp(arg,str_eq,strlen(str_eq)) == 0) {
    strcat(str_eq,format);

    /*
     * Read the parameter value from the string "arg" according to the format
     * given.
     *
     * Return 0 if parameter name cannot be found.
     */
    if((retval = sscanf(arg,str_eq,result)) == 0) {
      prmsg(ERROR,("unknown parameter %s=xxx\n",str));
      return(0);

    /*
     * If found, print parameter value.
     */
    } else {
      strcat(str_eq,"\n");
      if(*pfmt == 's') {
        /*
         * The sscanf() function skips whitespace for "%s" format, and returns
         * an EOF error if an argument contains only whitespace. This would
         * therefore mean that the input value of "result" remains unchanged.
         *
         * To get an empty string argument from the input, check therefore if
         * the last character in the input is an equal sign "=".
         */
	if(retval == EOF && *(arg + strlen(arg) - 1) == '=') {
	  *(char *)result = '\0';
          sprintf(bfhisnam,"%s=",str);
	} else {
	  rescop = strdup(result);
          sprintf(bfhisnam,"%s=%.*s",str,EdfMaxValLen - 11,basename(rescop));
	  free(rescop);
	}
	prmsg(DMSG,(str_eq,result));
      } else {
        sprintf(bfhisnam,"%s%s",str,strchr(arg,'='));
        if(*pfmt == 'f' || *pfmt == 'g')
 	  prmsg(DMSG,(str_eq,*(float *)result));
        else if(*pfmt == 'd')
	  prmsg(DMSG,(str_eq,*(int *)result));
        else if(*pfmt == 'u')
	  prmsg(DMSG,(str_eq,*(unsigned int *)result));
        if(strcmp(pfmt,"lf") == 0 || strcmp(pfmt,"lg") == 0)
 	  prmsg(DMSG,(str_eq,*(double *)result));
        if(strcmp(pfmt,"hd") == 0)
 	  prmsg(DMSG,(str_eq,*(short *)result));
        if(strcmp(pfmt,"hu") == 0)
 	  prmsg(DMSG,(str_eq,*(short unsigned *)result));
        if(strcmp(pfmt,"ld") == 0)
 	  prmsg(DMSG,(str_eq,*(long *)result));
        if(strcmp(pfmt,"lu") == 0)
 	  prmsg(DMSG,(str_eq,*(long unsigned *)result));
      }
    }
    edf_history_argv("InputArg",bfhisnam);
    return(1);
  } else
    return(0);
}

/*==============================================================================
 * Analyzes a list of filename patterns and extracts filenames from them.
 *
 * At the first call, or the next call after a reset, the routine starts at the
 * beginning of the list of filename patterns, analyzes the first pattern and
 * if successful returns with the first filename.
 *
 * For the following calls, the routine keeps internally trace of how far the
 * pattern list has been analyzed, and returns subsequent filenames until an
 * error occurs or the pattern list is exhausted.
 *
 * When the pattern list has been exhausted, the internal state of the routine
 * is reset. This can also be done manually by calling the routine with a non-
 * positive number of file name patterns.
 *
 * There can be an arbitrary number of filename patterns as input to the
 * routine.
 *
 * The routine creates a sequence of filenames from a filename template with
 * some wildcard characters. These wildcard characters are replaced by numbers
 * according to the numerical information contained in the filename pattern.
 *
 * The filename pattern consists of up to four elements, separated by commas:
 * 1) the filename template: essentially a filename (possibly with path), but
 *    the name itself can contain one or several wildcard characters "%"
 *    (percent sign). These need not be contiguous in the template;
 * 2) the start value of the numerical sequence;
 * 3) the end value of the numerical sequence;
 * 4) the increment between two sequence numbers.
 *
 * In a filename pattern with wildcards, the first filename returned will be
 * the template string with the wildcard characters replaced by the start value
 * of the numerical sequence. The second filename will then contain the start
 * value plus the increment, and so on, until the generated number passes the
 * end value of the sequence.
 *
 * Negative numbers are allowed, even for the increment. The sequence then
 * counts downwards.
 *
 * Under the following circumstances the routine will return exactly one
 * filename (with the start value of the sequence):
 * - if the increment is 0;
 * - if (start value) > (end value) and the increment is positive;
 * - if (start value) < (end value) and the increment is negative;
 *
 * If the wildcard pattern is longer than the significant digits in the number,
 * it will be left padded with zeros. If the number is negative, the first
 * wildcard is replaced by a "-" (minus). If the wildcard pattern is shorter
 * than the significant digits in the number, the number will be truncated.
 *
 * Not all elements of the sequence need to be specified. Missing elements are
 * replaced by default values:
 * - start value = 1;
 * - end value = start value;
 * - increment = 1.
 *
 * If the filename template does not contain any wildcards, the template string
 * is returned as filename.
 *
 * Examples:   pattern                filenames created
 *             abc%%d,1,3,2           abc01d    abc03d
 *             abc%%de%%,497,495,-1   ab04de97  ab04de96  ab04de95
 *             abcdef                 abcdef
 *             abc%%%                 abc001
 *             abc%%%,8               abc008
 *             abc%%%,4,7             abc004    abc005    abc006 abc007
 *             abc%%%,-7,-6           abc-07    abc-06
 *
 * Input : filnam: buffer to receive character string. Must be allocated by
 *                 calling program
 *         fillen: length of buffer filnam
 *         patc:   number of file name patterns to analyze. If <= 0, the
 *                 internal state of the routine is reset
 *         patv:   array of strings with the file name patterns
 * Output: filnam: most recent filename obtained from the pattern analysis
 * Return: -1  (= error) if input arguments are incorrect or invalid
 *         -2  (= end) if pattern list has been exhausted (end of present
 *             analysis) or if manual reset has been done
 *          0  else (= OK, new filename obtained)
 */

int fnampat(char *filnam,size_t fillen,int patc,char *patv[])
{
  static char filbuf[FILENAME_MAX];
  char number[1024];
  char *ptrpatv;
  static int patn = 0;
  int err,numlen = sizeof(number);
  static long num,fst,lst,inc,maxloop,loop = 0;

  /*
   * Check for incorrect input buffer arguments. If so, return error.
   */
  if(fillen <= 0 || filnam == NULL)
    return(-1);
  *filnam = '\0';

  /*
   * Check if the input pattern list is exhausted, or a manual reset has been
   * requested. Return with end code.
   */
  if(patn >= patc || patc <= 0) {
    patn = 0;
    loop = 0;
    return(-2);
  }

  /*
   * Check if the filename pattern has wildcards. If not, return just this
   * pattern string and advance the pattern list pointer to the next pattern.
   */
  ptrpatv = *(patv + patn);
  if(filename_has_pattern(ptrpatv) == 0) {
    strncat(filnam,ptrpatv,fillen - 1);
    patn++;
    return(0);
  }

  /*
   * Check if at the beginning of a pattern analysis (then the pattern loop
   * counter is 0). If so, extract the filename template and possibly the
   * numerical arguments. Set default values for the numerical arguments if they
   * are not provided in the pattern.
   */
  if(loop == 0) {
    if(filename_parameter(filbuf,sizeof(filbuf),ptrpatv,0) == NULL)
      return(-1);
    fst = num_str2long(filename_parameter(number,numlen,ptrpatv,1),NULL,&err);
    if(err)
      fst = 0;
    lst = num_str2long(filename_parameter(number,numlen,ptrpatv,2),NULL,&err);
    if(err)
      lst = fst;
    inc = num_str2long(filename_parameter(number,numlen,ptrpatv,3),NULL,&err);
    if(err)
      inc = 1;

    /*
     * Determine number of iterations for the pattern, and set file index number
     * to the first value.
     */
    maxloop = inc == 0 ? 1 : (lst - fst) / inc + 1;
    if(maxloop < 1)
      maxloop = 1;
    num = fst;
  }

  /*
   * Construct the actual filename from the pattern with the present file index
   * number.
   */
  filename_pattern(filnam,fillen,filbuf,num);

  /*
   * Check if at the end of a pattern analysis. If so, advance the pattern list
   * pointer to the next pattern and reset loop counter. If not, set the file
   * index number to its next value for the next call of the routine.
   */
  if(++loop >= maxloop) {
    loop = 0;
    patn++;
  } else
    num+=inc;
  return(0);
}

/*==============================================================================
 * Creates a name for an output file by taking the string "infile" with the
 * name of the input file and replacing the extension contained in "inext" by
 * the string contained in "outext". The resulting file name can optionally be
 * preceded by the directory path contained in "outdir".
 *
 * More precisely, the "infile" is searched for the occurrence of the substring
 * given by "inext". If found, "inext" in the string "infile" is replaced by
 * "outext". It does not need to be separated by e.g. a dot, and the substring
 * does not even have to be at the end of "infile". As an example, with "infile"
 * containing "datafile3.dat", "inext" containing "e3" and "outext" containing
 * "_cor", the output file name is "datafil_cor.dat".
 *
 * If "infile" is a NULL pointer or empty, the output file name is "outext".
 * If "outext" is also a NULL pointer, the routine returns a NULL pointer.
 *
 * If "inext" is a NULL pointer or is not found as a substring of "infile", then
 * the output file name is created by concatenating "infile" with "outext". If
 * "outext" is empty or NULL, this means that the output file name is the same
 * as the input file name.
 *
 * If "outext" is a NULL pointer or empty, the output file name is "infile",
 * with the substring "inext" removed if found.
 *
 * The resulting file name will be preceded by the directory path contained in
 * "outdir", if
 * - "outdir" does contain a real string (not NULL pointer and not empty);
 * - "infile" does not contain a directory part (i.e. the "infile" string does
 *   not contain the directory separator "/" anywhere).
 *
 * No tests are made that the string in "outdir" is of the type "directory path"
 * (e.g. with at least one "/" separator at the end of the string), thus the
 * routine could also be used to add a prefix to the output file name.
 *
 * Note that the routine returns a pointer to a static character array. It will
 * therefore be overwritten by each subsequent call, but it needs not (cannot)
 * be freed.
 *
 * Input : infile: string with the name for the input file
 *         outdir: string with the (optional) directory path for the output file
 *         inext:  string with the extension for the input file name
 *         outext: string with the extension for the output file name
 * Output: none
 * Return: string with the name for the output file
 *         NULL   if no output file name could be created
 */

char *outname(char *infile,char *outdir,char *inext,char *outext)
{
  char *pos;
  static char outfile[1024];

  /*
   * Put output directory path at the beginning of the file name if:
   * - it exists (not NULL pointer and not empty);
   * - the file name itself does not contain a directory path.
   */
  if(outdir != NULL && *outdir != '\0' && strchr(infile,'/') == NULL)
    strcpy(outfile,outdir);
  else
    *outfile = '\0';

  /*
   * Search for the substring with the input file name extension.
   */
  if(infile == NULL || *infile == '\0') {
    /*
     * If the input file name is a NULL pointer or empty, the output file name
     * is just the file name extension. If the extension is equally a NULL
     * pointer or empty, return a NULL pointer for the output file name.
     */
    if(outext == NULL || *outext == '\0')
      return((char *)NULL);
    strcat(outfile,outext);

  } else if(inext == NULL || *inext == '\0' ||
      (pos = (char *)strstr(infile,inext)) == NULL) {
    /*
     * If the file name extension is not found, concatenate the input file name
     * with the output file extension.
     */
    strcat(outfile,infile);
    if(outext != NULL && *outext != '\0')
      strcat(outfile,outext);
  } else {
    /*
     * If found, replace everything from the start of the input file name
     * extension with the output file name extension.
     */
    strncat(outfile,infile,pos - infile);
    if(outext != NULL && *outext != '\0')
      strcat(outfile,outext);
    strcat(outfile,pos + strlen(inext));
  }
  
  return(outfile);
}

/*==============================================================================
 * Determines the byte ordering scheme of the computer on which the program
 * runs.
 *
 * The possibilities are big endian, where the most significant byte (of a
 * 2-byte or bigger word) has the lower address, or little endian, where the
 * least significant byte has the lower address.
 *
 * Input : none
 * Output: none
 * Return:  1  if big endian byte ordering
 *          0  if little endian byte ordering
 *         -1  if type cannot be determined
 */

int isbigendian()
{
  union {
    char buf[4];
    long x;
  } v;

  /* ??? why does setting to 0 help for the 64 bit case? Would one then not get
     0x1234567800000000 for the big endian case ??? */
  v.x = 0; /*if 64 bit longs */
  v.buf[0] = 0x12; v.buf[1] = 0x34; v.buf[2] = 0x56; v.buf[3] = 0x78;

  if (v.x == 0x12345678)
    return(1);    /* Sun Sparc, HP700 ... */
  else if (v.x == 0x78563412)
    return(0);    /* 80x86, vax, ... */
  /* ??? if it is neither (see remark above), what is returned then ??? */
  return(-1);
}

/*==============================================================================
 * Measures and prints the time difference between two consecutive calls.
 *
 * At the first call to this routine, the input argument "str" must be a NULL
 * pointer. This sets the start time.
 *
 * All subsequent calls with "str" not equal to a NULL pointer measure the time
 * between the start time and the time of the present call, print it together
 * with "str" and store the time of the present call into the start time as
 * preparation for the next call. It is thus possible to time a whole sequence
 * of events.
 *
 * To end such a sequence and start a new one, the routine can at any time be
 * called again with a NULL pointer, thus resetting the start time.
 *
 * Input : str: if NULL: resets the start time to the time of the present call
 *              else:    string to be printed with the time difference
 * Output: none
 * Return: if no error: 0
 *         else       : error code of gettimeofday() function
 */

int bench(char *str)
{
  static struct timeval start,stop;
  struct timezone tzp;
  int iret;

  if(str == (char *)NULL) {
    iret = gettimeofday(&start,&tzp);
  }
  else {
    if((iret = gettimeofday(&stop,&tzp)) == 0) {
      prmsg(DMSG,("Time in %s : %10.3f\n",str,(double)(stop.tv_sec-start.tv_sec)
        + (double)(stop.tv_usec-start.tv_usec) * (double)0.000001));
      start.tv_sec = stop.tv_sec;
      start.tv_usec = stop.tv_usec;
    }
  }
  return(iret);
}
