/*
 *   Project: The SPD Image correction and azimuthal regrouping
 *			http://forge.epn-campus.eu/projects/show/azimuthal
 *
 *   Copyright (C) 2001-2010 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: P. Boesecke (boesecke@esrf.fr)
 *                      R. Wilcke (wilcke@esrf.fr)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

/*
Update 21/10/2011 P. Boesecke (boesecke@esrf.fr)
                  Flag NO_SHARED_MEMORY added to allow compilation with mingw 
                  on Windows (cygwin flag -mno-cygwin). If set, do not use 
                  shared memories, strlib included from edfpack.
                  two POSIX functions missing in mingw: strtok, strtok_r.
                  They must be replaced by other functions to use mingw.
Update 18/10/2011 P. Boesecke (boesecke@esrf.fr)
                  INVALID_TYP added, TMPTYP added.
Update 14/10/2011 P. Boesecke (boesecke@esrf.fr)
                  new data type SDMTYP: this array contains multiplication
                  factors that must be applied after spatial distortion
                  correction to the corrected image. It is created together 
                  with SDXTYP and SDYTYP, set_moutfile added,
                  get_doprerot added.
Update 23/09/2011 P. Boesecke (boesecke@esrf.fr)
                  subtract_im renamed to subtract_drk
                  new subtract_im function added.
Update 12/09/2011 P. Boesecke (boesecke@esrf.fr)
                  set_prerot_headval added
Update 31/08/2011 P. Boesecke (boesecke@esrf.fr)
                  add include of cmpr.h
Update 19/08/2011 P. Boesecke (boesecke@esrf.fr)
                  Version set to spd-1-5 (prerotation)
                  The prerotation correction needs to be tested, 
                  all existing functionality of version spd-1-4 
                  should work as before.
Update 27/07/2011 P. Boesecke (boesecke@esrf.fr)
                  add include of sx.h,
                  getShmDataPtr added
Update 22/07/2011 P. Boesecke (boesecke@esrf.fr)
                  rearrangement of code and renaming of functions
                  (see correct.c). Version set to spd-1-4
Update 03/06/2010 P. Boesecke (boesecke@esrf.fr)
                  add 2 new input arguments to function declaration azim_int().
Update 16/11/2009 R. Wilcke (wilcke@esrf.fr)
                  move the definitions of the SPD return code here from routine
                  "analyse_args()" in file "inout.c".
Update 06/10/2009 P. Boesecke (boesecke@esrf.fr)
                  replaced include SaxsDefinition.h => reference.h
                  added include project.h angle.h
                  (works with saxspack>=V2.440 and edfpack>=E2.169)
Update 30/09/2009 R. Wilcke (wilcke@esrf.fr)
                  add input argument to function declaration prntvers().
Update 26/08/2009 R. Wilcke (wilcke@esrf.fr)
                  add function declaration getstate().
Update 25/08/2009 R. Wilcke (wilcke@esrf.fr)
                  add function declaration prntvers().
Update 24/04/2009 R. Wilcke (wilcke@esrf.fr)
                  add new input argument (char *) to the "outname" declaration.
Update 16/02/2009 A. Gotz (andy.gotz@esrf.fr)
                  To allow compilation with cpp compiler all
                  C-function included into extern "C" {}
                  __cplusplus added to allow compilation with gcc
Update 22/01/2008 R. Wilcke (wilcke@esrf.fr)
                  add function declaration region_compare().
Update 17/12/2007 R. Wilcke (wilcke@esrf.fr)
                  add a fifth argument to scale_im().
Update 06/12/2007 R. Wilcke (wilcke@esrf.fr)
                  add include of "filename.h";
                  add function declaration fnampat().
Update 28/11/2007 R. Wilcke (wilcke@esrf.fr)
                  add a fourth argument to expon_im().
Update 10/10/2007 R. Wilcke (wilcke@esrf.fr)
                  add new image type DISTYP;
                  rename "flatfield" to "floodfield" in "typestr" array.
Update 10/04/2007 R. Wilcke (wilcke@esrf.fr)
                  add function declaration set_dodark().
Update 13/02/2007 R. Wilcke (wilcke@esrf.fr)
                  remove special enumerated data for header value updating;
                  add new image type CMDTYP.
Update 26/09/2006 R. Wilcke (wilcke@esrf.fr)
                  modify declaration of mark_overflow_nocorr() for additional
                  argument;
                  add declarations of set_inpexp() and expon_im().
Update 11/08/2006 R. Wilcke (wilcke@esrf.fr)
                  include "ipol.h" for additional SAXS definitions.
Update 19/08/2005 R. Wilcke (wilcke@esrf.fr)
                  define macro RELTABSH instead of RELTABSIZE.
Update 25/01/2005 R. Wilcke (wilcke@esrf.fr)
                  add a whole new set of "Dis" parameters to the user header
                  data structure for the displaced parameters;
                  add Offset_1, Offset_2, BSize_1 and BSize_2 to the enumeration
                  data for the header value updating.
Update 24/01/2005 R. Wilcke (wilcke@esrf.fr)
                  remove definition of TEMP_BUF_SIZE;
                  add definition of RAD2DEG().
Update 13/01/2005 R. Wilcke (wilcke@esrf.fr)
                  change "headkey" words Psize_1 -> PSize_1, Psize_2 -> PSize_2;
                  remove declaration of set_dolater();
                  add declarations for new header elements "ProjectionType",
                  "DetectorRotation_1", "DetectorRotation_2" and
                  "DetectorRotation_3": flags FL_PRO, FL_ROT1, FL_ROT2 and
                  FL_ROT3, members ProjTyp, DetRot_1, DetRot_2 and DetRot_3 for
                  structure "data_head", "ProjectionType", "DetectorRotation_1",
                  "DetectorRotation_2", "DetectorRotation_3" strings in string
                  array "headkey";
                  add more values in the enumeration data type for updating of
                  header values from the distortion file or from the command
                  line;
                  remove last input argument from the azim_int() declaration.
Update 21/09/2004 R. Wilcke (wilcke@esrf.fr)
                  change name bin_imag() to map_imag().
Update 25/08/2004 R. Wilcke (wilcke@esrf.fr)
                  add parameter to the azim_int() declaration to calculate the
                  scattering vector for images that have been projected to the
                  Ewald sphere;
                  add enumeration data type for updating certain header values
                  from the distortion file or from the command line;
                  change name of routine set_psizdist() to set_dstrtval().
Update 24/08/2004 R. Wilcke (wilcke@esrf.fr)
                  include "numio.h";
                  add flags FL_TITLE, FL_TIME and FL_EXTIML to macro FL_IMAGE
                  and remove flags FL_ORIEN, FL_DUMMY, FL_DDUMM, FL_PSIZ1 and
                  FL_PSIZ2.
Update 29/03/2004 R. Wilcke (wilcke@esrf.fr)
                  add declarations for new header elements "BSize_1" and
                  "BSize_2": flags FL_BSIZ1 and FL_BSIZ2, members BSize_1 and
                  BSize_2 for structure "data_head", "BSize_1" and "BSize_2"
                  strings in string array "headkey";
                  add declarations for new header elements "Dim_1" and
                  "Dim_2" (as above for "BSize_1" and "BSize_2");
                  add  FL_BSIZ1 and FL_BSIZ2 to FL_IMAGE;
                  make members "init" and "Orientat" in structure "data_head"
                  type "long" instead of "short";
                  replace function definitions set_bckgim(), set_drkim() and
                  set_floim() by set_imgbuf();
                  remove definition of LOW_MEM macro (no longer used).
Update 19/03/2004 R. Wilcke (wilcke@esrf.fr)
                  add function declaration bin_imag().
Update 29/01/2004 R. Wilcke (wilcke@esrf.fr)
                  removed the conditional code that depended on macro MY_TIMEVAL
                  (definition of structures "timeval" and "timezone").
Update 14/01/2004 R. Wilcke (wilcke@esrf.fr)
                  add handling of variable length argument list with "stdarg"
                  for ANSI C compilers (include "stdarg.h" and different
                  function declaration for _prmsg()).
Update 21/07/2003 R. Wilcke (wilcke@esrf.fr)
                  add a second argument to the set_normint() function.
Update 26/11/2002 R. Wilcke (wilcke@esrf.fr)
                  change type of set_psizdist() from "void" to "int";
                  add image type MSKTYP for "mask" data (a mask defining the
                  pixels to ignore in azimuthal integration).
Update 26/09/2002 R. Wilcke (wilcke@esrf.fr)
                  remove declaration of routine set_verbose();
                  define new message type macro PRERR.
Update 17/09/2002 R. Wilcke (wilcke@esrf.fr)
                  declare functions unloadspd() and area_only() as "void".
Update 05/09/2002 R. Wilcke (wilcke@esrf.fr)
                  add member "ExpTime" to structure "data_head" and define
                  corresponding flag;
                  convert members "Intens_0" and "Intens_1" of structure
                  "data_head" from "float" to "character string".
Update 03/09/2002 R. Wilcke (wilcke@esrf.fr)
                  rename enumerated variable BKGTYP to DRKTYP;
                  change function names set_bkgconst() and set_bkgim() to
                  set_drkconst() and set_drkim();
                  rename enumerated variable SCATYP to SBKTYP;
                  change function names set_scaconst(), set_scafact() and
                  set_scaim() to set_bckgconst(), set_bckgfact and set_bckgim();
Update 13/06/2002 R. Wilcke (wilcke@esrf.fr)
                  include <sys/time.h> instead of <time.h>.
Update 03/06/2002 R. Wilcke (wilcke@esrf.fr)
                  include <libgen.h> header file.
Update 12/03/2002 R. Wilcke (wilcke@esrf.fr)
                  move declaration of global variable "typestr" and definition
                  of macro MAXTYP from "inout.c" to this file;
                  replace data type SPDTYP by the two new data types SDXTYP and
                  SDYTYP.
Update 05/03/2002 R. Wilcke (wilcke@esrf.fr)
                  remove structure member "lut_d->temp_im" (no longer used);
                  add members "Title" and "Time" to structure "data_head" and
                  define the corresponding flags FL_TITLE and FL_TIME.
Update 04/03/2002 R. Wilcke (wilcke@esrf.fr)
                  rename global variable NO_TYP to HD_TYP.
Update 20/12/2001 R. Wilcke (wilcke@esrf.fr)
                  remove last argument (title) in save_esrf_file() declaration
                  and add "type" as new 5th argument.
Update 19/12/2001 R. Wilcke (wilcke@esrf.fr)
                  remove the last input argument in the declaration of
                  save_esrf_file();
                  define macro FL_IMAGE as a combination of several header
                  keyword flags.
Update 10/12/2001 R. Wilcke (wilcke@esrf.fr)
                  add definition of user header type AVETYP.
Update 03/12/2001 R. Wilcke (wilcke@esrf.fr)
                  add the averaging buffer and the scale factor to the input
                  arguments of the azim_int() declaration.
Update 29/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add "rows" and "cols" as input arguments to get_buffer();
                  change some "headkey" key words: PSize_1 -> Psize_1,
                  PSize_2 -> Psize_2, Orientation -> RasterOrientation.
Update 26/11/2001 R. Wilcke (wilcke@esrf.fr)
                  azim_int(): change the input arguments;
                  add two new arguments to the declaration of put_buffer().
Update 22/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add definition of user header type AZITYP.
Update 19/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add declarations for the new functions set_inpconst(),
                  set_inpfact(), set_scaconst(), set_scafact() and scale_im().
Update 16/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add declaration of the keywords for the user header elements;
                  add declaration for new function set_psizdist().
Update 12/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add 3. input argument "type" to declaration of
                  mark_overflow_nocorr().
Update 08/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add declarations for functions set_inpmin() and set_inpmax().
Update 05/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add 2 more new arguments to the declaration of azim_int().
Update 22/10/2001 R. Wilcke (wilcke@esrf.fr)
                  add 3 new input arguments to azim_int() declaration.
Update 18/10/2001 R. Wilcke (wilcke@esrf.fr)
                  add declarations for normint_im() and azim_int().
Update 03/10/2001 R. Wilcke (wilcke@esrf.fr)
                  change last argument in the declaration of save_esrf_file()
                  from "char *" to "int".
Update 02/10/2001 R. Wilcke (wilcke@esrf.fr)
                  remove the last two arguments from the declaration of
                  save_esrf_file().
Update 13/09/2001 R. Wilcke (wilcke@esrf.fr)
                  changed type of member "init" in structure "data_head" from
                  "short" to "unsigned short".
Update 20/08/2001 R. Wilcke (wilcke@esrf.fr)
                  add declaration for function get_headval();
Update 17/08/2001 R. Wilcke (wilcke@esrf.fr)
                  changed value of SPDTYP from -1 to 6;
                  added declarations of flags for the data_head members;
                  add member "WaveLeng" to structure "data_head" and define
                  corresponding flag.
Update 14/08/2001 R. Wilcke (wilcke@esrf.fr)
                  add new image type macro SPDTYP;
                  add new input argument of type "int" in 5th position to
                  read_esrf_file().
Update 13/08/2001 R. Wilcke (wilcke@esrf.fr)
                  change type of set_headval() from "void" to "int" and add
                  a second input argument;
                  add definitions for the macros SRCTYP, CORTYP, BKGTYP, FLOTYP,
                  NO_TYP and SCATYP for the image types.
Update 07/08/2001 R. Wilcke (wilcke@esrf.fr)
                  declare new routine set_scaim();
                  add fourth input argument to subtract_im().
Update 03/08/2001 R. Wilcke (wilcke@esrf.fr)
                  declare new routines set_normint() and set_headval();
                  include <string.h>;
                  added definition of structure data_head.
Update 28/06/2001 R. Wilcke (wilcke@esrf.fr)
                  change declaration of routine correct_image() by eliminating
                  the background and floodfield image from the input arguments;
                  declare new routines set_bkgim() and set_floim().
Update 26/06/2001 R. Wilcke (wilcke@esrf.fr)
                  declare new function set_xycorout().
Update 25/06/2001 R. Wilcke (wilcke@esrf.fr)
                  move declaration of "verbose" to "correct.c";
                  declare new functions set_verbose(), set_overflow(),
                  set_dummy(),set_actrad(),set_splinfil(),set_xycorin(),
                  set_xysize(),get_xsize(),get_ysize(),set_bkgconst(),
                  set_doflat(), set_dolater(), set_dospd().
Update 11/05/2001 R. Wilcke (wilcke@esrf.fr)
                  remove declaration of gethead() routine - no longer used.
Update 02/02/2001 R. Wilcke (wilcke@esrf.fr)
                  change all arguments of divide_insito_im() and divide_im()
                  to type "float";
                  change name of substract_im() to subtract_im() and make all
                  arguments of type "float".
                  change all "unsigned short" arguments of mark_overflow() and
                  mark_overflow_nocorr() to type "float";
                  change all "unsigned short" arguments of corr_calc() to type
                  "float";
                  remove declaration of corr_calc_plus();
                  remove definitions of HALFSCALE and FLO_SHIFT;
                  change all "unsigned short" arguments of correct_image() to
                  type "float";
                  change member *temp_im of struct lut_descript to type "float";
Update 18/01/2001 R. Wilcke (wilcke@esrf.fr)
                  removed declaration of pxcorr();
                  changed parameter types of pxcorrgrid() and readarray() from
                  "double" to "float".
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 

#if defined(__STDC__)
#include <stdarg.h>
#else
#include <varargs.h>
#endif /* __STDC__ */

#include <libgen.h>
#include <sys/time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#ifndef NO_SHARED_MEMORY
#include <sys/shm.h>
#include <sys/ipc.h> 
#endif

#include "spec_shm.h"

#include <strlib.h>
#include <edfio.h>
#include <reference.h>
#include <project.h>
#include <arc.h>
#include <ipol.h>
#include <numio.h>
#include <filename.h>
#include <sx.h>
#include <rot3d.h>
#include <r2t.h>
#include <cmpr.h>

extern int errno;

#if SOLARIS 
#define UNDERSCORE 1 
#endif

#if LINUX
#define UPPERCASE 1
#endif

#define USE_OFFSET_TAB 0
#define WASTE4_FORSPEED 1  /* This flag needs to be on if you want to read in
			      x and y distortion from a edf file */
#define WASTE2_FORSRCTEMP 1
#define KEEP_COUNTS 1
#define TEMP_LONG 1
#define LUT_BYTE 0
#define MY_MALLOC 1
#define BOUND_CHECK 1
#define BOUND_SUPER 0

#define MAX_PIXELSIZE 6 
#define MAX_TRIANGLES 1000
#define MAX_PARTS 200
#define VALUES_PERLINE 5

#define MSG 0
#define WARNING 1
#define ERROR 2
#define FATAL 3
#define DMSG 4
#define PRERR 0x02000000

#define INCTARGET 1
#define ABSSRC 2
#define UNCOMPRESSED 4
#define PROGEND 8
#define MULTIINC 16

#if LUT_BYTE
#define LUT_TYPE unsigned char
#define FULLSCALE 0x80
#define SHIFT 7
#define MAPSCALE 0x7f
#define BITMASK 0x80
#else
#define LUT_TYPE unsigned short
#define FULLSCALE 0x8000
#define SHIFT 15
#define MAPSCALE 0x7fff
#define BITMASK  0x8000
#endif

#define MAXHIST (1<<18)
#define RELTABSH 5          /* for rel_tab sequence size of 32 (= 1 << 5) */

#define RAD2DEG(x) ((double)(x) * 180. / 3.1415926535897932384626)

/*
 * Define the image types:
 * SRCTYP  source image
 * CORTYP  corrected image
 * DRKTYP  dark (= background) image
 * FLOTYP  floodfield image
 * HD_TYP  header buffer handed over online
 * SBKTYP  scattering background image
 * SDXTYP  x-direction displacement values for spatial distortion correction
 * SDYTYP  y-direction displacement values for spatial distortion correction
 * SDMTYP  multiplication factors applied after spatial distortion correction
 * AZITYP  azimuthal integrated image
 * AVETYP  azimuthal averaged image
 * MSKTYP  mask image with pixels to ignore for azimuthal integration
 * CMDTYP  header structure with values filled by command line arguments
 * DISTYP  file with the spline parameters for the distortion correction
 * TMPTYP  temporary header for calculations   
 */

static char *typestr[] = {"invalid","source","corrected","dark","floodfield",
  "header","scattering-background","x-distortion","y-distortion","m-distortion",
  "regrouped","averaged","mask","command-line","spline-distortion",
  "temporary header"};

#define MAXTYP ( sizeof(typestr) / sizeof(char *) )

enum {INVALID_TYP=0,SRCTYP,CORTYP,DRKTYP,FLOTYP,HD_TYP,SBKTYP,SDXTYP,SDYTYP,SDMTYP,
      AZITYP,AVETYP,MSKTYP,CMDTYP,DISTYP,TMPTYP};

struct lut_descript {
  LUT_TYPE *lut;
  unsigned char *prog;
  int prog_length;
  unsigned int starttidx;
  unsigned int startsidx;
  int *offset_tab;
  int *rel_tab;
  int **relend_tab;
  unsigned short *abs_src;
  int maxxpixel;
  int maxypixel;
  short *xrel;
  short *yrel;
};

struct triangle {
  float x[3];
  float y[3];
  float area;
  int xpos;
  int ypos;
};

/*
 * Define flags for the return code of SPD:
 *
 * - SPD_ERRFLG: an error occured during the image processing
 * - SPD_BASUSE: filename of command line option "base_name" was used
 * - SPD_SRCFIL: a file for the source image was written
 * - SPD_DRKFIL: a file for the dark image was written
 * - SPD_CORFIL: a file for the corrected image was written
 * - SPD_AZIFIL: a file for the azimuthally integrated image was written
 * - SPD_AVEFIL: a file for the azimuthally averaged image was written
 *
 * All applicable flags are "OR"ed together in the return code.
 */
#define SPD_ERRFLG 0x80000000
#define SPD_BASUSE 0x01000000
#define SPD_SRCFIL 0x00010000
#define SPD_DRKFIL 0x00020000
#define SPD_CORFIL 0x00040000
#define SPD_AZIFIL 0x00080000
#define SPD_AVEFIL 0x00100000

/*
 * Declarations for the user data header:
 * - the flags to indicate whether a particular member of the header structure
 *   has been initialized. The flags will be "OR"ed into the "init" member;
 * - the structure of the user data header;
 * - the keywords associated with the members of the structure.
 */
#define FL_ORIEN 0x00000001
#define FL_DUMMY 0x00000002
#define FL_DDUMM 0x00000004
#define FL_OFFS1 0x00000008
#define FL_OFFS2 0x00000010
#define FL_PSIZ1 0x00000020
#define FL_PSIZ2 0x00000040
#define FL_INTE0 0x00000080
#define FL_INTE1 0x00000100
#define FL_CENT1 0x00000200
#define FL_CENT2 0x00000400
#define FL_SAMDS 0x00000800
#define FL_WAVLN 0x00001000
#define FL_TITLE 0x00002000
#define FL_TIME  0x00004000
#define FL_EXTIM 0x00008000
#define FL_BSIZ1 0x00010000
#define FL_BSIZ2 0x00020000
#define FL_DIM1  0x00040000
#define FL_DIM2  0x00080000
#define FL_PRO   0x00100000
#define FL_ROT1  0x00200000
#define FL_ROT2  0x00400000
#define FL_ROT3  0x00800000
#define FL_PRECEN1 0x01000000
#define FL_PRECEN2 0x02000000
#define FL_PREDIS  0x04000000
#define FL_PREROT1 0x08000000
#define FL_PREROT2 0x10000000
#define FL_PREROT3 0x20000000

#define FL_IMAGE (FL_OFFS1 | FL_OFFS2 | FL_BSIZ1 | FL_BSIZ2 | FL_TITLE | \
  FL_TIME | FL_EXTIM)

struct data_head {
  unsigned long init;
  unsigned short Dim_1; // FL_DIM1
  unsigned short Dim_2; // FL_DIM2
  long Orientat;        // FL_ORIEN
  float Dummy;          // FL_DUMMY
  float DDummy;         // FL_DDUMM
  float Offset_1;       // FL_OFFS1
  float Offset_2;       // FL_OFFS2
  float PSize_1;        // FL_PSIZ1
  float PSize_2;        // FL_PSIZ2
  float Center_1;       // FL_CENT1
  float Center_2;       // FL_CENT2
  float BSize_1;        // FL_BSIZ1
  float BSize_2;        // FL_BSIZ2
  float SamplDis;       // FL_SAMDS
  float WaveLeng;       // FL_WAVLN
  float DetRot_1;       // FL_ROT1
  float DetRot_2;       // FL_ROT2
  float DetRot_3;       // FL_ROT3

  float PreCenter_1;    // FL_PRECEN1
  float PreCenter_2;    // FL_PRECEN2
  float PreSamplDis;    // FL_PREDIS
  float PreDetRot_1;    // FL_PREROT1
  float PreDetRot_2;    // FL_PREROT2
  float PreDetRot_3;    // FL_PREROT3

  char ProjTyp[5];                 // FL_PRO
  char Intens_0[EdfMaxValLen + 1]; // FL_INTE0
  char Intens_1[EdfMaxValLen + 1]; // FL_INTE1 
  char Title[EdfMaxValLen + 1];    // FL_TITLE
  char Time[EdfMaxValLen + 1];     // FL_TIME
  char ExpTime[EdfMaxValLen + 1];  // FL_EXTIM
  /*
   * Now the values for the displaced parameters.
   */
  unsigned long Dspinit;
  unsigned short DspDim_1; // FL_DIM1
  unsigned short DspDim_2; // FL_DIM2
  long DspOrientat;        // FL_ORIEN
  float DspDummy;          // FL_DUMMY
  float DspDDummy;         // FL_DDUMM
  float DspOffset_1;       // FL_OFFS1
  float DspOffset_2;       // FL_OFFS2
  float DspPSize_1;        // FL_PSIZ1
  float DspPSize_2;        // FL_PSIZ2
  float DspCenter_1;       // FL_CENT1
  float DspCenter_2;       // FL_CENT2
  float DspBSize_1;        // FL_BSIZ1
  float DspBSize_2;        // FL_BSIZ2
  float DspSamplDis;       // FL_SAMDS
  float DspWaveLeng;       // FL_WAVLN
  float DspDetRot_1;       // FL_ROT1
  float DspDetRot_2;       // FL_ROT2
  float DspDetRot_3;       // FL_ROT3

  float DspPreCenter_1;    // FL_PRECEN1
  float DspPreCenter_2;    // FL_PRECEN2
  float DspPreSamplDis;    // FL_PREDIS
  float DspPreDetRot_1;    // FL_PREROT1
  float DspPreDetRot_2;    // FL_PREROT2
  float DspPreDetRot_3;    // FL_PREROT3

  char DspProjTyp[5];                 // FL_PRO
  char DspIntens_0[EdfMaxValLen + 1]; // FL_INTE0
  char DspIntens_1[EdfMaxValLen + 1]; // FL_INTE1
  char DspTitle[EdfMaxValLen + 1];    // FL_TITLE
  char DspTime[EdfMaxValLen + 1];     // FL_TIME
  char DspExpTime[EdfMaxValLen + 1];  // FL_EXTIM
};

static char *headkey[] = {"RasterOrientation","Dummy","DDummy",
  "Offset_1","Offset_2","PSize_1","PSize_2","Intensity0","Intensity1",
  "Center_1","Center_2","SampleDistance","WaveLength","Title","Time",
  "ExposureTime","BSize_1","BSize_2","Dim_1","Dim_2","ProjectionType",
  "DetectorRotation_1","DetectorRotation_2","DetectorRotation_3",
  "PreCenter_1","PreCenter_2","PreSampleDistance",
  "PreDetectorRotation_1","PreDetectorRotation_2","PreDetectorRotation_3"};
static int maxhdkey = sizeof(headkey) / sizeof(char *);

/*
 * The macros "prmsg()" and "__prmsg()" handle the printing of user-defined
 * messages. The actual file name and line number may be added to the user's
 * message.
 *
 * The macros work by calling the function "_prmsg()". For more details, see  
 * the corresponding description.
 *
 * The input parameter that contains the message to print must contain a full
 * print argument list, i.e. the format statement, the variables to print, and
 * the surrounding parentheses! Example for a valid prmsg() call:
 *
 *   prmsg(ERROR,("Cannot read <%s> (open failed)\n",filename));
 *
 * "prmsg" will use the file name and line number corresponding to the location
 * from where it was called. "__prmsg" allows these to be given as parameters.
 * This can be useful, e.g. if the macro is used inside a service function, and
 * the important information is not the location in the service function, but
 * the location where this service function was called.
 *
 * Macro prmsg:
 * Input : N: message type
 *         M: message to print
 * Output: none
 * Return: none
 *
 * Macro __prmsg:
 * Input : N: message type
 *         M: current file name
 *         L: current line number
 *         K: message to print
 * Output: none
 * Return: none
 */
#define prmsg(N,M)  do{_prmsg(NULL,N,__FILE__,__LINE__); _prmsg M; } while(0)
#define __prmsg(N,M,L,K)  do{_prmsg(NULL,N,M,L); _prmsg K; } while(0)

#if MY_MALLOC
/*
 * If MY_MALLOC is defined, an alternative set of routines for memory
 * management is used. The following routines are defined:
 *
 * - pmalloc(), prealloc(), pfree() replace the standard routines malloc(),
 *   realloc() and free();
 * - print_memsize() prints the total allocated memory size.
 *
 * The differences to the standard routines are:
 * - the alternative set provides error printout indicating the current file
 *   and line number where the error occurred;
 * - the alternative set operates on a linked list of data structures of type
 *   "pmem". Each structure contains the size of its data segment, a pointer to
 *   the next element in the list, and a pointer to its own data segment. This
 *   allows to keep track of the overall memory usage by the program with the
 *   routine print_memsize().
 *
 * Otherwise, the input parameters and return values of the routines in the
 * alternative set are the same as the ones in the standard set.
 *
 * If MY_MALLOC is not defined, pmalloc(), prealloc() and pfree() are defined 
 * to be the standard set of memory management routines, and print_memsize()
 * is a dummy.
 */

struct pmem {
  void *data;            /* data segment */
  long int size;         /* size of the data segment */
  struct pmem *next;     /* pointer to next structure in linked list */
  void *fill128;         /* align the return ptr again on 16 byte boundary*/
};

#define pmalloc(N) _pmalloc(N,__FILE__,__LINE__)
#define prealloc(N,M) _prealloc(N,M,__FILE__,__LINE__)
#define pfree(N) _pfree(N,__FILE__,__LINE__)

#else 
#define pmalloc malloc
#define pfree free
#define prealloc realloc
#endif /* MY_MALLOC */

/*
 * Function Prototypes
 */

#if defined __cplusplus
  extern "C" {
#endif

#ifndef NO_SHARED_MEMORY
  # define SHM_HEADER struct shm_header
#else
  # define SHM_HEADER void
#endif

SHM_HEADER *getShmPtr(int,int);
void *getShmDataPtr(SHM_HEADER*,int);

int getstate(void);
void prntvers(char *);
int get_headval(struct data_head *,int);
int pr_headval(FILE *, int);
int get_xsize(void);
int get_ysize(void);
void set_actrad(float);
void set_bckgconst(float);
void set_bckgfact(float);
void set_dodark(int);
void set_doflat(int);
void set_dospd(int);
void set_doprerot(int doprerot);
int get_doprerot( void );
void set_normprerot(int normprerot);
void set_drkconst(float);
void set_dummy(float);
int set_headval(struct data_head,int);
void set_imgbuf(void *,int);
void set_inpconst(float);
void set_inpexp(float);
void set_inpfact(float);
void set_inpmax(float);
void set_inpmin(float);
void set_normint(int,float);
void set_overflow(unsigned long);
int set_dstrtval(int);
void set_splinfil(char *);
void set_xycorin(char *,char *);
void set_xycorout(char *,char *);
void set_moutfile(char *mfile);
void set_xysize(int,int);

#if defined(__STDC__)
void _prmsg(char *format,...);
#else
void _prmsg();     
#endif /* __STDC__ */

struct lut_descript *lut_calc(void);
void *_pmalloc(int,char *,int);
void *_prealloc(void *,int,char *,int);
int _pfree(void *,char *,int);
int print_memsize(void);
int print_memlist(void);
char *spd_fgets(FILE *);
struct spd_spline *spd_loadspline(char *);
int spd_findkeyword(FILE *,char *);
int spd_readarray(FILE *,int,float *);
void spd_unloadspline(struct spd_spline *);
int spd_calcspline(struct spd_spline *,int,int,float [],float [],float [],float []);
int spd_corr(float **,float **,float **,int,int,int,int);
int spd_free_buffers(float *,float *);
int spd_func(int,int,float *,float *);
int trianglecutv_only(struct triangle *,float,struct triangle [],int *);
int trianglecuth_only(struct triangle *,float,struct triangle [],int *);
void area_only(float *,float *,float *);
int triangle_cutall(struct triangle [],int *,int *,int *,int *,int *);
int calcparts(float [],float [],int,float [],int *,int *,int *,int *,float *);
int debugout(struct triangle [],int,float,float [],int,int,int,int);
int pxtomm(float *,float *,float *,float *);
int loadspd(char *,float *,float *,int *,int);
int byte_swap2N();
int set_prerot_headval( int );
int correct_image(float *,float *);
int expon_im(float *,float *,float,int);
int scale_im(float *,float *,float,float,int);
int subtract_drk(float *,float *,float *,float);
int subtract_im(float *,float *,float *,float,float);
int divide_insito_im(float *,float *);
int divide_im(float *,float *,float *);
int normint_im(float *,float *,int);
int make_grid(unsigned short *,float,float,int);
int mark_overflow(float *,float *,struct lut_descript *,float);
int mark_overflow_nocorr(float *,float *,long *,int);
int map_imag(void *,void **,double,double,int);
int region_compare(int,float,float,float,float,float,float,float,float,float,
  float,float,float);
int azim_int(float *,float *,float *,float,int,float,float,int,int,float,int);
int undistort_im(float *,float *,struct lut_descript *);
int minmax4(float,float,float,float,float *,float *);
int histcompare_count(const void *,const void *);
int histcompare_idx(const void *,const void *);
int debug_print(unsigned char *,LUT_TYPE *,unsigned short *,int,int,int,int,
  int);
int user_code(int,char *[]);
int scan_argument(char *,char *,char *,void *);
int prepare_flood(unsigned short *,float *);
int get_buffer(int,char *,void **,int *,int *,int);
int put_buffer(char *,void **,int,int,int);
int clean_buffer(void **,int,int);
char *outname(char *,char *,char *,char *);
int fnampat(char *,size_t,int,char *[]);
int analyse_args(int,char *[],char *);
int save_esrf_file(char *,void *,int,int,int,int);
int read_esrf_file(char *,void **,int *,int *,int,int,int *);
int bench(char *);
int despair(unsigned char *,LUT_TYPE *,int);
#if defined __cplusplus
  }
#endif
