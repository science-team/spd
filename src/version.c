/*
 *   Project: The SPD Image correction and azimuthal regrouping
 *			http://forge.epn-campus.eu/projects/show/azimuthal
 *            
 *   Copyright (C) 2009-2010 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: R. Wilcke (wilcke@esrf.fr)  
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.  
 *   If not, see <http://www.gnu.org/licenses/>.
 */

/*
Update 24/08/2009 R. Wilcke (wilcke@esrf.fr)
                  first working version.
Update 30/09/2009 R. Wilcke (wilcke@esrf.fr)
                  prntvers(): add input argument "progname" and print it in
                  the version information;
                  prntvers(): add date and time of compilation to the version
                  information.
*/

#include "spd.h"
char versistr[] = VERSION;

/*==============================================================================
 * This routine prints the version string of the program.
 *
 * The version string is handed over to the program in the C-preprocessor macro
 * "VERSION", which is defined as part of the compilation process.
 *
 * The routine is automatically recompiled when the version string changes.
 *
 * For details, see the corresponding Makefile.
 *
 * Input : none
 * Output: none
 * Return: none
 */


void prntvers(char *progname)
{
  prmsg(MSG,("%s version %s\n",progname,versistr));
  prmsg(MSG,("compiled %s, %s\n",__DATE__,__TIME__));
  return;
}
