/*
 *   Project: The SPD Image correction and azimuthal regrouping
 *			http://forge.epn-campus.eu/projects/show/azimuthal
 *
 *   Copyright (C) 2001-2010 European Synchrotron Radiation Facility
 *                           Grenoble, France
 *
 *   Principal authors: P. Boesecke (boesecke@esrf.fr)
 *                      R. Wilcke (wilcke@esrf.fr)
 *                      J. Kieffer (kieffer@esrf.fr)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published
 *   by the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   and the GNU Lesser General Public License  along with this program.
 *   If not, see <http://www.gnu.org/licenses/>.
 */

/*
Update 24/10/2011 P. Boesecke (boesecke@esrf.fr)
                  Default value of do_dark changed to 0. If not set explicitely
                  on the command line do_dark will be set to 1 by giving either
                  dark_file, dark_const or dark_id different from -1.
                  Flag NO_SHARED_MEMORY added to allow compilation on Windows.
                  strtok_r -> strlib_tok_r,
                  cor_ext default changed to ".cor.edf" (originally ".cor"),
                  because ".cor" cannot be opened by fit2d,
                  azim_ext default changed to ".azim.edf" (originally "")
Update 18/10/2011 P. Boesecke (boesecke@esrf.fr)
                  INVALID_TYP added, TMPTYP added.
Update 14/10/2011 P. Boesecke (boesecke@esrf.fr)
                  is_not_prerotpar added,
                  put_buffer: special condition to pass prerotation
                              parameters
Update 29/09/2011 P. Boesecke (boesecke@esrf.fr)
                  SDMTYP and moutfile added
Update 23/09/2011 P. Boesecke (boesecke@esrf.fr)
                  do_dist = 4 added
Update 12/09/2011 P. Boesecke (boesecke@esrf.fr)
                  get_buffer: offset-0.5 corrected for 
                              SDXTYP, SDYTYP
Update 10/09/2011 P. Boesecke (boesecke@esrf.fr)
                  sx parameters are only calculated in correct.c
                  when all header values are updated.
Update 09/09/2011 P. Boesecke (boesecke@esrf.fr)
                  get_buffer: if now buffer is specified for 
                  AZITYP, defaults are used (but currently only 
                  rough estimations).
                  The data type of the overflow parameter has been
                  changed from unsigned short to unsigned long.
                  Otherwise, it could not be used with detectors
                  having more than two byte integers per pixel
                  (e.g. pixel detectors). Because the overflow 
                  value is now detector dependent its default 
                  value has been changed from 0xffff to 0.
                  Otherwise it would have been necessary to 
                  set it explicitely to 0. 
Update 05/09/2011 P. Boesecke (boesecke@esrf.fr)
                  put_buffer: valset variable added
                  analyse_args: do_dist is only set to 0 if do_prerot is 0
                  get_buffer: default parameters for SDXTYP and SDYTYP
                  from SRCTYP
Update 02/09/2011 P. Boesecke (boesecke@esrf.fr)
                  Currently defined values of the raw data compression 
                  option raw_cmpr (DCompression values of cmpr.h):
                    "none" | "UnCompressed"   : no compression (default)
                    "gzip" | "GzipCompression : gzip compression
                    "z"    | "ZCompression    : Z compression
                  All unknown values set raw_cmpr to no compression.
                  The option "clear" resets now also the options
                  "raw_cmpr" and "do_prerotion".
Update 31/08/2011 P. Boesecke (boesecke@esrf.fr)
                  Add new option "raw_cmpr" 
Update 19/08/2011 P. Boesecke (boesecke@esrf.fr)
                  current_shm_data[MAXTYP] added to avoid calls 
                  to getShmData when the shared memory is
                  possibly already detached (see 27/07/2011). 
                  current_shm_data is updated and modified like 
                  current_shm.
Update 01/08/2011 P. Boesecke (boesecke@esrf.fr)
                  label error_ret -> get_buffer_error.
                  In all headers the default prerotation angles 
                  are 0. The prerotation parameters are only read 
                  for SRC and DRK headers. 
                  This allows to write the actual prerotation
                  parameters into the internal header of the SDX 
                  and SDY images. The validity of the LUT can then
                  be checked by comparing them with the prerotation
                  parameters in the SRC header: if do_prerotation
                  is set all prerotation parameters must coincide,
                  otherwise all preroation angles must be 0.
Update 27/07/2011 P. Boesecke (boesecke@esrf.fr)
                  getShmDataPtr can be called with a version number. The
                  data pointer needs sometimes to be determined when
                  the shared memory in not accessible any more.
                  This is a dirty workaround which should be fixed.
                  %x changed to %p for pointers.
Update 23/07/2011 P. Boesecke (boesecke@esrf.fr)
                  prerotation parameters imported by scanhead
Update 09/07/2011 P. Boesecke (boesecke@esrf.fr)
                  getShmDataPtr: This version of spd distinguishes 
                  automatically between different shared memory versions.
                  The function getShmDataPtr returns the start of the 
                  data section depending on the shared memory version.
                  analyse_args: only a single return point for debugging
Update 23/09/2010 R. Wilcke (wilcke@esrf.fr)
                  put_buffer(): add SPEC types SHM_ULONG and SHM_LONG to the
                  allowed data types and use edf_datatype2machinetype() to
                  convert them to the correct type for the computer used;
                  get_buffer(): add SPEC types SHM_ULONG and SHM_LONG to the
                  allowed data types for input buffers.
Update 03/06/2010 P. Boesecke (boesecke@esrf.fr)
                  command line option azim_pro added, azim_int updated,
                  azim_da, azim_a0, azim_a1 are expressed in degrees,
                  Because the arguments of azim_int need to be in radian they
                  are multiplied with NUM_PI/180.0, in put_buffer exception 
                  for AZITYP updated.
Update 02/12/2009 R. Wilcke (wilcke@esrf.fr)
                  replace the term "azimuthally integrated" by "azimuthally
                  regrouped" (likewise "integration" by "regrouping").
Update 16/11/2009 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): pass a copy of "tmpnam1" to the "basename()"
                  routine (the routine can change its argument);
                  analyse_args(): add command line option "ave_ext";
                  analyse_args(): put the prefix "SPD_" in front of the macros
                  for the return code (ERRFLG, BASUSE, ...) and move the
                  definitions to the file "spd.h".
Update            J. Kieffer 
                  get_time(), --server command line option added in MAIN
Update 06/10/2009 P. Boesecke (boesecke@esrf.fr)
                  SAXS_PI => NUM_PI
Update 30/09/2009 R. Wilcke (wilcke@esrf.fr)
                  add argument "progname" to all prntvers() calls.
Update 09/09/2009 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): set "inptbusy" to 0 when shared memories have
                  been copied (was set to 1 by mistake).
Update 27/08/2009 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): copy shared memory types SRCTYP, DRKTYP and
                  SBKTYP always to a newly allocated internal buffer;
                  define new global variable "inptbusy";
                  analyse_args(): set and reset "inptbusy" at the start and end
                  of the input data acquisition;
                  add routine getstate() to return the value of "inptbusy".
Update 24/08/2009 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): get version of the program from the new
                  routine "prntvers()", no longer from __DATE__ and __TIME__;
                  analyse_args(): add command line option "version" to print the
                  version string of the program. 
Update 06/05/2009 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): change "flat_distortion" default from 0 to 1;
                  analyse_args(): use the "outdir" value also for the "xoutfile"
                  and "youtfile" names.
Update 04/05/2009 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): rename variables: "do_later" to "flat_aft",
                  "do_spd" to "do_dist" and "do_flat" to "flat_dist";
                  set default values: "cor_ext" = ".cor" and "src_ext" = ".edf";
                  do not use directory path of source file for corrected file;
                  set directory path of "base_name" as default for "outdir".
Update 24/04/2009 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): modify code for the obsolete options "to_ext"
                  and "from"ext";
                  analyse_args(): add new command line argument "outdir" and
                  code to define a directory path for the output files. 
Update 17/04/2009 R. Wilcke (wilcke@esrf.fr)
                  get_filnam(): there may now be a prefix and postfix string
                  before and after the keyword string;
                  analyse_args(): if the dark file name does not contain a path,
                  use the one from the source image file.
Update 25/03/2009 P. Boesecke (boesecke@esrf.fr)
                  set_type(): add code to set type to default if input argument
                  is empty or NULL.
Update 16/02/2009 A. Gotz (andy.gotz@esrf.fr)
                  flag MAKE_FUNCTION added
Update 18/11/2008 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): acquire buffer for dark image only if needed.
Update 24/06/2008 R. Wilcke (wilcke@esrf.fr)
                  reset the values of the command line arguments "type" and
                  "dvo" to their defaults when input argument "clear" is given.
Update 16/06/2008 P. Boesecke (boesecke@esrf.fr)
                  analyse_args(): add new command line arguments "type" and
                  "dvo" and code (new routines set_type(), set_dvo()).
Update 22/01/2008 R. Wilcke (wilcke@esrf.fr)
                  slightly modify many error messages in the program;
                  _prmsg(): change the message text for WARNING, ERROR and FATAL
                  messages by printing the message type in front of the text;
                  get_buffer(): change test for incompatible image sizes: before
                  they had to be identical modulo binning, now the correction
                  images (e.g. dark, flood...) can be bigger than the source
                  image as long as they contain the source image fully.
Update 14/01/2008 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): multiply inp_max and inp_min with the software
                  binning factors.
Update 11/01/2008 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): change compatibility test of image to acquire
                  with source image: instead of requesting identical dimensions
                  modulo binning, test if source image is fully contained in
                  the image to be acquired.
Update 17/12/2007 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): correct x-scaling argument of map_imag() call;
                  put_buffer(): write PSize_2 value for AZITYP data in radian.
Update 11/12/2007 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): if there are several input files, do each time
                  a test for re-allocation of all other file buffers as well.
                  This may be necessary because of different binnings of the
                  input source file.
Update 06/12/2007 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): change code to obtain the name of the input
                  source file from a filename pattern template.
Update 28/11/2007 R. Wilcke (wilcke@esrf.fr)
                  get_buffer() and analyse_args(): change code to account for
                  the fact that map_imag() now also does the linearity
                  correction;
                  analyse_args(): apply linearity correction to dark constant.
Update 23/11/2007 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct error in getting the darkfile name.
Update 09/10/2007 R. Wilcke (wilcke@esrf.fr)
                  add new function get_filnam() to get the name of a file from
                  a keyword in the source image file;
                  analyse_args(): add code to read the name of the distortion
                  file from a keyword in the source image file, and read a
                  separate distortion file for each source image if this feature
                  is used.
Update 26/09/2007 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add code to read the name of the flood field
                  image file from a keyword in the source image file, and read a
                  separate flood field image file for each source image if this
                  feature is used.
Update 18/04/2007 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): test if the dimensions of the image processed
                  (modulo binning) do agree with the ones of the source image;
                  analyse_args(): change default value for "distfile" from
                  "spatial.dat" to the empty string;
                  analyse_args(): do not make distortion correction if neither
                  "distortion_file" nor "xfile" and "yfile" are specified.
Update 10/04/2007 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add code to read the name of the dark image
                  file from a keyword in the source image file, and read a
                  separate dark image file for each source image if this feature
                  is used;
                  analyse_args(): add new command line argument "do_dark" and
                  code to suppress dark image correction.
Update 13/02/2007 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): for the update of the output image header, do
                  no longer use the special enumerated data, but the general
                  flags for the user data header;
                  analyse_args(): introduce a new image type CMDTYP to store
                  the header values that are defined on the command line;
                  analyse_args() and scanhead(): set the command-line defined
                  values for source and dark image header in scanhead() instead
                  of analyse_args().
Update 26/09/2006 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new command line argument "inp_exp" and
                  code for linearity correction (apply a constant exponent to
                  all values of an input image);
                  analyse_args(): modify calls of mark_overflow_nocorr() to add
                  new argument containing the list with the dummy pixels.
Update 07/06/2006 R. Wilcke (wilcke@esrf.fr)
                  user_code(): declare function as "int" and return a value (to
                  be consistent with the declaration in "spd.h");
                  get_buffer(): for AVETYP data from shared memory: add check
                  for the "rows" size of the buffer;
                  analyse_args(): add new command line argument "azim_a1" and
                  code for the azimuthal averaging over the second angle range;
                  analyse_args(): for dark image data coming from a shared
                  memory buffer and then saved to a file, add the name of this
                  file to the header values of the source file (this will also
                  put it into the header of the corrected image file);
                  analyse_args(): correct an error in the freeing of the source
                  image buffer after re-mapping.
Update 26/05/2005 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): force mapping of image also if dimension is
                  different from the source image.
Update 04/04/2005 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(), put_buffer() and help_arg(): add new command
                  line argument "azim_pass" to pass the full source image header
                  to the azimuthally regrouped output file.
Update 01/04/2005 R. Wilcke (wilcke@esrf.fr)
                  scanhead(), analyse_args() and read_esrf_file(): print info
                  concerning header values directly in scanhead();
                  scanhead(): move to this routine from routine set_headval()
                  (file "correct.c") the initialization of header values that
                  are not set by the input data.
Update 24/03/2005 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): force re-acquisition of image also if dimension
                  has changed.
Update 22/03/2005 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): write the file name of the input image to the
                  history only if input is indeed from a file;
                  get_buffer(): change how type == HD_TYP is skipped in the test
                  for undefined dimensions.
Update 09/03/2005 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): force re-acquisition of image not only if
                  binning has changed, but also if offset has changed.
Update 15/02/2005 R. Wilcke (wilcke@esrf.fr)
                  main(): return exit code EXIT_SUCCESS if the call to
                  analyse_args() is successful or EXIT_FAILURE if not.
Update 25/01/2005 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): handle new header update values "ofs1",
                  "ofs2", "bsz1" and "bsz2";
                  scanhead(): for data type SDXTYP and SDYTYP, store the values
                  for the displaced parameters into the user header.
Update 24/01/2005 R. Wilcke (wilcke@esrf.fr)
                  __main(): declare function as type "int" and do "return(0)";
                  put_buffer(): write "DetectorRotation_n" values to EDF header
                  in degree with "_deg" suffix.
Update 13/01/2005 R. Wilcke (wilcke@esrf.fr)
                  change header key words back: Psize_1 -> PSize_1,
                  Psize_2 -> PSize_2;
                  analyse_args(): remove "ave_waxs" command line argument and
                  all code referring to it, including as argument to the
                  azim_int() call (was replaced by "pro", see below);
                  analyse_args(): remove command line arguments "flat_after"
                  (replaced by new values of "do_distortion") and
                  "center_distort" (replaced by new values of "psize_distort");
                  analyse_args(): add new values "2" and "3" for command line
                  argument "do_distortion" and code to process them;
                  analyse_args(): add new values "2" for command line argument
                  "psize_distort" and code to process it;
                  analyse_args(): add header keys "ProjectionType",
                  "DetectorRotation_1", "DetectorRotation_2" and
                  "DetectorRotation_3", add command line arguments "pro",
                  "rot_1", "rot_2" and "rot_3" to set them from the command line
                  and hand them to the correction routines;
                  scanhead(): get the values for the new header keys from the
                  header;
                  put_buffer(): add the new header keys to the output data;
                  help_arg(): add help text for the new arguments.
Update 21/09/2004 R. Wilcke (wilcke@esrf.fr)
                  change bin_imag() call to map_imag();
                  get_buffer(): re-map the geometry of the image also in the
                  case that the offsets are different from the source image.
Update 14/09/2004 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): use clean_buffer() to remove data buffer if no
                  input source for input data types.
Update 07/09/2004 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): always set old_where[type] = no_stor if there
                  is neither a file nor a shared memory for an output data type.
Update 30/08/2004 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): call set_imgbuf() only if the buffer pointer
                  has been updated by get_buffer();
                  get_buffer(): move the prepare_flood() call to set_imgbuf()
                  (file correct.c).
Update 25/08/2004 R. Wilcke (wilcke@esrf.fr)
                  put_buffer(): do not copy all of online header to saved dark
                  image file header;
                  analyse_args(): add commandline option header_min to set
                  minimum data header length for output files;
                  analyse_args(): add command line option ave_waxs and add it as
                  input parameter to the azim_int() call;
                  analyse_args(): add command line option center_distort and
                  modify the code to allow setting of center coordinates and
                  sample distance in addition to pixel size.
Update 24/08/2004 R. Wilcke (wilcke@esrf.fr)
                  fprint_first_el(): corrected format for "printf();
                  put_buffer(): remove unused variables;
                  main(), clean_buffer(): add "return(0)" at the end of the
                  routine;
                  analyse_args(): the code that copies the input arguments to
                  the history header has been moved into scan_argument() (file
                  util.c).
Update 15/06/2004 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): modify tests for "cols" and "rows";
                  scanhead(): replace calls to atof() by calls to num_str2long()
                  or num_str2double().
Update 29/03/2004 R. Wilcke (wilcke@esrf.fr)
                  scanhead(): get header values BSize_1, BSize_2, Dim_1 and
                  Dim_2;
                  put_buffer(): update header information with the values of
                  BSize_1, BSize_2, Dim_1 and Dim_2 in the user's header
                  structure.
Update 19/03/2004 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new commandline options to set data header
                  values: bis_1, bis_2;
                  analyse_args(): get buffer for corrected image after the
                  buffers for dark, floodfield and scattering background images;
                  read_esrf_file(): remove test for image dimensions (is done in
                  get_buffer());
                  get_buffer(): add code for binning;
                  use function set_imgbuf() instead of separate calls to
                  set_bckgim(), set_drkim() and set_floim().
Update 02/02/2004 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new command line option header_ext to save
                  the header of the input source image to a file;
                  put_buffer(): allow type HD_TYP to save the header data to a
                  special output file.
Update 14/01/2004 R. Wilcke (wilcke@esrf.fr)
                  _prmsg(): add handling of variable length argument list with
                  "stdarg" for ANSI C compilers.
Update 04/11/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct format for writing arguments "dummy"
                  and "src_id" into history.
Update 16/09/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): write history parameters in the same form as
                  they are specified on the command line;
                  analyse_args(): add "dummy" command line argument to history.
Update 31/07/2003 R. Wilcke (wilcke@esrf.fr)
                  put_buffer() and save_esrf_file(): write the history already
                  in put_buffer() to the internal header structure.
Update 21/07/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new command line option norm_factor to set
                  a multiplication factor for the scattering intensity
                  normalization;
                  analyse_args(): add the scattering intensity normalization
                  factor as second argument to the set_normint() function.
Update 18/07/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct error in the construction of the
                  history for the corrected image.
Update 12/03/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct error in the logic for looping over
                  several input source image files.
Update 11/03/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): if there are header values defined on the
                  command line, get the present value of the header flags first
                  before ORing with the flags for the command line values.
Update 27/02/2003 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): set BASUSE flag only if the output buffer was
                  successfully written;
                  analyse_args(): write corrected output file only if command
                  line option "cor_ext" is set.
Update 17/12/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new commandline options to set data header
                  values i0 and i1.
Update 12/12/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct file name for azimuthal regrouping
                  for the online case;
                  analyse_args(): correct the loop over the input data files;
Update 28/11/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): put the help text for command line arguments
                  in a new function help_arg();
                  help_arg(): do the printout with two new macros PRNTHELP and
                  PRNTHLPN.
Update 26/11/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): change code to integrate both the "files
                  input" and the "shared memory input" into one correction loop;
                  analyse_args(): add new commandline options to set data header
                  values: cen_1, cen_2, off_1, off_2, pix_1, pix_2, dis, ori,
                  tit, wvl;
                  analyse_args(): change argument values in set_psizdist() call.
                  analyse_args(): add new commandline option mask_file to define
                  a mask of pixels to ignore for the azimuthal calculations.
Update 24/10/2002 R. Wilcke (wilcke@esrf.fr)
                  put_buffer() and get_buffer(): add code to handle image data
                  of type "unsigned character".
Update 11/10/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): close all open EDF data files when "clear";
Update 26/09/2002 R. Wilcke (wilcke@esrf.fr)
                  move routine _prmsg() from "correct.c" to "inout.c";
                  make "verbose" a global variable;
                  analyse_args(): remove calls to set_verbose();
                  _prmsg(): add code to handle the new message flag PRERR.
Update 17/09/2002 R. Wilcke (wilcke@esrf.fr)
                  put_buffer(): return with error if save_esrf_file() fails.
Update 16/09/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): give error message if requested number of rows
                  or columns is different from the one found in shared memory;
                  reallocate the message levels for the prmsg() calls.
Update 05/09/2002 R. Wilcke (wilcke@esrf.fr)
                  scanhead() and put_buffer(): add code for new "data_head"
                  member "ExpTime";
                  scanhead() and put_buffer(): change code to treat "data_head"
                  members "Intens_0" and "Intens_1" as string instead of
                  "float".
Update 03/09/2002 R. Wilcke (wilcke@esrf.fr)
                  change references from "background correction" to "dark image
                  correction, this includes changing the parameter names
                  bkg_const, bkg_file, bkg_id to dark_const, dark_file, dark_id;
                  rename enumerated variable BKGTYP to DRKTYP;
                  change function calls set_bkgconst() and set_bkgim() to
                  set_drkconst() and set_drkim();
                  change parameter and variable names for "scattering background
                  correction", this includes changing the parameter names
                  scat_const, scat_fact, scat_file, scat_id to bckg_const,
                  bckg_fact, bckg_file, bckg_id;
                  rename enumerated variable SCATYP to SBKTYP;
                  change function calls set_scaconst(), set_scafact() and
                  set_scaim() to set_bckgconst(), set_bckgfact and set_bckgim().
Update 13/06/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): change default of "psize_distort" from 1 to 0;
                  analyse_args(): put as program name the content of the input
                  argument "progname" instead of the fixed string "corimg_corr"
                  into the history.
Update 10/06/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): added parameters "-min", "-max", "-pass" and
                  "-pix_file" to the history string.
Update 10/06/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): changed the syntax of the history string.
Update 05/06/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): added dark constant to history.
Update 04/06/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): create a character string describing the
                  processing that is done with the image and write it to the
                  "History" keywords of the output data file;
                  analyse_args(): initialize "inpfact" to 1. (was 0. before).
Update 03/06/2002 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file(): declare "bytebuf" as a buffer with length
                  "EdfMaxLinLen + 1" instead of declaring it a pointer and
                  assigning memory later;
                  get_buffer() and put_buffer(): declare "id" and "val" buffers
                  with length "EdfMaxKeyLen + 1" and "EdfMaxValLen + 1".
Update 31/05/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): free HD_TYP buffers if there is no online
                  header for the image;
                  read_esrf_file(): replace machine_sizeof() call by
                  edf_machine_sizeof() (change in "edfio" library);
                  get_buffer(), analyse_args(), save_esrf_file() and
                  read_esrf_file(): add calls to read, modify and write the
                  HISTORY keywords in the data header.
Update 21/05/2002 R. Wilcke (wilcke@esrf.fr)
                  clean_buffer(): free buffer only if "free_flag" is set (before
                  buffer was always freed for shared memory access);
                  get_buffer(): set "current_shm[type]" to NULL if the file has
                  changed, and also in case of error return;
Update 14/03/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): force reading of the input file if the data
                  buffer is a NULL pointer.
Update 13/03/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): allow "spatial distortion displacement" data
                  types;
                  put_buffer(): do no longer create / reset the internal header
                  and history buffer for "spatial distortion displacement" data
                  types;
Update 12/03/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): change values for "old_where" from integer to
                  enumeration constants;
                  move declaration of global variable "typestr" and definition
                  of macro MAXTYP to "spd.h";
                  get_buffer(): use 0 and MAXTYP to test for valid values of the
                  "type" input argument;
                  get_buffer() and put_buffer(): replace data type SPDTYP by the
                  two new data types SDXTYP and SDYTYP.
Update 11/03/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): return error if file name given for output or
                  header buffer;
                  get_buffer(): correct return status (1 if buffer has changed,
                  0 if not, code -1 for error has not changed);
                  analyse_args(): use corrected return status of get_buffer();
                  get_buffer(): create or reset an internal history buffer for
                  each data buffer;
                  read_esrf_file(): read the history information from the file
                  into the internal history buffer;
                  analyse_args(): add information about the processing of the
                  corrected and azimuthally regrouped images to the internal
                  history buffer;
                  save_esrf_file(): write the internal history buffer to the
                  output file.
Update 06/03/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): put in maximum field width in the sscanf() calls
                  to prevent buffer overflow.
Update 05/03/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): change test for new file;
                  get_buffer(): reset internal header buffer only if input data
                  have changed;
                  analyse_args(): obtain values from the online header only if
                  there is also an online source image;
                  scanhead() and put_buffer(): add code for new "data_head"
                  members "Title" and "Time".
Update 04/03/2002 R. Wilcke (wilcke@esrf.fr)
                  rename global variable NO_TYP to HD_TYP;
                  put_buffer() and save_esrf_file(): change code to allow the
                  SPDTYP data type.
Update 01/03/2002 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file(): allocate a new data buffer only if needed,
                  i.e. if the buffer pointer is NULL.
Update 21/02/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): set "distortion_file" value only if "xfile"
                  and "yfile" are not specified;
                  analyse_args(): set the values for "distortion_file", "xfile",
                  "yfile", "xoutfile" and "youtfile" only if "do_distortion" is
                  set.
Update 24/01/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct the determination of the return value;
                  analyse_args(): compare with -1 instead of NULL when testing
                  for a not defined "bkgid" and "srcid";
                  analyse_args(): set the SPD_ERRFLG if a requested data buffer
                  cannot be obtained;
                  analyse_args(): set new flag BASUSE in the return value to
                  indicate if the "base_name" was used in creating a file;
Update 16/01/2002 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): do not call the correction routines if there
                  is no filename or shared memory specified for the results;
                  redefine the return value to specify which files were created.
Update 10/01/2002 R. Wilcke (wilcke@esrf.fr)
                  remove routine fprint_header().
Update 09/01/2002 R. Wilcke (wilcke@esrf.fr)
                  scanhead(): use new EDF-routine edf_search_header_element()
                  instead of find_header_element();
                  remove routine find_header_element().
Update 08/01/2002 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): do not free the internal header buffer for the
                  online header before reading the header;
                  analyse_args(): correct handling of "save_dark" parameter;
                  find_header_element(): correct arguments to strcasecmp()
                  call: second argument must be "key", not "value";
                  scanhead(): change "val" from (const char **) to
                  (const char *).
Update 20/12/2001 R. Wilcke (wilcke@esrf.fr)
                  save_esrf_file(): remove last input argument (title), add
                  "type" as new 5th argument, and change code accordingly;
                  save_esrf_file(): do not access internal header for SPDTYP;
                  put_buffer(): remove last argument (title) in save_esrf_file()
                  call and add "type" as new 5th argument.
                  get_buffer(): do no longer create a header stream for the
                  online header information, put it directly into the internal
                  header buffer;
                  remove global variable HDSTREAM (no longer needed);
                  make global variable HEADER_BUF a local one in get_buffer(),
                  rename it to "head_buf" and change code accordingly;
                  analyse_args(): set default of "dark_ext" to empty string;
                  get_buffer(): add new return value "1", return "0" if a new
                  data buffer was obtained and "1" if the old buffer is still
                  valid;
                  analyse_args(): change code to account for new return value of
                  get_buffer();
Update 19/12/2001 R. Wilcke (wilcke@esrf.fr)
                  add new routine find_header_element() to find the value of
                  a specified keyword in the header buffer;
                  scanhead(): change code to use the new find_header_element()
                  routine, and change first input argument to the type of the
                  image instead of the data stream;
                  analyse_args() and read_esrf_file(): change the arguments in
                  the call to scanhead();
                  read_esrf_file(): put the file header always in the
                  corresponding internal header buffer, and for SRCTYP also into
                  the NO_TYP header buffer;
                  save_esrf_file(): remove the input argument HDSTREAM;
                  put_buffer(): remove the input argument HDSTREAM in the call
                  to save_esrf_file();
                  analyse_args(): change the "while" loop over the input files
                  to a "do ... while" loop, and "continue" if the output file
                  name for a particlular input file could not be constructed;
                  analyse_args(): copy a restricted set of the source image
                  header values to the dark image.
Update 18/12/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): make "headrows" and "headcols" non-static and
                  do not initialize them.
Update 17/12/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): move the azimuthal regrouping and averaging
                  directly behind the correct_image() calls (2 locations!!!);
                  put_buffer(): modify outptr from (void **) to (void *) and
                  change code accordingly (old version crashed the program).
Update 13/12/2001 R. Wilcke (wilcke@esrf.fr)
                  put_buffer(): write units after the header values;
                  put_buffer(): write the full header of the input image only
                  to the corrected image and the source image, not to e.g. the
                  azimuthal regrouped image.
Update 13/12/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(), put_buffer() and read_esrf_file(): write the
                  header information into the "NO_TYP" instead into the "SRCTYP"
                  buffer and get it back from there for the output.
Update 11/12/2001 R. Wilcke (wilcke@esrf.fr)
                  put_buffer(): read online header values from the internal
                  buffer;
                  scanhead(): modify the way the header keywords are read;
                  put_buffer(): write all values of the CORTYP header back to
                  the online header;
Update 10/12/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(), put_buffer() and save_esrf_file(): create the
                  internal header buffer only in get_buffer() with
                  edf_new_header();
                  put_buffer(): write the elements of the header structure
                  directly with edf_add_header_element() into the internal
                  header buffer;
                  put_buffer() and save_esrf_file(): decide in put_buffer() if
                  the online header information is written to the output file.
Update 04/12/2001 R. Wilcke (wilcke@esrf.fr)
                  set errno = 0 for all calls to prmsg() with error types
                  FATAL and ERROR that do not correspond to system errors;
                  analyse_args(): add new options "src_ext" and "cor_ext",
                  remove option "save_src" and mark options "cor_file",
                  "from_ext" and "to_ext" as obsolete;
Update 03/12/2001 R. Wilcke (wilcke@esrf.fr)
                  use the size of the array "typestr" to set the dimensions of
                  "current_shm", "old_file", "old_where", "last_mtime" and
                  "last_utime";
                  analyse_args(): use __DATE__ and __TIME__ for the printout
                  of the current version;
                  analyse_args(): add command line options "ave_id" and
                  "ave_scf";
                  add new type "averaged" to the array "typestr";
                  analyse_args(), get_buffer() and put_buffer(): add code to
                  handle the new buffer type AVETYP;
                  analyse_args(): add the "averaged buffer" and the scale factor
                  to the azim_int() input arguments.
Update 30/11/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer() and analyse_args(): move the set_xysize() call
                  from get_buffer() to analyse_args();
                  get_buffer(): modify the tests for the dimensions of the
                  image buffer to be acquired;
                  analyse_args(): change command line options save_raw ->
                  save_src, raw_ext ->src_ext;
                  analyse_args(): add command line option "azim_id";
                  analyse_args(): always detach from SRCTYP buffers at the end
                  of the routine (was only for srcid != -1).
Update 29/11/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): add "rows" and "cols" as input arguments;
                  change all calls to get_buffer() by adding "rows" and "cols";
                  get_buffer() and read_esrf_file(): test input "rows" and
                  "cols" against values of the image and return with error if
                  they do not agree;
                  get_buffer(): add AZITYP to the allowed data types and tread
                  it as CORTYP;
                  change some header key words: PSize_1 -> Psize_1,
                  PSize_2 -> Psize_2, Orientation -> RasterOrientation;
                  analyse_args(): add command line option "azim_ext".
Update 28/11/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add command line option "base_name";
Update 27/11/2001 R. Wilcke (wilcke@esrf.fr)
                  put_buffer() and get_buffer(): make "typestr" a global array;
                  put_buffer(), get_buffer() and save_esrf_file(): replace the
                  fixed header name "src_head" by the string in "typestr";
                  put_buffer(): change code to put the correct header values
                  for source and dark images into the file;
                  put_buffer(): change code to save AZITYP images;
                  analyse_args(): made the allocated AZITYP buffer 4 rows bigger
                  to store the "s" and averaged values.
Update 26/11/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): for the azimuthal regrouping, use the radial
                  and angular dimensions instead of the end values, replace the
                  command line arguments azim_a1 and azim_r1 by azim_a_num and
                  azim_r_num, and change the input arguments to azim_int()
                  accordingly;
                  add two new arguments to the put_buffer() call to define the
                  dimensions of the buffer, and change the calls accordingly;
Update 20/11/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): protect against a NULL return from outname();
Update 19/11/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new additive and multiplicative constants
                  to adjust the values of the input image and of the scattering
                  background image;
                  analyse_args(): replace variable names "in_ext" and "out_ext"
                  by "from_ext" and "to_ext";
Update 16/11/2001 R. Wilcke (wilcke@esrf.fr)
                  add new command line argument "psize_distort" and the code to
                  get it from the user and hand it to the correction routines.
Update 15/11/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer() and save_esrf_file(): replace references to the
                  "onl_head" buffer by "src_head";
                  put_buffer() and save_esrf_file(): move creation of the
                  internal header buffer and copying of the online header from
                  save_esrf_file() to put_buffer();
                  put_buffer(): change the way the header keywords and values
                  are handled for the updating of the internal header buffer and
                  the online header;
                  analyse_args(): save the source and dark current shared
                  memory segments to files if requested.
Update 13/11/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): improve the loop processing for the reading of
                  the online header;
                  put_buffer(): write the current values of the CORTYP header
                  structure back to the online header, if it exists;
Update 12/11/2001 R. Wilcke (wilcke@esrf.fr)
                  save_esrf_file(): change the way the header values from the
                  user's header structure are written into the data file: write
                  them into a buffer with sprintf(), then use
                  edf_write_header_line().
Update 08/11/2001 R. Wilcke (wilcke@esrf.fr)
                  split the code of "util.c" in two files:
                  - "inout.c" contains all input/output related routines,
                  - "util.c" contains the other routines of the old "util.c".
Update 08/11/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new command line arguments "inp_min" and
                  "inp_max" and code to hand them to the correction routines.
Update 05/11/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add 2 more new arguments to azim_int() call
                  and code to get their values from the user.
Update 31/10/2001 R. Wilcke (wilcke@esrf.fr)
                  user_code(): add call to set_return_value() to return a status
                  to the calling program (i.e. "spec).
Update 22/10/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add 3 new arguments to azim_int() call and
                  code to get their values from the user;
Update 19/10/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): change test for correct_image() return value;
                  analyse_args(): add code to implement azimuthal regrouping.
Update 16/10/2001 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file(): create a new header module for the source
                  image header before reading values into it;
                  move the closing of the source image header module from
                  save_esrf_file() to get_buffer();
                  save_esrf_file(): create a new header module for the online
                  image header before reading values into it;
                  get_buffer(): close both the source and the online header
                  module whenever the input source file or the online header has
                  changed;
                  read_esrf_file(): always read new values into the header
                  module, even if "headpass" is 0.
Update 03/10/2001 R. Wilcke (wilcke@esrf.fr)
                  save_esrf_file(): change last input argument to contain the
                  EDF I/O stream for the temporary online header and modify
                  code accordingly;
                  put_buffer(): change last argument in save_esrf_call() to
                  the EDF I/O stream for the temporary online header;
Update 02/10/2001 R. Wilcke (wilcke@esrf.fr)
                  scanhead(): get the values for the header structure from the
                  temporary "HDSTREAM" header;
                  scanhead(): change the first input argument from a pointer to
                  the header buffer ("char *header") to the stream descriptor
                  for the EDF data I/O ("int stream"), and change function
                  declaration accordingly;
                  analyse_args(): change the first argument of the scanhead()
                  call from the buffer pointer to the header stream descriptor;
                  analyse_args(): pre-set "Dummy" for the corrected image from
                  the online header;
                  read_esrf_file(): replace the code to read the header keywords
                  by a call to scanhead();
                  rename HEADER_NO and HEADER_LEN to "headrows" and "headcols",
                  make them local variables in scanhead() and remove them
                  anywhere else;
                  save_esrf_file(): remove the last two input arguments and test
                  for "HDSTREAM != -1" to find out if there is an online header;
                  put_buffer(): remove the last two arguments in the call to
                  save_esrf_file().
Update 01/10/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): free temporary header when "HDSTREAM" is closed.
Update 28/09/2001 R. Wilcke (wilcke@esrf.fr)
                  added new global variable "HDSTREAM" for a temporary EDF
                  header;
                  get_buffer(): added code to read the online header and to put
                  it into the temporary EDF header;
                  analyse_args(): set "HDSTREAM" to -1 if "clear" parameter is
                  given on the command line;
                  save_esrf_file(): add code to get the online header from the
                  temporary EDF header and write it to the output file header;
Update 25/09/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add new command line parameter "pass" and the
                  code to set it;
                  read_esrf_file() and save_esrf_file(): add code to read the
                  entire header from the source file and to write it to the
                  corrected image file;
                  scanhead() and save_esrf_file(): replace edf_maxkeylen() and
                  edf_maxvallen() calls by macros EdfMaxKeyLen and EdfMaxValLen;
                  read_esrf_file(): replace edf_maxlinlen() call by macro
                  EdfMaxLinLen.
Update 24/09/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): do no longer test the return value of
                  scanhead();
                  analyse_args(): change code to have correct setting of header
                  values and output "Dummy" value;
                  read_esrf_file(): move the code with the label "badheader" to
                  the end of the routine.
Update 14/09/2001 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file(): change the opening mode for the file in the
                  edf_open_data_file() call from "old" to "read";
                  analyse_args(): write an output file only if correct_image()
                  was successful;
                  analyse_args(): clear the flags of the SRCTYP header structure
                  before getting the new header values if the image is from a
                  shared memory.
Update 20/08/2001 R. Wilcke (wilcke@esrf.fr)
                  add declaration for function scanhead();
                  save_esrf_file(): put the initialized values of the CORTYP
                  header structure in the output file;
                  analyse_args(): declare "src_im" as static and do not free the
                  buffer when processing input source files;
                  analyse_args(): set HEADER_NO and HEADER_LEN to 0 if no online
                  header;
                  analyse_args(): test if the command line argument "dummy" is
                  set, and replace "Dummy" by the value in the input file only
                  if it is not set;
                  get_buffer() and analyse_args(): set the "Dummy" value from
                  the input source file, and reset it again to the value of
                  the command line if this is set. This is a kludge!  It should
                  be done with a proper flag signaling when "Dummy" is set;
Update 17/08/2001 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file() and scanhead(): fill the "init" member of the
                  header structure with the new flags indicating which keywords
                  have been found;
                  read_esrf_file(): transfer the header structure also for type
                  "SPDTYP";
                  analyse_args(): read input source image before all other
                  images to define the dimensions of the images to process;
                  get_buffer(): call prepare_flood() only after testing for the
                  correct size of the flood field image;
                  analyse_args(): do not reset the image buffers to NULL when
                  executing "clear";
                  get_buffer(): add variable "shm_changed" to flag if the shared
                  memory segment has changed, and call prepare_flood() only if
                  "shm_changed || file_changed" is true;
                  scanhead(): do no longer return an error if not all header
                  elements are found;
                  scanhead() and read_esrf_file(): get additional header
                  keyword "WaveLength".
Update 14/08/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): if there is a shared memory header provided
                  and the command line option "dummy" is not given, use the
                  "Dummy" value in the shared memory header to set "Dummy" for
                  the output file (with set_dummy());
                  read_esrf_file(): add input argument "type" in 5th position;
                  read_esrf_file(): read the values for the structure type
                  "data_head" from the header of the file and, if successful,
                  transfer the structure to the corresponding image type header
                  structure in the correction routines (except for SPDTYP);
                  get_buffer(): change the call to read_esrf_file() to include
                  the new "type" argument.
Update 13/08/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add command line argument "clear" and the
                  corresponding code to reset all option values to their default
                  values;
                  analyse_args(): add the second argument to the set_headval()
                  call to put the online header into the SRCTYP header;
Update 10/08/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): rename variable "filename" to "distfile",
                  declare it static and initialize it to "spatial.dat";
                  analyse_args(): declare as static the variables simul_flag,
                  do_spd, do_flat, do_later, verbose, norm_int, overf, bkgconst,
                  dummy and arad and give them the required initialization
                  values;
                  scanhead() and save_esrf_file(): change format for reading of
                  header from "%s" to "%[^\n]" as the string can contain blanks;
                  analyse_args(): correct the code to get the "simul_flag"
                  option (it got "corid" instead) and move the code for the grid
                  simulation directly behind the loop over the option arguments;
                  analyse_args(): transfer the parameters to the correction
                  routines only after the end of the loop over the option
                  arguments.
Update 09/08/2001 R. Wilcke (wilcke@esrf.fr)
                  increase dimensions of current_shm[], old_where[], old_file[],
                  last_mtime[] and last_utime[] from 5 to 6 and add
                  initialization elements;
                  analyse_args(): change code for the background corrections:
                  now the background constant can be specified in addition to
                  any of the two other background options.
Update 08/08/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): in the "error_ret", free the buffer if it has
                  been allocated;
                  analyse_args(): rename "input_file" to "srcfile";
                  analyse_args(): remove declaration of shm_src, shm_cor,
                  shm_flo and shm_bkg;
                  analyse_args(): test if the get_buffer() calls have been
                  successful, and return with an error if not;
                  analyse_args(): test for "srcid != -1" instead of "src_im" for
                  the shared memory case;
                  analyse_args(): move the get_buffer() call for the source
                  image into the code for the shared memory case - the file case
                  has already its own get_buffer();
                  analyse_args(): give the get_buffer() call for the source
                  image a "srcid" = -1 for the file case and a "srcfile" = NULL
                  for the shared memory case;
                  main(): print an error message if analyse_args() return != 0.
Update 07/08/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): add code to get a buffer for the scattering
                  background image and hand it over to the correction routines;
                  get_buffer(): add code for obtaining a buffer for the
                  scattering background image;
                  clean_buffer(): do not free an empty buffer pointer;
                  analyse_args(): clean the source image buffer at the end of
                  the routine (did not happen before if there was no valid
                  buffer for the corrected image);
                  rename global variable COR_FILE to "corfile" and make it a
                  local variable in analyse_args().
Update 06/08/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): initialize "dummy" to 0., and add code to
                  select the correct value of "dummy" - this parameter might be
                  given on the command line and in the user-header.
Update 03/08/2001 R. Wilcke (wilcke@esrf.fr)
                  add routine scanhead() to get the header keywords and values
                  from the header shared memory segment and put them in the
                  header structure;
                  analyse_args(): read header from shared memory segment
                  directly after the source image has been read, get the values
                  using scanhead() and hand them to the correction routines
                  with set_headval().
Update 02/08/2001 R. Wilcke (wilcke@esrf.fr)
                  move declaration of structure current_shm to the beginning of
                  the code;
                  analyse_args(): add command line argument "norm_int" and the
                  code to read it in and pass it to the correction routines;
                  get_buffer(): change order of arguments in the set_xysize();
                  get_buffer(): declare return type of function as "int".
Update 01/08/2001 R. Wilcke (wilcke@esrf.fr)
                  move the get_buffer() and clean_buffer() calls for the header
                  shared memory segment (HEADER_ID) from  put_buffer() to
                  analyse_args();
                  rename global variable HEADER_ID to "headid" and make it local
                  to routine analyse_args();
                  rename local variable header_buf to HEADER_BUF and make it
                  global;
Update 26/07/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): correct the values given to HEADER_NO and
                  HEADER_LEN;
                  save_esrf_file(): change the format in the sscanf() call of
                  the header buffer to remove leading blanks in the values;
                  save_esrf_file(): update comments about the header keywords
                  that are automatically written (EDF_DataFormatVersion and
                  EDF_DataBlocks keywords removed);
                  save_esrf_file(): change reading of the header buffer so that
                  it can work with two different formats of the header buffer.
Update 28/06/2001 R. Wilcke (wilcke@esrf.fr)
                  use routines set_bkgim() and set_floim() to hand pointers to
                  the background image and the flood field image to the image
                  correction routines;
                  change call to correct_image() by eliminating the background
                  image and the flood field image from the input arguments.
Update 26/06/2001 R. Wilcke (wilcke@esrf.fr)
                  correct the comments explaining "xfile" and "yfile";
                  analyse_args(): add command line arguments "xoutfile" and
                  "youtfile" and the code to read them in and pass them to the
                  correction routines;
Update 25/06/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): add missing string argument in prmsg() call in
                  test for correct image size;
                  remove declaration of variables X_COR, Y_COR, DO_XYFILE and
                  SPLINE_INVALID;
                  remove declaration of variables XOFFSET and YOFFSET and all
                  code that uses them (they do not seem to serve any purpose in
                  the image correction);
                  remove declaration of variable COR_FORMAT and all related
                  code, the output format is now always EDF;
                  remove declaration of the parameters for the geometrical
                  method (CURV_RADIUS, XCENTER and YCENTER) and all related
                  code. This method is no longer used;
                  remove function save_mar_file(), it is no longer needed;
                  analyse_args(): change type of "bkgconst" from "int" to
                  "float" and change format argument in scan_argument()
                  accordingly;
                  remove declaration of variable LUT_INVALID (this is now an
                  internal variable of the functions in "correct.c");
                  use new function calls to set the values of former global
                  variables:
                  - set_verbose() for "verbose";
                  - set_overflow() for IMAGE_OVER and IMAGE_OVER_SET;
                  - set_dummy() for Dummy;
                  - set_actrad() for ACTIVE_R;
                  - set_splinfil() for FILENAME;
                  - set_xycorin() for XFILE and YFILE;
                  - set_xysize() for XSIZE and YSIZE;
                  - set_bkgconst() for BKG_CONST;
                  use new function calls to obtain the values of former global
                  variables:
                  - get_xsize() for XSIZE;
                  - get_ysize() for YSIZE;
Update 19/06/2001 R. Wilcke (wilcke@esrf.fr)
                  initialize global variable COR_FILE to NULL.
Update 18/06/2001 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file(): remove reading of BYTEORDER header keyword
                  and the code to do forced byteswapping;
                  analyse_args(): remove code for setting the global variable
                  SRC_SWAP for forced byteswapping;
                  remove global variable SRC_SWAP.
Update 12/06/2001 R. Wilcke (wilcke@esrf.fr)
                  change variable name "DUMMY" to "Dummy" to avoid name conflict
                  with macro DUMMY().
Update 14/05/2001 R. Wilcke (wilcke@esrf.fr)
                  save_esrf_file(): completely rewritten to use the "ESRF Data
                  Format" access routines of P. Boesecke;
                  put_buffer(): replace SHM_FLOAT by MFloat in the call to
                  save_esrf_file();
                  remove struct "esrf_id" (no longer needed).
Update 11/05/2001 R. Wilcke (wilcke@esrf.fr)
                  remove gethead() routine - no longer needed.
Update 10/05/2001 R. Wilcke (wilcke@esrf.fr)
                  read_esrf_file(): completely rewritten to use the "ESRF Data
                  Format" access routines of P. Boesecke;
                  get_buffer(): replace SHM_FLOAT by MFloat in the call to
                  read_esrf_file();
                  get_buffer(): do no longer allocate a buffer for the image
                  (this is done in read_esrf_file());
                  get_buffer(): test if the shared memory segment has changed,
                  and allocate a new data buffer only if it has.
Update 25/04/2001 R. Wilcke (wilcke@esrf.fr)
                  analyse_args(): correct comments for "flat_distortion"
                  parameter;
Update 19/04/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): allow shared memory segments of type "unsigned
                  short" for input image and background, copy them to an
                  allocated data buffer of type "float";
                  clean_buffer(): when detaching from a shared memory segment,
                  free the corresponding allocated buffer if it exists.
Update 14/03/2001 R. Wilcke (wilcke@esrf.fr)
                  get value for Dummy from data header, if possible.
Update 13/02/2001 R. Wilcke (wilcke@esrf.fr)
                  clean up global variables: remove C_XSIZE, C_YSIZE;
                  added global variable Dummy.
Update 02/02/2001 R. Wilcke (wilcke@esrf.fr)
                  get_buffer(): change code to return a buffer of type "float"
                  except for a "header" buffer;
                  put_buffer(): change type of output buffer to "SHM_FLOAT".

*/
#include "spd.h"
#include <sys/time.h>

static int raw_cmpr = UnCompressed;
int azim_pass = 1,headpass = 0;
int verbose = 1, inptbusy = 0;

/*
 * All elements of current_shm and current_shm_data
 * are initialized to NULL (C language default).
 */
static SHM_HEADER* current_shm[MAXTYP];
static       void* current_shm_data[MAXTYP];

void help_arg(void);
int scanhead(int,struct data_head *);
int get_filnam(char **,char *,char *,int);
int fprint_first_el(FILE *,const char *,int *,int *);

int fprint_first_el(FILE *out,const char *header_key,int *pErrorValue,
  int *pstatus)
{
  const char *key,*value;

  edf_first_header_element(header_key,&key,&value,pErrorValue,pstatus);
  if(*pstatus || key == NULL) {
    printf(" \'%s\' : no elements\n",header_key);
    return(-1);
  } else
    printf(" \'%s\' : \'%s\' = \'%s\'\n",header_key,key,value);
  return(0);
}

/*==============================================================================
 * This routine handles the printing of user-defined messages.
 *
 * It works by being called from the macros "prmsg" or "__prmsg" (see
 * corresponding description). These macros call this routine twice:
 * - first with four arguments, of which the first is == NULL. The remaining
 *   three contain the message type, the line number and the file name;
 * - then with a variable number of arguments. The first (which must not be
 *   NULL) contains the format string, the remaining ones the parameters to
 *   print.
 *
 * Messages have one of the types DMSG, MSG, WARNING, ERROR or FATAL.
 *
 * For each of the types, the additional flag PRERR can be set. If it is, then
 * the output stream for the message is "stderr", else "stdout".
 *
 * Depending on the message type and the value of the user-defined "verbose"
 * global variable, the user message may or may not be printed:
 * - if verbose == -1, return without any action;
 * - if verbose ==  0, print only the message with types MSG, ERROR or FATAL;
 * - if verbose ==  1, print all messages except those of type DMSG;
 * - if verbose ==  2, print all messages.
 *
 * If the message type is ERROR or FATAL, also print the current line number and
 * file name (as defined by the C preprocessor macros __LINE__ and __FILE__). If
 * the system error variable "errno" is set, print the corresponding system
 * error message as well.
 *
 * If the message type is FATAL, the program will then be terminated by exit(1).
 * Note that this will not happen if verbose == -1!
 *
 * This routine uses a variable length argument list. This is handled
 * differently by old C compilers than by ANSI standard C compilers (STDC).
 * For details, see the "varargs" and "stdargs" manual pages.
 *
 * Input : va_alist: variable argument list
 * Output: none
 * Return: none
 */

#if defined(__STDC__)
void _prmsg(char *format,...)
{
#else
void _prmsg(va_alist)
va_dcl
{
  char *format;
#endif /* __STDC__ */
  char *errmsg = NULL;
  va_list args;
  static int type;
  static int line;
  static char file[256];
  static char *typmsg[] = {"\0","WARNING: ","\nERROR: ","\nFATAL: ","\0"};
  FILE *outstr;

  /*
   * Get the format argument. For "stdarg" argument handling, this is
   * explicitly handed over in the argument list. For "varargs" argument
   * handling, it is the first argument of the variable length argument list.
   */
#if defined(__STDC__)
  va_start(args,format);
#else
  va_start(args);
  format = va_arg(args,char*);
#endif /* __STDC__ */

  /*
   * If the format argument is == NULL, only get the message type, the current
   * file name and the current line number (as defined by the C preprocessor
   * macros __LINE__ and __FILE__).
   *
   * If it is not NULL, it contains the format string for the printing.
   */
  if(format == NULL) {
    type = va_arg(args,int);
    strcpy(file,va_arg(args,char*));
    line = va_arg(args,int);
    va_end(args);
    return;
  }

  /*
   * Leave this routine without printing in the following cases:
   * - verbose == -1
   * - verbose == 0 and type is neither MSG nor FATAL nor ERROR
   * - verbose == 1 and type is DMSG
   */
  if((verbose == -1) ||
    (verbose != 1 && verbose != 2 && type == WARNING) ||
    (verbose != 2 && type == DMSG)) {
    va_end(args);
    return;
  }

  /*
   * Determine the output stream for the printing of the message.
   *
   * If the flag PRERR is set in "type", print to "stderr", else to "stdout".
   */
  if(type & PRERR) {
    outstr = stderr;
    type &= ~PRERR;
   } else
    outstr = stdout;

  /*
   * Print the message to the requested output.
   *
   * The format string obtained earlier and the rest of the variable argument
   * list is given to a vfprintf() call for printing.
   *
   * For message type WARNING, ERROR and FATAL, print the type of the message
   * before the message text.
   *
   * If the message type is ERROR or FATAL, also print after the message text
   * the current line number and file name (as defined by the C preprocessor
   * macros __LINE__ and * __FILE__). If the system error variable "errno" is
   * set, print the corresponding system error message as well.
   *
   * If the message type is FATAL, terminate the program with exit(1).
   */
  fprintf(outstr,"%s",*(typmsg + type));
  vfprintf(outstr,format,args);
  if(type == ERROR || type == FATAL) {
    if(errno != 0)
      errmsg = strerror(errno);
    fprintf(outstr,"%s (in file %s line %d)\n",errmsg ? errmsg : " ",file,line);
  }
  va_end(args);

  if(type == FATAL) {
    fprintf(outstr,"Program terminated\n");
    exit(1);
  }
}

const char * shm_data_type_string ( int shm_type )
{ const char * data_type_string="";
  switch ( shm_type ) {
    case SHM_DOUBLE: data_type_string="double"; break;
    case SHM_FLOAT:  data_type_string="float"; break;
    case SHM_LONG:   data_type_string="long"; break;
    case SHM_ULONG:  data_type_string="unsigned long"; break;
    case SHM_SHORT:  data_type_string="short"; break;
    case SHM_USHORT: data_type_string="unsigned short"; break;
    case SHM_CHAR:   data_type_string="char"; break;
    case SHM_UCHAR:  data_type_string="unsigned char"; break;
    case SHM_STRING: data_type_string="string"; break;
    default: data_type_string="unknown"; break;
  }
  return( data_type_string );
}

#ifndef NO_SHARED_MEMORY
/*==============================================================================
 * Gets the header from the shared memory segment identified by the input
 * parameter "id".
 *
 * If the input flag "delete" is set, the shared memory segment is removed
 * afterwards.
 *
 * Note that this shared memory header is "spec" specific. It is not part of
 * the standard shared memory operations.
 *
 * Input : id:     shared memory identifier
 *         delete: if true, remove the shared memory segment
 * Output: none
 * Return: pointer to the "spec" shared memory header
 */
SHM_HEADER *getShmPtr(int id, int delete)
{
  SHM_HEADER *shm;
  struct shmid_ds info;

  /*
   * shmat() attaches the shared memory segment associated with the identifier
   * "id" to the data segment of the calling process.
   *
   * Last parameter of shmat must be SHM_RDONLY if read only access is desired,
   * otherwise it is read / write (with 0 it is read / write).
   */

  /* shmat is casted to a void as it is a char * on some machines. Casting it
     to shm_header would not feel quite right as it only starts with a
     shm_header
  */
  if((shm = (void *)shmat(id,(char *)0,0)) == (void *) -1) {
    prmsg(ERROR,("Could not attach shared mem (id %d)\n",id));
    return((SHM_HEADER *)NULL);
  }

  if(shm->head.head.magic != SHM_MAGIC) {
    prmsg(ERROR,("Shared mem (id %d) is not a spec shared mem\n",id));
    shmdt((void *)shm);
    return((SHM_HEADER *)NULL);
  }

  if(!delete)
    return(shm);

  /*
   * Calling shmctl() with the command IPC_STAT places the current value of
   * each member of the data structure associated with "id" into the structure
   * pointed to by "info".
   *
   * The element "info.shm_nattch" contains the number of current attaches to
   * the shared memory segment.
   */
  shmctl(id,IPC_STAT,&info);
  if(info.shm_nattch == 1) {

    /*
     * shmdt() detaches the shared memory segment located at the address
     * specified by "shm" from the calling process' data segment.
     */
    shmdt((void *)shm);

    /*
     * Calling shmctl() with the command IPC_STAT removes the memory identifier
     * specified by "id" from the system and destroys the shared memory and data
     * structure associated with it.
     */
    if(!shmctl(id,IPC_RMID,&info)) {
      prmsg(DMSG,("Shared mem (id %d) deleted - nobody attached \n",id));
    }
    return((SHM_HEADER *)NULL);
  }

  return(shm);
} /* getShmPtr */

/*==============================================================================
 * This routine returns the pointer to the data of the shared memory
 * starting at shm_header depending on the shared memory version. 
 *
 * If shm_version is larger than 3 the new header structure
 * shm_header is used, otherwise the old structure shm_oheader
 * Only if shm_version is 0 the actual version is read from the header,
 * otherwise no memory access is done.
 *
 * data pointer corresponding to version is returned.
 * Depending on shm_version the pointer to the start of the shared memory 
 * data section is * returned:
 *   version 0   read version from header 
 *   version <=3 shm_oheader
 *   version >3  shm_header
 *
 * Data is defined as void * data in spec_shm.h. Apparently 'void * data'
 * is the start of the data section and not a pointer to the data. A better 
 * specification would be 'void data'. But C does not allow it.
 *
 * Attention: For shm_header!=NULL and shm_version==0 the shared memory 
 *            must be attached!
 */

void *getShmDataPtr( SHM_HEADER* shm, int shm_version )
{ void * shm_data_ptr = NULL;
  int    shm_type = -1; // undefined

  if (shm) {
    if (shm_version==0) {
      shm_version = shm->head.head.version;
      shm_type = shm->head.head.type;
    }

    if (shm_version>3) {
      shm_data_ptr = (void *)  &(((struct shm_header*) shm)->data);
    } else {
      shm_data_ptr = (void *)  &(((struct shm_oheader*) shm)->data);
    }

    prmsg(DMSG,("Version of shared %s memory at %p is %d - data starts at %p\n",
      shm_data_type_string(shm_type),
      (void *)shm, shm_version, shm_data_ptr));
  }

  return( shm_data_ptr );

} // getShmDataPtr

u32_t getShmUTime( SHM_HEADER* shm )
{ return( shm->head.head.utime );
} /* getShmUTime */

u32_t incShmUTime( SHM_HEADER* shm )
{ return( shm->head.head.utime++ );
} /* incShmUTime */

u32_t getShmRows( SHM_HEADER* shm )
{ return( shm->head.head.rows );
} /* getShmRows */

u32_t getShmCols( SHM_HEADER* shm )
{ return( shm->head.head.cols );
} /* getShmCols */

u32_t getShmType( SHM_HEADER* shm )
{ return( shm->head.head.type );
} /* getShmType */
#else /* ifndef NO_SHARED_MEMORY */
SHM_HEADER *getShmPtr(int id, int delete)
{ return ( (SHM_HEADER *) NULL );
} /* getShmPtr */

void *getShmDataPtr( SHM_HEADER* shm, int shm_version )
{ return ( NULL );
} /* getShmDataPtr */

u32_t getShmUTime( SHM_HEADER* shm )
{ return ( (u32_t) 0 );
} /* getShmUTime */

u32_t incShmUTime( SHM_HEADER* shm )
{ return( (u32_t) 1 );
} /* incShmUTime */

u32_t getShmRows( SHM_HEADER* shm )
{ return ( (u32_t) 0 );
} /* getShmRows */

u32_t getShmCols( SHM_HEADER* shm )
{ return ( (u32_t) 0 );
} /* getShmCols */

u32_t getShmType( SHM_HEADER* shm )
{ return ( (u32_t) -1 );
} /* getShmType */

int shmdt(const void *shmaddr)
{ return(-1);
} /* shmdt */
#endif /* ifndef NO_SHARED_MEMORY */

/*==============================================================================
 * If SPEC_PIPE is defined, then this package can be called from a different
 * program using the call user_code().
 *
 * If SPEC_PIPE is not defined, then this package will be a stand-alone program
 * with its own main().
 */
#if SPEC_PIPE
int __main()
{
  /* This is a trick to get cc working if data_pipe has been
     compiled with gcc. __main seems to be called before main .
  */
  return(0);
}

int user_code(int argc,char *argv[])
{
  unsigned long status;

  errno = 0;
  status = (unsigned long)analyse_args(argc,argv,"corimg_corr");
  prmsg(DMSG,("Image processing returned status %#x\n",status));
  set_return_value((float)status);
  return(status);
}

#else
/*==============================================================================
 * Function "main" for the SPD offline version.
 *
 * The routine hands the command line arguments (without the program name) to
 * the routine analyse_args() and returns an exit code of success or failure to
 * the user depending on the return value of analyse_args().
 *
 * Input : argc: number of the command line arguments (including program name)
 *         argv: string array with the program name and the command line
 *               arguments
 * exit code:  EXIT_SUCCESS  (== 0)  if analyse_args() returns no error
 *             EXIT_FAILURE  (!= 0)  else
 */
#if MAKE_FUNCTION
# define MAIN main_spd
#else
# define MAIN main
#endif

/*==============================================================================
 * get the time to do some simple benchmarking
 * =============================================================================
 */
double get_time() {
    struct timeval tv;
    gettimeofday(&tv,(void *)0);
    return (double) tv.tv_sec+tv.tv_usec*1e-6;
}

int MAIN(int argc,char *argv[])
{
  if(argc >=2 ){
    if(!strcasecmp( argv[1],"--server")){
      int my_argc=0;                        // something that looks like argc
      char *my_argv[256];                   // something that looks like argv
      char *svprt;                          // pointer
      char *s;                              // pointer
      char buf[2048];                       // buffer for input command
      int x=0;                              // index
      unsigned long status;                 // return status of SPD
      double start;                         // start & stop time
      static const char delim [] =" \t\n";  // delimiters of strings
      my_argv[0]=" ";
// when we are in server mode, use unbuffered output stdout & stderr
      setvbuf(stdout, NULL, _IONBF, 0);
      setvbuf(stderr, NULL, _IONBF, 0);
      prmsg(MSG,("Server Mode\n"));
      while(strcasecmp(my_argv[0],"--exit")!=0) {
	fgets(buf,2048, stdin);
	//prmsg(MSG,"I got: %s\n",buf);
	s = strlib_tok_r(buf,delim,&svprt);
	my_argc = 0;
	while(s!=0) {
	  my_argv[my_argc++] = s;
	  s = strlib_tok_r(NULL,delim,&svprt);
        }
	my_argv[my_argc] = 0;
	for(x=0;x<=my_argc;++x) {
	  if(my_argv[x] != 0)
	    printf("argv[%d] = %s\n",x,my_argv[x]);
	  //else
	  //  printf ("argv[%d] = NULL\n",x);
        }
	start = get_time();
	status = (unsigned long)analyse_args(my_argc,my_argv,"SPD Server Mode");
	prmsg(MSG,("Image processing took %.3f s returned status %#x\n",
	  (get_time()-start),status));
      }
      exit(EXIT_SUCCESS);
    }
  }

  if(analyse_args(argc - 1,argv + 1,argv[0]) < 0)
    exit(EXIT_FAILURE);
  exit(EXIT_SUCCESS);
}
#endif /* SPEC_PIPE */

/*==============================================================================
 * Returns the value of the "input busy flag":
 *   1  if input data are just being read by the program
 *   0  if not
 *
 * Input : none
 * Output: none
 * Return: value of the "input busy flag"
 */

int getstate(void)
{
  return(inptbusy);
}

/*==============================================================================
 * Update the values of user_head with command header values
 *
 * Input : struct data_head *user_head header to be updated
 * Return: 0   no errors
 */

int upd_headvalcmd( struct data_head *user_head ) 
{
  struct data_head cmd_head;

  get_headval(&cmd_head,CMDTYP);

  /*
   * Set the header values defined on the command line in user_head
   */
  if(cmd_head.init != 0) {

    if(cmd_head.init & FL_OFFS1) {
      user_head->Offset_1 = cmd_head.Offset_1;
      user_head->init |= FL_OFFS1;
    }
    if(cmd_head.init & FL_OFFS2) {
      user_head->Offset_2 = cmd_head.Offset_2;
      user_head->init |= FL_OFFS2;
    }
    if(cmd_head.init & FL_BSIZ1) {
      user_head->BSize_1 = cmd_head.BSize_1;
      user_head->init |= FL_BSIZ1;
    }
    if(cmd_head.init & FL_BSIZ2) {
      user_head->BSize_2 = cmd_head.BSize_2;
      user_head->init |= FL_BSIZ2;
    }

    if(cmd_head.init & FL_CENT1) {
      user_head->Center_1 = cmd_head.Center_1;
      user_head->init |= FL_CENT1;
    }
    if(cmd_head.init & FL_CENT2) {
      user_head->Center_2 = cmd_head.Center_2;
      user_head->init |= FL_CENT2;
    }
    if(cmd_head.init & FL_INTE0) {
      strcpy(user_head->Intens_0,cmd_head.Intens_0);
      user_head->init |= FL_INTE0;
    }
    if(cmd_head.init & FL_INTE1) {
      strcpy(user_head->Intens_1,cmd_head.Intens_1);
      user_head->init |= FL_INTE1;
    }
    if(cmd_head.init & FL_PSIZ1) {
      user_head->PSize_1 = cmd_head.PSize_1;
      user_head->init |= FL_PSIZ1;
    }
    if(cmd_head.init & FL_PSIZ2) {
      user_head->PSize_2 = cmd_head.PSize_2;
      user_head->init |= FL_PSIZ2;
    }
    if(cmd_head.init & FL_SAMDS) {
      user_head->SamplDis = cmd_head.SamplDis;
      user_head->init |= FL_SAMDS;
    }
    if(cmd_head.init & FL_ORIEN) {
      user_head->Orientat = cmd_head.Orientat;
      user_head->init |= FL_ORIEN;
    }
    if(cmd_head.init & FL_TITLE) {
      strcpy(user_head->Title,cmd_head.Title);
      user_head->init |= FL_TITLE;
    }
    if(cmd_head.init & FL_WAVLN) {
      user_head->WaveLeng = cmd_head.WaveLeng;
      user_head->init |= FL_WAVLN;
    }
    if(cmd_head.init & FL_PRO) {
      strcpy(user_head->ProjTyp,cmd_head.ProjTyp);
      user_head->init |= FL_PRO;
    }
    if(cmd_head.init & FL_ROT1) {
      user_head->DetRot_1 = cmd_head.DetRot_1;
      user_head->init |= FL_ROT1;
    }
    if(cmd_head.init & FL_ROT2) {
      user_head->DetRot_2 = cmd_head.DetRot_2;
      user_head->init |= FL_ROT2;
    }
    if(cmd_head.init & FL_ROT3) {
      user_head->DetRot_3 = cmd_head.DetRot_3;
      user_head->init |= FL_ROT3;
    }

    /* 
     * Set pre-rotation parameters
     */

    if(cmd_head.init & FL_PRECEN1) {
      user_head->PreCenter_1 = cmd_head.PreCenter_1;
      user_head->init |= FL_PRECEN1;
    }
    if(cmd_head.init & FL_PRECEN2) {
      user_head->PreCenter_2 = cmd_head.PreCenter_2;
      user_head->init |= FL_PRECEN2;
    }
    if(cmd_head.init & FL_PREDIS) {
      user_head->PreSamplDis = cmd_head.PreSamplDis;
      user_head->init |= FL_PREDIS;
    }
    if(cmd_head.init & FL_PREROT1) {
      user_head->PreDetRot_1 = cmd_head.PreDetRot_1;
      user_head->init |= FL_PREROT1;
    }
    if(cmd_head.init & FL_PREROT2) {
      user_head->PreDetRot_2 = cmd_head.PreDetRot_2;
      user_head->init |= FL_PREROT2;
    }
    if(cmd_head.init & FL_PREROT3) {
      user_head->PreDetRot_3 = cmd_head.PreDetRot_3;
      user_head->init |= FL_PREROT3;
    }
  }
  return(0);
}

/*==============================================================================
 * Obtains the header keyword values from the EDF header (input file or
 * temporary online header).
 *
 * Initialize a select set of the header value if they are not set by the input
 * data.
 *
 * For "x-distortion" and "y-distortion" data, get the "displaced" header values
 * (if any).
 *
 * The prerotation parameters are only read for source and dark files.
 * In all headers the default prerotations are 0. 
 *
 * On return, the "init" member of the structure contains the "OR"ed flags of
 * all the keywords found (or initialized).
 *
 * Input : type: type of data buffer for which the header is to be obtained (see
 *               routine get_buffer() for more information)
 * Output: user_head: structure with the values of the header keywords
 * Return: -1  if error
 *          0  else
 */

int scanhead(int type,struct data_head *user_head)
{
  static char dspkey[100] = "Displaced";
  const char *val;
  int i,ish,status,err;
  struct data_head cmd_head;
  int retval=0;

  user_head->init = 0;
  user_head->Dspinit = 0;

  unsigned long paramsneeded, paramsgiven;

  /*
   * Search the header for values corresponding to the following keywords:
   * - RasterOrientation
   * - Dummy
   * - DDummy
   * - Offset_1
   * - Offset_2
   * - PSize_1
   * - PSize_2
   * - Intensity0
   * - Intensity1
   * - Center_1
   * - Center_2
   * - SampleDistance
   * - WaveLength
   * - Title
   * - Time
   * - ExposureTime
   * - BSize_1
   * - BSize_2
   * - Dim_1
   * - Dim_2
   * - ProjectionType
   * - DetectorRotation_1
   * - DetectorRotation_2
   * - DetectorRotation_3
   *
   * The prerotation parameters are only updated in the source image header
   * and the spec header
   * 
   * - PreDetectorRotation_1
   * - PreDetectorRotation_2
   * - PreDetectorRotation_3
   * - PreCenter_1 
   * - PreCenter_2
   * - PreSampleDistance
   *
   * If the keywords are found and the values are valid, then they set the
   * corresponding values in the global variables. If not, then the previous
   * values of the global variables are kept. This is not an error condition.
   */
  for(i = 0, ish = 1; i < maxhdkey; i++, ish = 1 << i) {
    if(edf_search_header_element(*(typestr + type),*(headkey + i),&val,&err,
      &status) && status == status_success) {
      switch(ish) {
      case FL_ORIEN:
	user_head->Orientat = num_str2long(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_DUMMY:
	user_head->Dummy = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_DDUMM:
	user_head->DDummy = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_OFFS1:
	user_head->Offset_1 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_OFFS2:
	user_head->Offset_2 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_PSIZ1:
	user_head->PSize_1 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_PSIZ2:
	user_head->PSize_2 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_INTE0:
	strcpy(user_head->Intens_0,val);
        user_head->init |= ish;
	break;
      case FL_INTE1:
	strcpy(user_head->Intens_1,val);
        user_head->init |= ish;
	break;
      case FL_CENT1:
	user_head->Center_1 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_CENT2:
	user_head->Center_2 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_SAMDS:
	user_head->SamplDis = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_WAVLN:
	user_head->WaveLeng = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_TITLE:
	strcpy(user_head->Title,val);
        user_head->init |= ish;
	break;
      case FL_TIME:
	strcpy(user_head->Time,val);
        user_head->init |= ish;
	break;
      case FL_EXTIM:
	strcpy(user_head->ExpTime,val);
        user_head->init |= ish;
	break;
      case FL_BSIZ1:
	user_head->BSize_1 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_BSIZ2:
	user_head->BSize_2 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_DIM1:
	user_head->Dim_1 = num_str2long(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_DIM2:
	user_head->Dim_2 = num_str2long(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_PRO:
	strcpy(user_head->ProjTyp,val);
        user_head->init |= ish;
	break;
      case FL_ROT1:
	user_head->DetRot_1 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_ROT2:
	user_head->DetRot_2 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
	break;
      case FL_ROT3:
	user_head->DetRot_3 = num_str2double(val,NULL,&err);
        user_head->init |= ish;
        /*
         * Load prerotation parameters only for SCRTYP
         */
      case FL_PRECEN1:
        if ( (type == SRCTYP) || (type == HD_TYP) ) {
          user_head->PreCenter_1 = num_str2double(val,NULL,&err);
          user_head->init |= ish;
        }
        break;
      case FL_PRECEN2:
        if ( (type == SRCTYP) || (type == HD_TYP) ) {
          user_head->PreCenter_2 = num_str2double(val,NULL,&err);
          user_head->init |= ish;
        }
        break;
      case FL_PREDIS:
        if ( (type == SRCTYP) || (type == HD_TYP) ) {
          user_head->PreSamplDis = num_str2double(val,NULL,&err);
          user_head->init |= ish;
        }
        break;
      case FL_PREROT1:
        if ( (type == SRCTYP) || (type == HD_TYP) ) {
          user_head->PreDetRot_1 = num_str2double(val,NULL,&err);
          user_head->init |= ish;
        }
        break;
      case FL_PREROT2:
        if ( (type == SRCTYP) || (type == HD_TYP) ) {
          user_head->PreDetRot_2 = num_str2double(val,NULL,&err);
          user_head->init |= ish;
        }
        break;
      case FL_PREROT3:
        if ( (type == SRCTYP) || (type == HD_TYP) ) {
          user_head->PreDetRot_3 = num_str2double(val,NULL,&err);
          user_head->init |= ish;
        }
        break;
      }
    }
  }

  prmsg(DMSG,("Header flags set in %s data: 0x%x\n",*(typestr + type),
    user_head->init));

  /*
   * Set the header values defined on the command line for the source or dark
   * image, if any.
   */
  if(type == SRCTYP || type == DRKTYP)
    upd_headvalcmd( user_head );

  /*
   * For the following header elements, set default values if they are not set
   * by the input data or command line arguments:
   * - RasterOrientation =  1
   * - Dummy =              0.
   * - DDummy =             DDSET(Dummy)
   * - Offset_1 =           0.
   * - Offset_2 =           0.
   * - Intensity1 =         Intensity0  (only if Intensity0 is defined)
   * - Title =              "\0"  (i.e. an empty string)
   * - BSize_1 =            1.
   * - BSize_2 =            1.
   * - ProjectionType =     "Saxs"
   * - DetectorRotation_1 = 0.
   * - DetectorRotation_2 = 0.
   * - DetectorRotation_3 = 0.
   */
  if(!(user_head->init & FL_DUMMY))
  {
    user_head->init |= FL_DUMMY;
    user_head->Dummy = 0.;
  }
  if(!(user_head->init & FL_DDUMM))
  {
    user_head->init |= FL_DDUMM;
    user_head->DDummy = DDSET(user_head->Dummy);
  }
  if(!(user_head->init & FL_OFFS1))
  {
    user_head->init |= FL_OFFS1;
    user_head->Offset_1 = 0.;
  }
  if(!(user_head->init & FL_OFFS2))
  {
    user_head->init |= FL_OFFS2;
    user_head->Offset_2 = 0.;
  }
  if(!(user_head->init & FL_ORIEN))
  {
    user_head->init |= FL_ORIEN;
    user_head->Orientat = 1;
  }
  if(!(user_head->init & FL_INTE1) && user_head->init & FL_INTE0)
  {
    user_head->init |= FL_INTE1;
    strcpy(user_head->Intens_1,user_head->Intens_0);
  }
  if(!(user_head->init & FL_TITLE))
    *(user_head->Title) = '\0';
  if(!(user_head->init & FL_BSIZ1))
  {
    user_head->init |= FL_BSIZ1;
    user_head->BSize_1 = 1.;
  }
  if(!(user_head->init & FL_BSIZ2))
  {
    user_head->init |= FL_BSIZ2;
    user_head->BSize_2 = 1.;
  }
  if(!(user_head->init & FL_PRO))
  {
    user_head->init |= FL_PRO;
    strcpy(user_head->ProjTyp,"Saxs");
  }
  if(!(user_head->init & FL_ROT1))
  {
    user_head->init |= FL_ROT1;
    user_head->DetRot_1 = 0.;
  }
  if(!(user_head->init & FL_ROT2))
  {
    user_head->init |= FL_ROT2;
    user_head->DetRot_2 = 0.;
  }
  if(!(user_head->init & FL_ROT3))
  {
    user_head->init |= FL_ROT3;
    user_head->DetRot_3 = 0.;
  }

  /*
   * For all header types the default value for prerotations is zero.
   * - PreDetectorRotation_1 = 0.      // FL_PREROT1
   * - PreDetectorRotation_2 = 0.      // FL_PREROT2
   * - PreDetectorRotation_3 = 0.      // FL_PREROT3
   *
   * If PreCenter and PreSampleDistance are not set by the input 
   * data or command line arguments default values are calculated:
   * - PreCenter_1                     // FL_PRECEN1
   * - PreCenter_2 and                 // FL_PRECEN2
   * - PreSampleDistance               // FL_PREDIS
   *
   * are calculated so that they agree with Center_1, Center_2 
   * and SampleDistance after prerotation
   */

  if(!(user_head->init & FL_PREROT1))
  {
    user_head->init |= FL_PREROT1;
    user_head->PreDetRot_1 = 0.;
  }
  if(!(user_head->init & FL_PREROT2))
  {
    user_head->init |= FL_PREROT2;
    user_head->PreDetRot_2 = 0.;
  }
  if(!(user_head->init & FL_PREROT3))
  {
    user_head->init |= FL_PREROT3;
    user_head->PreDetRot_3 = 0.;
  }
 
  if(type == SDXTYP || type == SDYTYP) {

    /*
     * For "x-distortion" and "y-distortion" data, get the "displaced" header
     * values (if any).
     */
    for(i = 0; i < maxhdkey; i++) {
      strncpy(dspkey + 9,*(headkey + i),90);
      if(edf_search_header_element(*(typestr + type),dspkey,&val,&err,
        &status) && status == status_success) {
        user_head->Dspinit |= 1 << i;
        switch(i) {
        case 0: // FL_ORIEN
	  user_head->DspOrientat = num_str2long(val,NULL,&err);
	  break;
        case 1: // FL_DUMMY
	  user_head->DspDummy = num_str2double(val,NULL,&err);
	  break;
        case 2: // FL_DDUMM
	  user_head->DspDDummy = num_str2double(val,NULL,&err);
	  break;
        case 3: // FL_OFFS1
	  user_head->DspOffset_1 = num_str2double(val,NULL,&err);
	  break;
        case 4: // FL_OFFS2
	  user_head->DspOffset_2 = num_str2double(val,NULL,&err);
	  break;
        case 5: // FL_PSIZ1
	  user_head->DspPSize_1 = num_str2double(val,NULL,&err);
	  break;
        case 6: // FL_PSIZ2
	  user_head->DspPSize_2 = num_str2double(val,NULL,&err);
	  break;
        case 7: // FL_INTE0
	  strcpy(user_head->DspIntens_0,val);
	  break;
        case 8: // FL_INTE1
	  strcpy(user_head->DspIntens_1,val);
	  break;
        case 9: // FL_CENT1
	  user_head->DspCenter_1 = num_str2double(val,NULL,&err);
	  break;
        case 10: // FL_CENT2
	  user_head->DspCenter_2 = num_str2double(val,NULL,&err);
	  break;
        case 11: // FL_SAMDS
	  user_head->DspSamplDis = num_str2double(val,NULL,&err);
	  break;
        case 12: // FL_WAVLN
	  user_head->DspWaveLeng = num_str2double(val,NULL,&err);
	  break;
        case 13: // FL_TITLE
	  strcpy(user_head->DspTitle,val);
	  break;
        case 14: // FL_TIME
	  strcpy(user_head->DspTime,val);
	  break;
        case 15: // FL_EXTIM
	  strcpy(user_head->DspExpTime,val);
	  break;
        case 16: // FL_BSIZ1
	  user_head->DspBSize_1 = num_str2double(val,NULL,&err);
	  break;
        case 17: // FL_BSIZ2
	  user_head->DspBSize_2 = num_str2double(val,NULL,&err);
	  break;
        case 18: // FL_DIM1
	  user_head->DspDim_1 = num_str2long(val,NULL,&err);
	  break;
        case 19: // FL_DIM2
	  user_head->DspDim_2 = num_str2long(val,NULL,&err);
	  break;
        case 20: // FL_PRO
	  strcpy(user_head->DspProjTyp,val);
	  break;
        case 21: // FL_ROT1
	  user_head->DspDetRot_1 = num_str2double(val,NULL,&err);
	  break;
        case 22: // FL_ROT2
	  user_head->DspDetRot_2 = num_str2double(val,NULL,&err);
	  break;
        case 23: // FL_ROT3
	  user_head->DspDetRot_3 = num_str2double(val,NULL,&err);
          break;
        case 24: // FL_PRECEN1
          user_head->DspPreCenter_1 = num_str2double(val,NULL,&err);
          break;
        case 25: // FL_PRECEN2
          user_head->DspPreCenter_2 = num_str2double(val,NULL,&err);
          break;
        case 26: // FL_PREDIS
          user_head->DspPreSamplDis = num_str2double(val,NULL,&err);
          break;
        case 27: // FL_PREROT1
          user_head->DspPreDetRot_1 = num_str2double(val,NULL,&err);
          break;
        case 28: // FL_PREROT2
          user_head->DspPreDetRot_2 = num_str2double(val,NULL,&err);
          break;
        case 29: // FL_PREROT3
          user_head->DspPreDetRot_3 = num_str2double(val,NULL,&err);
          break;
        }
      }
    }

  } /*if(type == SDXTYP || type == SDYTYP)*/

  return(retval);

} /* scanhead */

/*==============================================================================
 * Read a filename from a keyword in the source header.
 *
 * If the name of a file in the input arguments of "spd" has been specified in
 * the form
 *
 *   filename=prefix[char_string]postfix
 *
 * (a character string value with a leading and trailing square bracket,
 * optionally preceded and/or followed by other character strings, e.g.
 *       dark_file=[DarkFileName]
 *   or  dark_file=../datadark/[DarkFileName]
 *   or  dark_file=[DarkFileName]_len32
 *   or  dark_file=../mydat/[DarkFileName].ext
 * )
 *
 * then the "char_string" part of this filename is to be read from the header of
 * the source image in the keyword "char_string".
 *
 * Test if that is the case. If so get the keyvalue and assemble the file name
 * from the "prefix" string, followed by the keyvalue and then the "postfix"
 * string.
 *
 * Example:
 *   with dark_file=../mydat/[DarkFileName].ext
 *   and the definition in the input file header DarkFileName=gd10dark
 *   the resulting filename is ../mydat/gd10dark.ext
 *
 * Return with error if the file name contains the "[char_string]" construct but
 * the corresponding keyword cannot be found in the source header.
 *
 * If the file name does not contain the "[char_string]" construct (including
 * the cases of only a opening '[", only a closing "]" or "]" preceding '["),
 * or if the input is from shared memory (file is a NULL pointer), then the
 * routine returns without action. This is not an error condition.
 *
 * Input : file  : pointer to input file name string (NULL for shared memory)
 *         inbuf : character string given as file name argument to spd
 *         outbuf: buffer (possibly empty) for the new file name
 *                 Note: this buffer must be allocated by the calling program
 *         type  : type of file to be read (source, flood field, ...)
 * Output: file  : pointer to filename constructed from keyword, or unchanged
 *                 if no keyword in input file name string
 *         outbuf: character string with the file name from the input data file
 * Return: -1  if error
 *          0  else
 */
int get_filnam(char **file,char *inbuf,char *outbuf,int type)
{
  const char *keyval;
  char *lpos,*rpos;
  char key[EdfMaxKeyLen + 1] = {'\0'};;
  int status,err;
  int iret = 0;

  lpos = strchr((const char *)inbuf,(int)'[');
  rpos = strrchr((const char *)inbuf,(int)']');
  if(*file != NULL && lpos != NULL && rpos != NULL && lpos < rpos) {

    /*
     * Remove prefix, postfix and square brackets, use only "char_string" for
     * the keyword search.
     * If successful, concatenate the prefix, the obtained key value and the
     * postfix into the output buffer, this will be used as the new file name
     * buffer. Note that the original buffer with the value of the file name
     * input argument is not changed.
     */
    strncat(key,lpos + 1,rpos - lpos - 1);
    if(edf_search_header_element(*(typestr + SRCTYP),key,&keyval,&err,&status)
      && status == status_success) {
      *outbuf = '\0';
      strncat(outbuf,(const char *)inbuf,lpos - inbuf);
      strcat(outbuf,keyval);
      strcat(outbuf,(const char *)(rpos + 1));
      *file = outbuf;
    } else {
      prmsg(ERROR,("%s file keyword \"%s\" not found in source header\n",
	*(typestr + type),key));
      iret = -1;
    }
  }
  return(iret);
} /* get_filnam */

/*==============================================================================
 * Returns only 0 if key is one of the following prerotation parameters:
 * - PreDetectorRotation_1
 * - PreDetectorRotation_2
 * - PreDetectorRotation_3
 * - PreCenter_1
 * - PreCenter_2
 * - PreSampleDistance
 */

int is_not_prerotpar ( const char * key )
{ static const char *prerotpar[] = {"PreDetectorRotation_1",
    "PreDetectorRotation_2", "PreDetectorRotation_3", "PreCenter_1",
    "PreCenter_2", "PreSampleDistance", (const char *) NULL };

  int notfound=1;
  const char **ppar=prerotpar;

  for ( ppar = prerotpar; *ppar; ppar++ ) {
    if (!( notfound = strcmp( *ppar, key ) )) break;
  }

  return( notfound );

} /* is_not_prerotpar */

/*==============================================================================
 * Get a buffer for the type of data requested and, where applicable, fill it
 * with the appropriate data values.
 *
 * The following data types are possible:
 * - SRCTYP: source data (input image)
 * - CORTYP: corrected data (output image)
 * - DRKTYP: dark image data (the image for the dark image subtraction)
 * - FLOTYP: flood field data (the image for the flood field correction)
 * - HD_TYP: header data set from SPEC
 * - SBKTYP: scattering background data (the image for the scattering background
 *           correction)
 * - SDXTYP: x-direction displacement values for spatial distortion correction
 * - SDYTYP: y-direction displacement values for spatial distortion correction
 * - SDMTYP: multiplication factors for spatial distortion correction
 * - AZITYP: azimuthal regrouped data (output image)
 * - AVETYP: azimuthal averaged data (output image)
 * - MSKTYP: mask image with pixels to ignore for azimuthal regrouping (input)
 * - CMDTYP: header structure with values filled by command line arguments
 *
 * The storage associated with the data in the buffer can be a file or a shared
 * memory segment. Identifiers for both are input to this routine.
 *
 * Not all combination of data type and data storage are possible, however.
 *
 * The data type CMDTYP is only filled from the command line and thus cannot be
 * used in a get_buffer() call.
 *
 * A shared memory segment can be a storage for all data types except SDXTYP,
 * SDYTYP and MSKTYP (i.e., spatial distortion displacement or azimuthal mask
 * data). The routine then normally returns a pointer to the location of the
 * shared memory as buffer pointer. If the data type is CORTYP, then this
 * shared memory serves as the place to put the corrected image data into.
 *
 * If the data type is one of the input types SRCTYP, DRKTYP, SBKTYP or FLOTYP,
 * the shared memory is always copied to a newly allocated internal buffer. This
 * makes the shared memory quickly available for refilling with new data by a
 * data acquisition process. The routine returns a pointer to this new buffer.
 *
 * Note that the correction routines need all input data to be of type "float".
 * If the shared memory contains its data in a different form, this routine can
 * convert the data under certain circumstances.
 *
 * For details, see below in the section "Input from shared memory segment".
 *
 * A file can only be a storage for the data types SRCTYP, DRKTYP, FLOTYP,
 * SBKTYP, SDXTYP, SDYTYP, SDMTYP and MSKTYP (i.e., source, dark image, 
 * flood field, scattering background, spatial distortion or azimuthal mask
 * data). If this is the case, then a buffer will be allocated, and the data
 * values will be read from the file and stored in the buffer.
 *
 * If neither the "shared memory segment identifier" nor the "file name" are
 * defined in the input arguments, then a NULL buffer pointer will be returned,
 * as no data can be obtained. Exceptions to this are the output data types
 * CORTYP, AZITYP, AVETYP, SDXTYP, SDYTYP and SDMTYP, as for these no input 
 * data are required. A buffer will be allocated and its pointer returned 
 * in these cases. The buffer values must be initialized separately.
 *
 * The input arguments "shared memory segment identifier" and "file name"
 * should not both be defined in a call to this routine. If they are, then the
 * shared memory segment identifier will be ignored, and processing will be as
 * if the file name only was defined.
 *
 * Note that if "type" is CORTYP, AZITYP, AVETYP or HD_TYP (corrected output
 * data, azimuthally regrouped output data, regrouped output data or online
 * header), "file" must be a NULL pointer, i.e. there cannot be a file
 * associated with these data types. If a file name is given, the routine
 * returns an error.
 *
 * For output data types, the dimensions of the buffer must be given as input
 * arguments "rows" and "cols". For input data types, "rows" and "cols" are
 * determined from the dimensions of the input image.
 *
 * For the azimuthally averaged data type, the "rows" argument given in the
 * input argument is checked against the corresponding dimension of the shared
 * memory buffer. If there are less than 4 rows in the buffer, the routine
 * returns an error. If 8 rows are requested and there is not enough room for 8
 * but enough for 4 (one averaged angle range only), the routine returns the
 * value 4 for "rows".
 *
 * The routine returns an error under any of the following conditions:
 * - the data type requested is not one of the possible ones;
 * - there is a file name specified for an output or a header buffer;
 * - memory allocation for the data buffer is required but fails;
 * - data needs to be read from a file, but there is an error in doing so;
 * - a shared memory segment is requested but cannot be attached or has the
 *   wrong data type in it;
 * - the "cols" dimension of the azimuthal averaging buffer is not compatible
 *   with the one given as input argument.
 *
 * It is not an error if neither the shared memory segment identifier nor the
 * file name are defined.
 *
 * Input : shm_id:     shared memory segment identifier, or -1 if none defined
 *         file:       name of data file, or NULL if none defined
 *         buffer_ptr: data buffer, or NULL if none available
 *         rows:       number of elements in the second dimension of the image
 *                     (this is the "slow-moving" index, i.e. the first index
 *                     in a two-dimensional C data buffer)
 *         cols:       number of elements in the first dimension of the image
 *                     (this is the "fast-moving" index, i.e. the second index
 *                     in a two-dimensional C data buffer)
 *         type:       type of data buffer to be obtained
 * Output: buffer_ptr: data buffer (may have been changed, allocated or freed)
 *         rows:       second dimension of the image (may have been changed)
 *         cols:       first dimension of the image (may have been changed)
 * Return:  1  if successful and the data buffer was updated (this includes
 *             the case where the data buffer was set to NULL)
 *          0  if successful and the old data buffer is still valid
 *         -1  else (error)
 */

int get_buffer(int shm_id,char *file,void **buffer_ptr,int *rows,int *cols,
  int type)
{
#define QUOTE(a) #a
#define FMT1(a) "%" QUOTE(a) "[^\n]"
#define FMT2(a) "%[^= ]%*[ =]%[" QUOTE(a) "^\n]"

  static char id[EdfMaxKeyLen + 1],val[EdfMaxValLen + 1];

  /*
   * By C language default, all elements of the following arrays are initialized
   * to:
   * - NULL   for old_file
   * - 0      for old_where, last_mtime, last_utime
   *
   * old_where tells the type of the previous data storage:
   *     no_stor  (= 0)  == none,
   *     fil_stor (= 1)  == file,
   *     shm_stor (= 2)  == shared memory.
   */
  static char *old_file[MAXTYP];
  static int old_where[MAXTYP];
  static int last_mtime[MAXTYP];
  static long last_utime[MAXTYP];

  enum {no_stor,fil_stor,shm_stor};
  void *pnewbuf;
  char *hdptr,*hdend;
  unsigned char *uch_buf;
  unsigned short *ush_buf;
  unsigned int *uin_buf;
  int *int_buf;
  int err,status,imgsiz,headrows,headcols;
  int file_changed = 0,shm_changed = 0;
  long hdtype;
  float *float_ptr,*flt_buf;
  double bu1,bu2,bs1,bs2;
  struct data_head src_head,user_head;
  SHM_HEADER *shm_ptr;
  struct stat stat_buf;


  /*
   * Check whether the type of data requested is a valid one.
   */
  if(type <= 0 || type >= MAXTYP || type == CMDTYP || type == TMPTYP) {
    errno = 0;
    prmsg(FATAL,("invalid data type %d for get_buffer().\n",type));
  }

  /*
   * Output or header data must not have a file associated with them.
   */
  if(file && (type == CORTYP || type == AZITYP || type == AVETYP ||
    type == HD_TYP)) {
    errno = 0;
    prmsg(ERROR,("no file permitted in get_buffer() for data type %s.\n",
      *(typestr + type)));
    return(-1);
  }

  /*
   * If the data image to be obtained is already in buffer but has a different
   * binning, dimension or offset than the source image, a possible cause is
   * that these parameters have changed in the source image. The data image then
   * needs to be re-processed accordingly.
   *
   * To enable this, force a re-acquisition of the data image.
   *
   * For the source image, a re-acquisition is always forced.
   */
  if(type != HD_TYP) {
    get_headval(&src_head,SRCTYP);
    if(type != SRCTYP) {
      unsigned short tstDim_1,tstDim_2;
      float tstOffset_1,tstOffset_2;
      if(type == SDXTYP || type == SDYTYP) {
	tstDim_1 = src_head.Dim_1 + 1;
	tstDim_2 = src_head.Dim_2 + 1;
	tstOffset_1 = src_head.Offset_1 - 0.5;
	tstOffset_2 = src_head.Offset_2 - 0.5;
      } else {
	tstDim_1 = src_head.Dim_1;
	tstDim_2 = src_head.Dim_2;
	tstOffset_1 = src_head.Offset_1;
	tstOffset_2 = src_head.Offset_2;
      }
      get_headval(&user_head,type);
      if(user_head.init != 0 && (user_head.BSize_1 != src_head.BSize_1 ||
	user_head.BSize_2 != src_head.BSize_2 ||
	user_head.Dim_1 != tstDim_1 || user_head.Dim_2 != tstDim_2 ||
	user_head.Offset_1 != tstOffset_1 || user_head.Offset_2 != tstOffset_2))
	old_where[type] = no_stor;
    } else
      old_where[SRCTYP] = no_stor;
    if(old_where[type] == no_stor && current_shm[type] != NULL) {
      shmdt((void *)current_shm[type]);
    }
  }

  /*
   * If there is a file name in the input "file" argument, find out if the file
   * has changed since the last access.
   *
   * It is defined as having changed if any of the following is true:
   * - the previous data storage was not a file;
   * - the current file name is different from the previous one;
   * - the current file name is identical to the previous one, but the
   *   modification time has changed.
   *
   * If the current file cannot be accessed, return with error.
   *
   * If there is no file name in the input "file" argument (i.e. NULL pointer),
   * the file is defined as not having changed.
   */
  if(file) {
    if(stat(file,&stat_buf) != 0)
      goto get_buffer_error;
    if(old_where[type] != fil_stor || strcmp(file,old_file[type]) != 0 ||
      (int)stat_buf.st_mtime != last_mtime[type]) {
      file_changed = 1;
      last_mtime[type] = (int)stat_buf.st_mtime;
    }
  }

  /*
   * Free the old buffers if the file has changed (then new buffers need to be
   * allocated) or if there was a file before but there is none now (then the
   * buffers are no longer needed).
   */
  if(file_changed || (file == NULL && old_where[type] == fil_stor)) {
    clean_buffer(buffer_ptr,type,1);
    if(old_file[type] != NULL) {
      pfree(old_file[type]);
      old_file[type] = NULL;
    }
  }

  /*
   * For input data types, remove the data buffer if there is neither a file nor
   * a shared memory segment where the data can be obtained from. The data
   * buffer is then obviously not needed.
   *
   * For output data types, it is normal not to have input data. The buffer that
   * is obtained here is needed to store the output data. Therefore, if there is
   * no shared memory segment for them, a buffer of the appropriate size must be
   * allocated if it does not yet exist.
   *
   * Output data types are "corrected", "azimuth" and "averaged". The types
   * "x-distortion" and "y-distortion" can either be input or output data.
   *
   * In all cases, create or reset the internal header and history buffers and
   * return without error afterwards. This is considered a state where the input
   * buffer has changed (return code 1).
   *
   * Note that in order for this code to work properly, it is assumed that the
   * data types "corrected", "azimuth" and "averaged" will always have a NULL
   * file pointer associated with them.
   */
  if(file == NULL && shm_id == -1) {
    edf_new_header(*(typestr + type));
    edf_history_new(*(typestr + type));
    /*
     * Output data types.
     */
    if(type == CORTYP || type == AZITYP || type == AVETYP || type == SDXTYP ||
       type == SDYTYP || type == SDMTYP) {

      /*
       * Better defaults could be calculated (these are sometimes useful values) 
       * It would be more adequate to use the parameters of CORTYP.
       */
      if (type == AZITYP) {
        if (*cols <=0 )
          *cols = (int) sqrt(src_head.Dim_1*src_head.Dim_1+src_head.Dim_2*src_head.Dim_2);
        if (*rows <=0 )
          *rows = 360;
      }

      if (type == SDXTYP || type == SDYTYP) {
        /* 
         * If available, get default values from SRCTYP 
         */
        if (*cols <=0 )
          *cols = src_head.Dim_1+1;
        if (*rows <=0 )
          *rows = src_head.Dim_2+1;

        if (src_head.init & FL_OFFS1) {
          user_head.Offset_1 = src_head.Offset_1-0.5;
          user_head.init |= FL_OFFS1;
        }
        if (src_head.init & FL_OFFS2) {
          user_head.Offset_2 = src_head.Offset_2-0.5;
          user_head.init |= FL_OFFS2;
        }
        if (src_head.init & FL_BSIZ1) {
          user_head.BSize_1 = src_head.BSize_1;
          user_head.init |= FL_BSIZ1;
        }
        if (src_head.init & FL_BSIZ2) {
          user_head.BSize_2 = src_head.BSize_2;
          user_head.init |= FL_BSIZ2;
        }
        if (src_head.init & FL_PSIZ1) {
          user_head.PSize_1 = src_head.PSize_1;
          user_head.init |= FL_PSIZ1;
        }
        if (src_head.init & FL_PSIZ2) {
          user_head.PSize_2 = src_head.PSize_2;
          user_head.init |= FL_PSIZ2;
        }
      }

      if (type == SDMTYP) {
        /* 
         * If available, get default values from SRCTYP 
         */
        if (*cols <=0 )
          *cols = src_head.Dim_1;
        if (*rows <=0 )
          *rows = src_head.Dim_2;

        if (src_head.init & FL_OFFS1) {
          user_head.Offset_1 = src_head.Offset_1;
          user_head.init |= FL_OFFS1;
        }
        if (src_head.init & FL_OFFS2) {
          user_head.Offset_2 = src_head.Offset_2;
          user_head.init |= FL_OFFS2;
        }
        if (src_head.init & FL_BSIZ1) {
          user_head.BSize_1 = src_head.BSize_1;
          user_head.init |= FL_BSIZ1;
        }
        if (src_head.init & FL_BSIZ2) {
          user_head.BSize_2 = src_head.BSize_2;
          user_head.init |= FL_BSIZ2;
        }
      }

      if(*cols <= 0 || *rows <= 0) {
	errno = 0;
	prmsg(ERROR,("illegal dimensions [%d,%d] for %s buffer\n",*cols,*rows,
	  *(typestr + type)));
	goto get_buffer_error;
      }
      if(*buffer_ptr != NULL)
	pfree(*buffer_ptr);

      if((*buffer_ptr = (void *)pmalloc(*cols * *rows * sizeof(float))) == NULL)
	goto get_buffer_error;

      user_head.Dim_1 = *cols;
      user_head.Dim_2 = *rows;
      user_head.init |= FL_DIM1 | FL_DIM2;

      set_headval(user_head,type);
    /*
     * Input data types.
     */
    } else if(*buffer_ptr != NULL)
      clean_buffer(buffer_ptr,type,1);

    old_where[type] = no_stor;
    return(1);
  }

  /*
   * Input from file:
   *
   * If the file has changed or if there is a file but the buffer pointer is
   * NULL, create or reset the internal header and history buffers, read the
   * data into a buffer, fill in the old file name and set the access type to
   * "fil_stor" for "file access". The allocation of the data buffer is done in
   * read_esrf_file().
   */
  if(file_changed || file != NULL && *buffer_ptr == NULL) {
    edf_new_header(*(typestr + type));
    edf_history_new(*(typestr + type));

    prmsg(DMSG,("Reading %s file %s\n",*(typestr + type),file));
    if(read_esrf_file(file,buffer_ptr,rows,cols,type,MFloat,&err)) {
      errno = 0;
      prmsg(ERROR,("error reading %s file %s\n",*(typestr + type),file));
      goto get_buffer_error;
    }

    old_file[type] = pmalloc(strlen(file) + 1);
    strcpy(old_file[type],file);
    old_where[type] = fil_stor;
  }

  /*
   * Input from shared memory segment:
   *
   * If the data storage is not a file but a shared memory segment, attach the
   * shared memory segment to the process' data segment. Its data buffer can
   * then be accessed through the address stored in "buffer_ptr".
   *
   * If the shared memory segment cannot be attached, return with error.
   *
   * If the shared memory segment can be attached, test whether it has changed
   * since the last access.
   *
   * It is defined to have changed if any of the following is true:
   * - the previous data storage was not a shared memory segment;
   * - the current shared memory address is different from the previous one;
   * - the current shared memory address is identical to the previous one, but
   *   the modification counter has changed.
   *
   * If the shared memory segment has not changed, then the old buffer still
   * contains valid data and need not be changed.
   *
   * If the shared memory segment has changed, a new data buffer needs to be
   * assigned. Test the data type of the shared memory segment:
   * - it must be "string" (SHM_STRING) for header data (type == HD_TYP);
   * - it must be "float" (SHM_FLOAT) for corrected (type == CORTYP), flood
   *   field (FLOTYP), azimuthal (AZITYP) and averaged (AVETYP) data;
   * - for all other cases, it must be "unsigned character" (SHM_UCHAR),
   *   "unsigned short" (SHM_USHORT), "unsigned / signed long" (SHM_ULONG /
   *   SHM_LONG), or "float" (SHM_FLOAT).
   *
   * Note that in the above definitions, for SPEC "LONG" means 32 bit.
   *
   * If the shared memory segment contains an illegal data type, return with
   * error.
   *
   * Otherwise, update the shared memory address and modification counter and
   * reset the internal header and history buffers.
   */
  if(file == NULL && shm_id != -1) {
    if((shm_ptr = (void *)getShmPtr(shm_id,0)) == NULL) {
      prmsg(ERROR,("%s shared memory id %d not found\n",*(typestr + type),
	shm_id));
      goto get_buffer_error;
    }

    if(old_where[type] != shm_stor || current_shm[type] != shm_ptr ||
      getShmUTime( shm_ptr) != last_utime[type]) {

      hdtype = getShmType(shm_ptr);
      if(((type == SRCTYP || type == DRKTYP || type == SBKTYP) &&
	hdtype != SHM_UCHAR && hdtype != SHM_USHORT && hdtype != SHM_ULONG &&
        hdtype != SHM_LONG && hdtype != SHM_FLOAT) ||
	((type == CORTYP || type == FLOTYP || type == AZITYP || type == AVETYP)
	&& hdtype != SHM_FLOAT) || (type == HD_TYP && hdtype != SHM_STRING))
      {
	errno = 0;
	prmsg(ERROR,("only %s supported for %s\n",
	  (type == CORTYP || type == FLOTYP || type == AZITYP || type == AVETYP)
	  ? "float" : ((type == HD_TYP) ? "string" :
	  "unsigned char, unsigned short, unsigned/signed long, or float"),
          *(typestr + type)));
	last_utime[type] = 0;
	goto get_buffer_error;
      }

      shm_changed = 1;

      /*
       * Free the buffer pointer if it is not NULL and if it is not pointing
       * to the previous shared memory buffer.
       */
      
      if(*buffer_ptr && current_shm[type] && 
          current_shm_data[type]!=*buffer_ptr) {
	pfree(*buffer_ptr);
	*buffer_ptr = NULL;
      }

      current_shm[type] = shm_ptr;
      current_shm_data[type] = getShmDataPtr(shm_ptr,0);
      last_utime[type] = getShmUTime(shm_ptr);
      old_where[type] = shm_stor;
      edf_new_header(*(typestr + type));
      edf_history_new(*(typestr + type));

      /*
       * Use the "rows" and "cols" information that was provided in the shared
       * memory header
       * - for the azimuthal averaging (AVETYP) buffer: to check if the "rows"
       *   of the buffer are at least what was requested by the input arguments
       *   and if not, if at least 4 rows (one averaging angle) can be used;
       * - for the other data buffers: to set the "rows" and "cols" arguments;
       * - for header buffers: to set the number of header items and the length
       *   of each item.
       */
      if(type != HD_TYP) {
	if(type == AVETYP) {
	  *cols = getShmCols(shm_ptr);
	  if(*rows > getShmRows(shm_ptr)) {
	    if(*rows == 8 && getShmRows(shm_ptr) >= 4)
	      *rows = 4;
	    else {
	      errno = 0;
	      prmsg(ERROR,
		("wrong shared memory dimensions found for %s buffer\n",
		*(typestr + type)));
	      goto get_buffer_error;
	    }
	  }
	} else {
	  *rows = getShmRows(shm_ptr);
	  *cols = getShmCols(shm_ptr);
          //++++++++ previous position of imgsiz = *cols * *rows;
	}
	imgsiz = *cols * *rows;

	/*
	 * Put a default header in the header structure. It contains those
	 * elements of the online header designated by the macro FL_IMAGE.
	 * However, the header structure of the corrected image always contains
	 * the same elements as the source image.
	 *
	 * For the source image, get the header structure here and make sure
	 * that header elements Dim_1 and Dim_2 are defined.
	 */
	if(type != SRCTYP) {
	  if(type == CORTYP)  //original
	     user_head = src_head;
	  else {
	    get_headval(&user_head,HD_TYP);
	    user_head.init &= FL_IMAGE;
	  }
	  set_headval(user_head,type);
	} else {
	  get_headval(&src_head,SRCTYP);
	  src_head.Dim_1 = *cols;
	  src_head.Dim_2 = *rows;
	  src_head.init |= FL_DIM1 | FL_DIM2;
	  set_headval(src_head,SRCTYP);
	}

      } else {
	headrows = getShmRows(shm_ptr);
	headcols = getShmCols(shm_ptr);
	/*
	 * Write the header information from the online header buffer into an
	 * internal EDF buffer. The online header buffer can have two different
	 * formats:
	 *
	 * 1) each keyword / value pair is contained on a single line, separated
	 *    by the separator symbol "=" and possibly some blanks on either or
	 *    both sides of the separator;
	 *
	 * 2) each keyword / value pair is contained on two consecutive lines,
	 *    the first line containing the keyword and the second one the
	 *    value. In this case there is no separator.
	 *
	 * In both cases, there can be empty lines before or after the ones
	 * containing the keyword / value information.
	 */

        /* shm_ptr is attached, getShmDataPtr can be used without problems */
	hdptr = (char *) getShmDataPtr(shm_ptr,0);
	hdend = hdptr + headrows * headcols;
	for(; hdptr < hdend; hdptr += headcols) {
	  if(*hdptr == '\0')
	    continue;
	  /*
	   * If the first non-empty header line does not contain an "=" (equal
	   * sign), then the header contains keywords and values on consecutive
	   * lines, without any separating "=".
	   */
	  if(strchr(hdptr,'=') == NULL) {
	    if(sscanf(hdptr,"%[^\n]",id) != 1)
	      continue;
	    hdptr += headcols;
	    /*
	     * This is to have for the "sscanf()" format string a maximum field
	     * width specified by the "EdfMaxValLen" macro. The line expands to
	     *
	     * if(sscanf(hdptr,"%EdfMaxValLen[^\n]",val) != 1)
	     *
	     * but with "EdfMaxValLen" replaced by the macro's value.
	     */
	    if(sscanf(hdptr,FMT1(EdfMaxValLen),val) != 1)
	      continue;
	  /*
	   * If the first non-empty header line does contain an "=" (equal
	   * sign), then the header contains in each line the sequence
	   * "keyword = value".
	   *
	   * Note that this way the possibility that the value itself might
	   * contain an "=" is also covered, as the keyword does never contain
	   * an "=" and the test for the non-existence of an "=" in the first
	   * line (i.e., the keyword line) is therefore conclusive.
	   */
	  } else {
	    /*
	     * This line expands to
	     *
	     * if(sscanf(hdptr,"%[^= ]%*[ =]%EdfMaxValLen[^\n]",id,val) != 2)
	     *
	     * similar to the FMT1() macro call above.
	     */
	    if(sscanf(hdptr,FMT2(EdfMaxValLen),id,val) != 2)
	      continue;
	  }
/*
	  prmsg(DMSG,("Read header keyword %s, value = %s\n",id,val));
*/
	  edf_add_header_element(*(typestr + type),id,val,&err,&status);
	  if(status != status_success) {
	    prmsg(ERROR,("error writing internal header buffer: %s\n",
	      edf_report_data_error(err)));
	    return(-1);
	  }
	}
      }

      /*
       * If the image is an input (source), dark, scattering background or flood
       * field image, then allocate a new buffer to store the data in. The main
       * purpose of this is to have the shared memory segment available as soon
       * as possible to be filled with new data from a data acquisition process.
       *
       * Otherwise, the shared memory segment itself is used as the buffer.
       */

      /* shm_ptr is attached, getShmDataPtr can be used without problems */
      *buffer_ptr = getShmDataPtr(shm_ptr,0);

      if(type == SRCTYP || type == DRKTYP || type == SBKTYP || type == FLOTYP) {
	float_ptr = (float *)pmalloc(imgsiz * sizeof(float));
	flt_buf = float_ptr + imgsiz - 1;
	switch(hdtype) {
	case SHM_UCHAR:
	  uch_buf = (unsigned char *)*buffer_ptr + imgsiz - 1;
	  for(; flt_buf >= float_ptr;)
	    *flt_buf-- = *uch_buf--;
	  break;
	case SHM_USHORT:
	  ush_buf = (unsigned short *)*buffer_ptr + imgsiz - 1;
	  for(; flt_buf >= float_ptr;)
	    *flt_buf-- = *ush_buf--;
	  break;
	case SHM_ULONG:
	  uin_buf = (unsigned int *)*buffer_ptr + imgsiz - 1;
	  for(; flt_buf >= float_ptr;)
	    *flt_buf-- = *uin_buf--;
	  break;
	case SHM_LONG:
	  int_buf = (int *)*buffer_ptr + imgsiz - 1;
	  for(; flt_buf >= float_ptr;)
	    *flt_buf-- = *int_buf--;
	  break;
	case SHM_FLOAT:
	  memcpy(float_ptr,*buffer_ptr,imgsiz * sizeof(float));
	}
	*buffer_ptr = float_ptr;
      }
    }
  }

  /*
   * Test if the image buffer needs to be mapped, i.e. if its geometry has
   * changed. Note: this test is not relevant for the online header buffer.
   *
   * For all image types except the source image, there must no longer be
   * undefined dimensions. If there are, return with error.
   *
   * For the source image, undefined dimensions mean that the image has already
   * been acquired before and therefore its dimensions have not been set anew
   * by the code above. Get the actual values from the corresponding header,
   * if they are defined. If not, return with error.
   */
  if(type != HD_TYP && (*cols <= 0 || *rows <= 0)) {
    if(type == SRCTYP && src_head.init & FL_DIM1 && src_head.init & FL_DIM2) {
      *cols = src_head.Dim_1;
      *rows = src_head.Dim_2;
    } else {
      errno = 0;
      prmsg(ERROR,("no dimensions found for %s buffer\n",*(typestr + type)));
      goto get_buffer_error;
    }
  }

  /*
   * For input image types, test if they need to be mapped.
   */
  if(type == DRKTYP || type == FLOTYP || type == SBKTYP || type == MSKTYP ||
    (type == SDXTYP || type == SDYTYP) && file != NULL) {

    get_headval(&user_head,type);
    bu1 = user_head.BSize_1;
    bu2 = user_head.BSize_2;
    bs1 = src_head.BSize_1;
    bs2 = src_head.BSize_2;
    /*
     * If the binning, the dimensions and the offsets are the same as for the
     * source image, the image does not need to be mapped.
     *
     * Displacement files (type SDXTYP and SDYTYP) always need to be mapped as
     * they may contain "displaced" image parameters that need to be processed.
     *
     * Dark image files also need to be mapped when they have changed as the
     * linearity correction may have to be applied.
     *
     */
    if(type == SDXTYP || type == SDYTYP || type == SDMTYP || 
      bu1 != bs1 || bu2 != bs2 || type == DRKTYP && file_changed | shm_changed ||
      user_head.Dim_1 != src_head.Dim_1 || user_head.Dim_2 != src_head.Dim_2 ||
      user_head.Offset_1 != src_head.Offset_1 ||
      user_head.Offset_2 != src_head.Offset_2) {

      /*
       * If source image is not fully contained in this image, return with
       * error.
       */

      if(region_compare(type,
	user_head.Offset_1,user_head.BSize_1,(float)user_head.Dim_1,
	user_head.Offset_2,user_head.BSize_2,(float)user_head.Dim_2,
	src_head.Offset_1,src_head.BSize_1,(float)src_head.Dim_1,
	src_head.Offset_2,src_head.BSize_2,(float)src_head.Dim_2) == -1) {
	prmsg(MSG,
	  ("%s does not cover full source region, correction not possible\n",
	  *(typestr + type)));
	goto get_buffer_error;
      }
      if(map_imag(*buffer_ptr,&pnewbuf,bs1 / bu1,bs2 / bu2,type) < 0)
	goto get_buffer_error;
      else if(*buffer_ptr != pnewbuf) {
	if(type == DRKTYP)
	  pfree(*buffer_ptr);
	else
	  clean_buffer(buffer_ptr,type,1);
	*buffer_ptr = pnewbuf;
	get_headval(&user_head,type);
	*cols = user_head.Dim_1;
	*rows = user_head.Dim_2;
      }
    }
  }

  prmsg(DMSG,("get_buffer(shm,file,buff,rows,cols,type): %d, %s, %p, %d, %d, %s\n",
    shm_id, file ? file : "<null>",*buffer_ptr,*rows,*cols,*(typestr + type)));
  return(file_changed | shm_changed);

get_buffer_error:
  prmsg(ERROR,("error get_buffer(shm,file,buff,rows,cols,type): %d, %s, %p, %d, %d, %s\n",
    shm_id,file ? file : "<null>",*buffer_ptr,*rows,*cols,*(typestr + type)));
  clean_buffer(buffer_ptr,type,1);
  old_where[type] = no_stor;
  edf_new_header(*(typestr + type));
  edf_history_new(*(typestr + type));
  return(-1);
} /* get_buffer */

/*==============================================================================
 * Save the data buffer to a file or to the shared memory segment.
 *
 * If there is an output data file defined, write the contents of the data
 * buffer to the output file. Additionally the header information will be
 * written. It consists of the following parts:
 * - the standard header as provided by the EDF output routines. It contains at
 *   least: EDF_DataBlockID, EDF_BinarySize, HeaderID, ByteOrder, DataType,
 *   Dim_1, Dim_2;
 * - the content of the header structure. This is filled by the input header
 *   data in the distortion file, the EDF data file or the online header shared
 *   memory segment;
 * - the content of the program-internal history structure, describing the
 *   actions that the program has performed on the data.
 *
 * If there is a shared memory segment defined for the type of data to be
 * written (given in "type"), then its modification counter will be increased.
 *
 * Both, only one or none of output data file and shared memory segment for data
 * might be defined.
 *
 * Input : file:       file name of the output data file, or NULL if not defined
 *         buffer_ptr: data buffer
 *         rows:       number of elements in the second dimension of the image
 *                     (this is the "slow-moving" index, i.e. the first index
 *                     in a two-dimensional C data buffer)
 *         cols:       number of elements in the first dimension of the image
 *                     (this is the "fast-moving" index, i.e. the second index
 *                     in a two-dimensional C data buffer)
 *         type:       type of data buffer to be written (see in get_buffer())
 * Output: none
 * Return: -1  if errors
 *          0  else
 */

int put_buffer(char *file,void **buffer_ptr,int rows,int cols,int type)
{
  static char id[EdfMaxKeyLen + 1],val[EdfMaxValLen + 1];

  void *outptr;
  char *pkey,*pval;
  int i,err,status,mtype = MFloat;
  int valset;
  SHM_HEADER *shm_ptr = current_shm[type];
  struct data_head user_head;

  /*
   * Check whether the type of data requested is a valid one.
   */
  if(!(type == SRCTYP || type == CORTYP || type == DRKTYP || type == SDXTYP ||
    type == SDYTYP || type == SDMTYP || type == AZITYP || type == AVETYP || 
    type == HD_TYP)) {
    errno = 0;
    prmsg(FATAL,("invalid data type %d for put_buffer().\n",type));
  }

  /*
   * If the input source image is a shared memory that is saved to a file, then
   * the complete online header is always written to the file as well; but the
   * header information from the input source file is only passed on to
   *
   * - the corrected output file if requested with "headpass";
   * - the azimuthally regrouped output file if requested with "azim_pass".
   */
  if(type == SRCTYP ||
    headpass && type == CORTYP || azim_pass && type == AZITYP) {

    int prerotpass = ((type == SRCTYP) || (headpass && (!get_doprerot())))?1:0;

    edf_first_header_element(*(typestr + HD_TYP),(const char **)&pkey,
      (const char **)&pval,&err,&status);
    while(pkey != NULL) {
      /*
       * Do not pass any PREROT parameters to corrected images when do_prerot 
       * is set, otherwise it would mean that the corrected data need still to 
       * be prerotated.
       */
      if ( (prerotpass) || (is_not_prerotpar( pkey )) ) {
        edf_add_header_element(*(typestr + type),pkey,pval,&err,&status);
        if(status != status_success) {
          prmsg(ERROR,("error copying %s header value: %s\n",pkey,
            edf_report_data_error(err)));
	  return(-1);
        }
      }
      edf_next_header_element(*(typestr + HD_TYP),(const char **)&pkey,
	(const char **)&pval,&err,&status);
    }
  }

  /*
   * Update the header information with the values from the header structure
   * (not necessary for the input source image).
   */
  if(type != SRCTYP) {
    get_headval(&user_head,type);

    for(i = 0; i < maxhdkey; i++) {
      valset=1;
      if(user_head.init & 1 << i) {
	strcpy(id,*(headkey + i));
	switch(i) {
	case 0:
	  sprintf(val,"%ld",user_head.Orientat);
	  break;
	case 1:
	  sprintf(val,"%g",user_head.Dummy);
	  break;
	case 2:
	  sprintf(val,"%g",user_head.DDummy);
	  break;
	case 3:
	  sprintf(val,"%g pixel",user_head.Offset_1);
	  break;
	case 4:
	  sprintf(val,"%g pixel",user_head.Offset_2);
	  break;
	case 5:
	  sprintf(val,"%g m",user_head.PSize_1);
	  break;
	case 6:
	  if(type != AZITYP)
	    sprintf(val,"%g m",user_head.PSize_2);
          else 
	    sprintf(val,"%g rad",user_head.PSize_2);
	  break;
	case 7:
	  strcpy(val,user_head.Intens_0);
	  break;
	case 8:
	  strcpy(val,user_head.Intens_1);
	  break;
	case 9:
	  sprintf(val,"%g pixel",user_head.Center_1);
	  break;
	case 10:
	  sprintf(val,"%g pixel",user_head.Center_2);
	  break;
	case 11:
	  sprintf(val,"%g m",user_head.SamplDis);
	  break;
	case 12:
	  sprintf(val,"%g m",user_head.WaveLeng);
	  break;
	case 13:
	  if(type == DRKTYP)
	    strcpy(val,"Dark Image");
	  else
	    strcpy(val,user_head.Title);
	  break;
	case 14:
	  strcpy(val,user_head.Time);
	  break;
	case 15:
	  strcpy(val,user_head.ExpTime);
	  break;
	case 16:
	  sprintf(val,"%g",user_head.BSize_1);
	  break;
	case 17:
	  sprintf(val,"%g",user_head.BSize_2);
	  break;
	case 18:
	  sprintf(val,"%d",user_head.Dim_1);
	  break;
	case 19:
	  sprintf(val,"%d",user_head.Dim_2);
	  break;
	case 20:
	  strcpy(val,user_head.ProjTyp);
	  break;
	case 21:
	  sprintf(val,"%g_deg",RAD2DEG(user_head.DetRot_1));
	  break;
	case 22:
	  sprintf(val,"%g_deg",RAD2DEG(user_head.DetRot_2));
	  break;
	case 23:
	  sprintf(val,"%g_deg",RAD2DEG(user_head.DetRot_3));
          break;
        default: 
          valset=0;
	}

        if (valset) {
	  edf_add_header_element(*(typestr + type),id,val,&err,&status);
	  if(status != status_success) {
	    prmsg(ERROR,("error updating %s header value %s: %s\n",id,val,
	      edf_report_data_error(err)));
	    return(-1);
	  }
        }
      }
    }
  }

  /*
   * Copy the history from the internal history structure to the header
   * structure. Note that for the corrected image, this also means that the
   * history will be written back to the online header, if there is one.
   */
  edf_history_write_header(*(typestr + type),*(typestr + type),&err,&status);

  /*
   * For corrected, azimuthal, averaged and spatial distortion displacement data
   * the buffer to be written has been created by the program.
   *
   * If only header data are to be saved to a special output file, no data
   * buffer is required.
   *
   * For the other types (source and dark image), there must be an input shared
   * memory to be written to file. If not, return with error.
   */
  if(type == CORTYP || type == AZITYP || type == AVETYP || type == SDXTYP ||
    type == SDYTYP || type == SDMTYP) {
    outptr = *buffer_ptr;

    /*
     * If a shared memory segment is defined for these data types, increase its
     * modification counter. If there was also an online header for the source
     * data, write the header values of the corrected image back to the online
     * header and increase its modification counter as well.
     */
    if(shm_ptr) {

      incShmUTime(shm_ptr);

      if(type == CORTYP && (shm_ptr = current_shm[HD_TYP]) != NULL) {
	char *hdptr,*hdend;
	int headrows,headcols;

	headrows = getShmRows(shm_ptr);
	headcols = getShmCols(shm_ptr);
        /* shm_ptr is attached, getShmDataPtr can be used without problems */
        hdptr = (char *) getShmDataPtr(shm_ptr,0);
	hdend = hdptr + headrows * headcols;

	/*
	 * Reset the online header to empty lines.
	 */
	for(; hdptr < hdend; hdptr += headcols)
	  *hdptr = '\0';

	/*
	 * Write the keywords and values of CORTYP header to the online header.
	 */
        /* shm_ptr is attached, getShmDataPtr can be used without problems */
        hdptr = (char *) getShmDataPtr(shm_ptr,0);

	edf_first_header_element(*(typestr + CORTYP),(const char **)&pkey,
	  (const char **)&pval,&err,&status);
	while(pkey != NULL) {
	  strcpy(hdptr,pkey);
	  hdptr += headcols;
	  strncpy(hdptr,pval,headcols - 1);
	  *(hdptr + headcols - 1) = '\0';
	  hdptr += headcols;
	  edf_next_header_element(*(typestr + CORTYP),(const char **)&pkey,
	    (const char **)&pval,&err,&status);
	}

        incShmUTime(shm_ptr);
      }
    }

  } else if(type == HD_TYP) {
    outptr = NULL;
  } else {
    if(shm_ptr) {
      rows = getShmRows(shm_ptr);
      cols = getShmCols(shm_ptr);
      switch(getShmType(shm_ptr)) {
      case SHM_UCHAR:
	mtype = edf_datatype2machinetype(Unsigned8);
	break;
      case SHM_USHORT:
	mtype = edf_datatype2machinetype(Unsigned16);
	break;
      case SHM_ULONG:
	mtype = edf_datatype2machinetype(Unsigned32);
	break;
      case SHM_LONG:
	mtype = edf_datatype2machinetype(Signed32);
	break;
      }
      /* shm_ptr is attached, getShmDataPtr can be used without problems */
      outptr = getShmDataPtr(shm_ptr,0);
    } else {
      prmsg(ERROR,("no shared memory segment to save for %s image.\n",
	*(typestr + type)));
      return(-1);
    }
  }

  /*
   * If an output file is defined, write to it the data and possibly additional
   * header information from shared memory segment.
   */
  if(file) {
    prmsg(MSG,("Saving %s image to file %s\n",*(typestr + type),file));
    return(save_esrf_file(file,outptr,rows,cols,type,mtype));
  }
  return(0);
} /* put_buffer */

/*==============================================================================
 * Detaches from shared memory and optionally deletes the data buffer.
 *
 * If there is a shared memory segment associated with the data, detach from it.
 *
 * If there was a buffer allocated for the data storage, delete it if the
 * "free_flag" is set.
 *
 * Note that if the data came from a file, there is always a data buffer
 * allocated for it, but no shared memory segment is associated with it. If
 * the data came from a shared memory segment, this memory segment might be
 * used as the data buffer (in which case there is no buffer allocated), or
 * a new buffer might be allocated to hold a copy of the data in the memory
 * segment (in which case there will be both a memory segment and an allocated
 * buffer for the data). For details, see routine get_buffer().
 *
 * Input : buffer_ptr: data buffer, or NULL if none available
 *         type:       type of data buffer to be obtained (see in get_buffer())
 *         free_flag:  flag to determine if an allocated data buffer will be
 *                     deleted: delete buffer if set, otherwise keep it
 * Output: buffer_ptr: data buffer (may have been set to NULL)
 * Return: 0
 */

int clean_buffer(void **buffer_ptr,int type,int free_flag)
{
  SHM_HEADER *shm_ptr = current_shm[type];
  void *shm_data_ptr = current_shm_data[type];

  if(shm_ptr != NULL) {
    shmdt((void *)shm_ptr);
  }

  /*
   * If there is a shared memory segment and an allocated buffer for this data,
   * then the pointer handed over as input argument points to the allocated
   * buffer and will be different from the pointer to the memory segment in the
   * array "current_shm". The allocated buffer is then freed. If the two
   * pointers are identical, the input pointer points to the memory segment and
   * must not be freed!
   */

  if(free_flag && *buffer_ptr) {
    if(shm_ptr == NULL || shm_data_ptr != *buffer_ptr) {
      pfree(*buffer_ptr);
    }
    *buffer_ptr = NULL;
    current_shm[type] = NULL;
    current_shm_data[type] = NULL;
  }
  return(0);
}

/*==============================================================================
 * Sets globally the data type of all output edf files.
 *
 * If out_type is NULL or an empty string the output data type is reset to
 * default.
 *
 * Input : new value for output type
 * Output: none
 * Return: none
 */
void set_type(char *out_type)
{
  int datatype_out;

  /*
   * Test if output type is NULL or empty.
   */
  if(!out_type)
    out_type="";
  if(strlen(out_type)) {
    /*
     * If not, convert string to data type and set output data type of all
     * output files, including xy-shift files.
     */
    datatype_out = edf_string2datatype(out_type);
    if(datatype_out!=InValidDType)
      edf_set_datatype(datatype_out);
  /*
   * If NULL or empty, set data type to default (no conversion).
   */
  } else
    edf_set_datatype(InValidDType);
}

/*==============================================================================
 * Sets globally the data value offset for all output edf files.
 *
 * Input : new value for dvo
 * Output: none
 * Return: none
 */
void set_dvo(long dvo)
{
  /*
   * Set data value offset of all (!) output files (also xy-shift files!)
   */
  edf_set_datavalueoffset(dvo);
}

/*==============================================================================
 * Analyze the input arguments, set the parameters of the correction routines
 * accordingly, get all required image data (input image, dark image image,
 * etc.) and call the correction routines.
 *
 * The input arguments consist of options and file names in the form
 *
 *    opt1=val1 opt2=val2 opt3=val3 filename1 [filename2 ...]
 *
 * Options have the form "name=value" and are distinguished from the file name
 * arguments by the presence of the equal sign "=".
 *
 * No option needs to be given, and no filename needs to be given either.
 * However, at least an input image must be specified, either with the "src_id"
 * option or by giving at least one filename.
 *
 * The routine processes first the options, until it encounters the first input
 * argument that is not an option (i.e. that does not contain a "=") or when
 * all input arguments have been processed. Any input arguments that might be
 * left in the input line are considered to be file names for the input image
 * file.
 *
 * Note that it is therefore required that all options precede the input file
 * names, as any option following the first file name will also be considered
 * to be a file name.
 *
 * The order of the options is normally not relevant, e.g. the dark image
 * options can be specified before or after the flood field options. However,
 * some options are mutually exclusive (e.g., there are three options to
 * specify the dark image source, but they cannot all be specified at the same
 * time). In these cases, the order is important, and the last option given
 * is the one that is retained.
 *
 * The option "clear" is a special case: it will reset all previously set
 * option values to their default values. It therefore makes only sense if
 * given at the beginning of a series of options for a particular input image,
 * to reset options that might have been set for previous images. If given in
 * the middle of a series of options for one image, all options just set before
 * "clear" for this image will be reset, which is probably not what was
 * intended.
 *
 * If there is more than one file name specified, the routine will process them
 * all, in the order given by the input arguments.
 *
 * The routine returns an error if at least one of the input files was not
 * processed successfully. However, subsequent files in the input string are
 * still processed and may be corrected successfully.
 *
 * The possible options are described below in alphabetical order.
 *
 * active_radius   defines a circular "active area" in the image. No spatial
 *                 distortion corrections are done for pixels outside this area
 *                 (default: 0., i.e. no "active area" used)
 *
 * ave_ext         filename extension for the azimuthal averaged output image
 *                 file (default: empty string, i.e. no output file written).
 *                 The base of the filename is in "base_name"
 * ave_id          shared memory segment identifier for the azimuthal averaged
 *                 output image (default: -1, i.e. no identifier specified)
 *
 *                 The two options ave_ext and ave_id specify the output for
 *                 the azimuthal averaged image. This can be:
 *                 - ave_ext:  a output image file;
 *                 - ave_id:   a shared memory segment.
 *
 *                 The default values are that no output for the azimuthal
 *                 averaged image will be produced (no shared memory segment
 *                 and no filename extension specified).
 *
 * ave_scf         scale factor for "s" values of the averaged image
 *                 (default: 1.)
 *
 * azim_a0         start angle for 1st azimuthal regrouping (default: 0.0)
 * azim_a1         start angle for 2nd azimuthal regrouping (default: not used)
 * azim_a_num      angular dimension of output buffer for azimuthal regrouping
 *                 (default: 0)
 * azim_da         angle interval for azimuthal regrouping (default: 1.0)
 * azim_ext        filename extension for the azimuthal regrouped output image
 *                 file (default: empty string, i.e. no output file written).
 *                 The base of the filename is in "base_name"
 * azim_id         shared memory segment identifier for the azimuthal regrouped
 *                 output image (default: -1, i.e. no identifier specified)
 *
 *                 The two options azim_ext and azim_id specify the output for
 *                 the azimuthal regrouped image. This can be:
 *                 - azim_ext:  a output image file;
 *                 - azim_id:   a shared memory segment.
 *
 *                 The default values are that no output for the azimuthal
 *                 regrouped image will be produced (no shared memory segment
 *                 and no filename extension specified).
 *
 * azim_int        if set, regroupe the image along the azimuth angle
 *                 (default: 0, i.e. no regrouping)
 * azim_pass       if set, all header values of the corrected image are passed
 *                 to the azimuthal regrouped image (default: 1, i.e. set)
 *
 * azim_pro        choose "Saxs" or "Waxs" projection for azimuthal image.
 *                 in case of "Waxs" a "Saxs" input image is projected to the
 *                 Ewald sphere taking into account the detector rotations.
 *                 (default: not used). 
 *
 * azim_r0         minimum radius for azimuthal regrouping (default: 0.0)
 * azim_r_num      radial dimension of output buffer for azimuthal regrouping
 *                 (default: 0)
 *
 * base_name       contains the "file name base" from which all other file names
 *                 are derived (default: "image")
 *
 * bckg_const      constant to be added to the value of all scattering
 *                 background image pixels (default: 0.)
 * bckg_fact       multiplication factor for the value of all scattering
 *                 background image pixels (default: 1.)
 *
 *                 bckg_const and bckg_fact are applied to the scattering
 *                 background image as given in the following formula:
 *                     scattering * bckg_fact + bckg_const
 *
 * bckg_file       name of the file with the scattering background image
 *                 (default: no file, i.e. NULL pointer)
 * bckg_id         shared memory segment identifier for the scattering
 *                 background image (default: -1, i.e. no identifier specified)
 *
 *                 The two options bckg_id and bckg_file specify the input
 *                 source for the scattering background image. This can be:
 *                 - bckg_id:    a shared memory segment;
 *                 - bckg_file:  a scattering background image file.
 *
 *                 Default is no scattering background correction (no shared
 *                 memory segment nor scattering background file defined).
 *
 *                 Note that only one scattering background image input source
 *                 can be specified; if more than one is specified in the input
 *                 arguments, then only the last one will be taken into account.
 *
 * bin_1           number of pixels to bin together in x-direction (default: 1,
 *                 i.e. no binning to be performed)
 * bin_2           number of pixels to bin together in y-direction (default: 1,
 *                 i.e. no binning to be performed)
 *
 * bis_1           if set, defines value for "BSize_1" header keyword.
 * bis_2           if set, defines value for "BSize_2" header keyword.
 *
 * bkg_const       old name of "dark_const" parameter (do not use)
 * bkg_file        old name of "dark_file" parameter (do not use)
 * bkg_id          old name of "dark_id" parameter (do not use)
 *
 * cen_1           if set, defines value for "Center_1" header keyword.
 * cen_2           if set, defines value for "Center_2" header keyword.
 *
 * clear           reset all previously given options to their default values
 *                 (default: 0, i.e. the previously given options are not reset)
 *
 * cor_ext         filename extension for corrected output image file (default:
 *                 ".cor"). The base of the filename is in "base_name".
 *                 Note: if "cor_ext" is set to an empty string, no output file
 *                 is written.
 * cor_file        name of the output file for the corrected image (default: no
 *                 file, i.e. NULL pointer). This option is obsolete and should
 *                 be replaced by "cor_ext" with "base_name".
 * cor_id          shared memory segment identifier for the corrected output
 *                 image (default: -1, i.e. no identifier specified)
 *
 *                 The two options cor_id and cor_file specify the output for
 *                 the corrected image. This can be:
 *                 - cor_id:    a shared memory segment;
 *                 - cor_file:  a output image file.
 *
 *                 Default is no output location (no shared memory segment nor
 *                 output file defined).
 *
 * dark_const      constant for overall dark image subtraction (default: 0.)
 *                 For more details, see "dark_id" below.
 *
 * dark_ext        filename extension for the file with the dark (background)
 *                 image. This is mainly used to save an online dark image to a
 *                 file. (default: empty string, i.e. no output file written).
 *                 The base of the filename is in "base_name"
 *
 * dark_file       name of the file with the dark image (default: no file,
 *                 i.e. NULL pointer)
 *                 If dark_file is a string containing an opening and a closing
 *                 square bracket, then the name of the dark image file is to
 *                 be formed by removing the brackets and replacing the part
 *                 between the brackets by the value of the the keyword
 *                 "char_string" in the header of the source image file.
 *                 Example: 
 *                   with dark_file=../data/[char_string].ext
 *                   and the keyword char_string=gd10dark
 *                   the resulting filename is ../data/gd10dark.ext
 *
 * dark_id         shared memory segment identifier for the dark image
 *                 (default: -1, i.e. no identifier specified)
 *
 *                 The three options dark_const, dark_id and dark_file specify
 *                 the input source for the dark image correction. This can be:
 *                 - dark_id:    a shared memory segment;
 *                 - dark_file:  a dark image file;
 *                 - dark_const: an overall constant to be applied to all pixels
 *                               of the image data.
 *
 *                 Default is no shared memory segment nor dark image file
 *                 defined and the overall dark image constant 0. That also
 *                 implies that by default there is no dark image correction
 *                 performed.
 *
 *                 If a source for the dark image correction is specified, then
 *                 normally the dark image correction is performed. However,
 *                 this can be prevented by setting "do_dark=0" (see below).
 *
 *                 Note that in principle only either a shared memory segment or
 *                 an image file can be specified; if both are specified in the
 *                 input arguments, then the one specified last will be used.
 *
 *                 The overall constant can be specified in addition to either
 *                 of the two other options.
 *
 * distortion_file name of the file with the distortion correction parameters
 *                 (default: empty string, i.e. no file). Note: if "xfile" and
 *                 "yfile" are specified, "distortion_file" is ignored.
 *                 If distortion_file is a string containing an opening and a
 *                 closing square bracket, then the name of the distortion file
 *                 is to be formed by removing the brackets and replacing the
 *                 part between the brackets by the value of the the keyword
 *                 "char_string" in the header of the source image file.
 *                 Example: 
 *                   with distortion_file=../data/[char_string].ext
 *                   and the keyword char_string=gd10dist
 *                   the resulting filename is ../data/gd10dist.ext
 *
 * dis             if set, defines value for "SampleDistance" header keyword.
 *
 * do_dark         if set to 0, no dark image correction is performed, even if
 *                 there is a source for it (dark image buffer, file or constant
 *                 - see "dark_id", "dark_file" and "dark_const" above). The
 *                 purpose of this is to allow a saving of the online dark image
 *                 buffer for a later processing.
 *
 *                 If set to 1, then the dark image correction is performed if 
 *                 there is a source for it. Since 2011-10-23 the default value
 *                 is 0 and set to 1 by either dark_id, dark_file or dark_const,
 *                 if not set explicitely to a different value.
 *
 * do_distortion   controls the distortion correction/prerotation correction:
 *                  0: no distortion/prerotation correction is performed
 *                 >0: distortion/prerotation correction is performed as follows:
 *                     1: after the dark image subtraction (default)
 *                     2: after the floodfield division
 *                     3: after the normalization
 *                     4: after background subtraction.
 *
 *                 Note: regardless of the value of "do_distortion", no
 *                 distortion correction is performed if neither
 *                 "distortion_file" nor "xfile" and "yfile" are specified.
 *
 * do_prerotation  controls the prerotation correction:
 *                 0: no prerotation correction is performed (default)
 *                 1: prerotation correction after distortion correction
 *                 2: prerotation correction without distortion correction.
 *
 * norm_prerotation if set, renormalize the prerotated image according to the
 *                 change in spherical angle covered by each pixel. The
 *                 output intensities correspond to a perpendicular detector
 *                 that is exactly perpendicular to the primary beam.
 *                 (default: 1, renormalization)
 *                 
 * dummy           value that is used to ignore a pixel. If dummy is different
 *                 from 0 all pixels with a value in the range 
 *                 [dummy-ddummy,dummy+ddummy] are not processed in the analysis 
 *                 (default: 0., i.e. not set).
 *
 * dvo             value that defines globally the data value offset of all
 *                 written edf files (default 0). The data value offset is
 *                 converted to a long integer value and must be added to all
 *                 data values stored in the edf file:
 *                 <value> = <value stored in edf file> + <data value offset>
 *                 The conversion is done in the module "edfio" when a file is
 *                 written.
 *
 * flat_after      flag indicating whether the flood field correction is to be
 *                 done before or after the distortion correction. If the flag
 *                 is set, then flood field will be done after distortion
 *                 (default: 1, i.e. set). This is obsolete, use "do_distortion"
 *                 instead.
 * flat_distortion if set, the target image is normalized to a flat image
 *                 (default: 1, i.e. set)
 * flood_file      name of the file with the flood field image (default: no
 *                 file, i.e. NULL pointer)
 *                 If flood_file is a string containing an opening and a closing
 *                 square bracket, then the name of the flood file image file is
 *                 to be formed by removing the brackets and replacing the part
 *                 between the brackets by the value of the the keyword
 *                 "char_string" in the header of the source image file.
 *                 Example: 
 *                   with flood_file=../data/[char_string].ext
 *                   and the keyword char_string=gd10flood
 *                   the resulting filename is ../data/gd10flood.ext
 * flood_id        shared memory segment identifier for the flood field image
 *                 (default: -1, i.e. no identifier specified)
 *
 *                 The two options flood_id and flood_file specify the input
 *                 source for the floodfile image. This can be:
 *                 - flood_id:    a shared memory segment;
 *                 - flood_file:  a floodfile image file.
 *
 *                 Default is no flood field correction (no shared memory
 *                 segment nor flood field file defined).
 *
 *                 Note that only one flood field image input source can
 *                 be specified; if more than one is specified in the input
 *                 arguments, then only the last one will be taken into account.
 *
 * from_ext        filename extension for the input image file.
 *                 This is obsolete, use "src_ext" instead.
 *
 * header_ext      filename extension for the file with the header of the input
 *                 source image. This is mainly used to save the online header
 *                 to a file when the input source image itself is not saved.
 *                 (default: empty string, i.e. no header file written). The
 *                 base of the filename is in "base_name"
 * header_id       shared memory segment identifier where additional header
 *                 information for the output image is available (default: -1,
 *                 i.e. no identifier specified)
 * header_min      set minimum header length for output files (default: 0, i.e.
 *                 take default value from EDF routines)
 *
 * i0              if set, defines value for "Intensity0" header keyword.
 * i1              if set, defines value for "Intensity1" header keyword.
 * inp_const       constant to be added to the value of all source image pixels
 *                 (default: 0.)
 * inp_exp         exponent applied to the value of all source and dark image
 *                 pixels (default: 1.)
 * inp_factor      multiplication factor for the value of all source image
 *                 pixels (default: 1.)
 *
 *                 inp_const, inp_exp and inp_factor are applied to the source
 *                 and dark image as given in the following formula:
 *                    corrected  =  (source ^ inp_exp - dark ^ inp_exp) *
 *                                  inp_factor + inp_const
 *
 * inp_max         maximum allowed value for a pixel. Pixels with bigger values
 *                 are not processed in the analysis (default: 0., i.e. not set)
 * inp_min         minimum allowed value for a pixel. Pixels with smaller values
 *                 are not processed in the analysis (default: 0., i.e. not set)
 * mask_file       name of the file with a mask image of pixels to ignore for
 *                 azimuthal regrouping (default: no file, i.e. NULL pointer)
 *
 * norm_factor     multiplication factor for the value of all scattering
 *                 intensity normalized image pixels (default: 1.). Used only 
 *                 if "norm_int" is set
 * norm_int        if set, normalize image to absolute scattering intensities
 *                 0: no normalization (default)
 *                 1: full normalization to DOmega and Intensity1
 *                 2: normalization to Intensity1
 *                 3: normalization to DOmega
 *
 * overflow        pixel value used to mark an overflow (default: 0 == not set)
 *
 * off_1           if set, defines value for "Offset_1" header keyword.
 * off_2           if set, defines value for "Offset_2" header keyword.
 * ori             if set, defines value for "RasterOrientation" header keyword.
 * outdir          directory path for the output files (default: directory path
 *                 of "base_name" option).
 *
 *                 If "outdir" contains a string (i.e. not empty), this string
 *                 will be prefixed to the names of the output files unless 
 *                 these contain already a directory path.
 *
 *                 The output files concerned are:
 *                 - simulated grid image;
 *                 - file for saving online dark image buffer;
 *                 - file for saving online source image buffer;
 *                 - file for saving online header buffer;
 *                 - files for the x and y distortion correction displacements;
 *                 - corrected output image;
 *                 - file for azimuthal regrouping and averaging.
 *
 * pass            if set, all header values of the input source file are read
 *                 and written to the corrected output file (default: 0 not set)
 *
 * pix_1           if set, defines value for "PSize_1" header keyword.
 * pix_2           if set, defines value for "PSize_2" header keyword.
 *
 * pro             string defining projection type to calculate the scattering
 *                 vectors for the azimuthal regrouping (default: "Saxs"):
 *                 "Saxs"  normal SAXS images
 *                 "Waxs"  images that have been projected to the Ewald sphere
 *
 * psize_distort   if set, certain image parameters for the corrected image are
 *                 taken from the distortion files (default: 0, i.e. not set).
 *
 *                 If psize_distort = 1, only the pixel size for the corrected
 *                 image is taken from the distortion files.
 *
 *                 If psize_distort = 2, the pixel size, center, offset, sample
 *                 distance, binning size, projection and rotation for the
 *                 corrected image are taken from the distortion file.
 *
 *                 If psize_distort is set and any of the input arguments
 *                 pix_1, pix_2, cen_1, cen_2, off_1, off_2, bis_1, bis_2, dis,
 *                 pro, rot_1, rot_2 or rot_3 are set as well, then the value
 *                 given in the input arguments has precedence over the value in
 *                 the distortion files.
 *
 * rot_1           angle (in radian) for the detector rotation in plane 1
 * rot_2           angle (in radian) for the detector rotation in plane 2
 * rot_3           angle (in radian) for the detector rotation in plane 3
 *                 (default: 0. for all 3 angles)
 *
 * save_dark       if set, the SPEC shared memory segment for the dark current
 *                 (= background) image is saved to a file with extension
 *                 ".dark".
 *                 Possible values: 0 = not set,
 *                                  1 = save always,
 *                                  2 = save if new image (default).
 *                 Saving will only happen if the input source image is also
 *                 saved (i.e. "src_ext" is not empty)
 * scat_const      old name of "bckg_const" parameter (do not use)
 * scat_fact       old name of "bckg_fact" parameter (do not use)
 * scat_file       old name of "bckg_file" parameter (do not use)
 * scat_id         old name of "bckg_id" parameter (do not use)
 * simul_id        if set, the routine only produces a simulated grid with
 *                 Gaussian peaks and puts it to the output destination; no
 *                 image processing is done (default: 0, i.e. not set)
 *
 * src_ext         filename extension for the file with the input source image
 *                 (default: ".edf"). This is mainly used to save online input
 *                 data to a file. The base of the filename is in "base_name".
 *                 Note: if "src_ext" is set to an empty string, no output file
 *                 is written.
 * src_id          shared memory segment identifier for the input image data
 *                 (default: -1, i.e. no identifier specified)
 *
 *                 Note that if there is both a shared memory segment and
 *                 filename(s) specified for the input image data, only the
 *                 filename(s) will be used.
 *
 * tit             if set, defines string value for "Title" header keyword.
 * to_ext          filename extension for the corrected image file.
 *                 This is obsolete, use "cor_ext" instead.
 *
 * type            defines globally the data type of all written edf files.
 *                 Possible values are defined in the document "SaxsKeywords",
 *                 e.g. "FloatIEEE32" (default), "Unsigned16".
 *                 The values are converted to the closest possible output
 *                 value.
 *                 The conversion is done in the module "edfio" when an EDF file
 *                 is written.
 *
 * verbose         controls the level of message printing from the program:
 *                 -1: nothing is printed;
 *                  0: print only the message with types ERROR or FATAL;
 *                  1: (default) print all messages except those of type DMSG
 *                     (debug);
 *                  2: print all messages including debugging messages.
 * version          print version string of the program if != 0.
 *
 * wvl             if set, defines value for "WaveLength" header keyword.
 *
 * xfile           name of the input file with the values of the distortion
 *                 corrections for the x coordinate (default: empty string, i.e.
 *                 no file). Needs to be specified simultaneously with "yfile".
 *                 See also remark at "distortion_file" above.
 * xoutfile        name of the output file for the values of the distortion
 *                 corrections for the x coordinate (default: no file, i.e. NULL
 *                 pointer)
 * yfile           name of the input file with the values of the distortion
 *                 corrections for the y coordinate (default: empty string, i.e.
 *                 no file). Needs to be specified simultaneously with "xfile".
 *                 See also remark at "distortion_file" above.
 * youtfile        name of the output file for the values of the distortion
 *                 corrections for the y coordinate (default: no file, i.e. NULL
 *                 pointer)
 *
 * Input : argc:     number of input arguments
 *         argv:     array of strings with the input arguments
 *         progname: string with the name of the executing program
 * Output: none
 * Return: >= 0  if successful
 *          < 0  otherwise
 */
int analyse_args(int argc,char *argv[],char *progname)
{

#define MaxKeyLen1 EdfMaxKeyLen + 1

  static int floid = -1,darkid = -1,bckgid = -1,corid = -1,headid = -1;
  static int azim_id = -1,ave_id = -1;
  int srcid = -1;
  static void *flo_im = NULL,*dark_im = NULL,*bckg_im = NULL,*cor_im = NULL;
  static void *src_im = NULL,*azim_im = NULL,*ave_im = NULL,*mask_im = NULL;
  static void *head_buf,*pnewbuf;

  static char src_ext[256] = {".edf"},cor_ext[256] = {".cor.edf"};
  static char dark_ext[256] = {'\0'},head_ext[256] = {'\0'};
  static char azim_ext[256] = {".azim.edf"},ave_ext[256] = {'\0'};
  static char flobuf[1024],flotmp[1024],darkbuf[1024],darktmp[1024];
  static char bckgbuf[1024],corbuf[1024],maskbuf[1024];
  static char *flofile = NULL,*darkfile = NULL,*bckgfile = NULL,*corfile = NULL;
  static char *maskfile = NULL;
  char *srcfile = NULL,*tmpnam1 = NULL,*tmpnam2 = NULL;
  char tmpbuf1[1024],tmpbuf2[1024];

  static char out_type[256] = {'\0'},outdir[1024] = {'\0'};
  static char basnam[1024] = {"image"};
  static char xfbuf[1024] = {'\0'},yfbuf[1024] = {'\0'},distbuf[1024] = {'\0'};
  static char xoutbuf[1024] = {'\0'},youtbuf[1024] = {'\0'},moutbuf[1024] = {'\0'};
  static char *distfile = NULL;
  char srcbuf[1024];
  char *outfile,*pos;

  static unsigned long overf = 0;
  static int norm_int = 0,do_dist = 1,flat_dist = 1,flat_aft = 1,do_dark = 0;
  static int do_dark_cmd = 0; // is set to 1 by the option do_dark
  static int do_prerot = 0;
  static int norm_prerot = 1;
  static int azimint = 0,azim_rnum = 0,azim_anum = 0,azim_a1fl = 0;
  static int azim_pro = IO_NoPro;
  char azim_pro_buf[1024];
  static int cols = 0,rows = 0,bin_1 = 1,bin_2 = 1;
  static int save_dark = 2,psize_distort = 0;
  static int simul_flag = 0,dummy_set = 0,iverpr = 1,ihistnew = 1;
  int argn,clear,lcont,dstrt_tmp,rowtmp,err,status;
  int gbufstat = 0,iret = 0;

  static float darkconst = 0.,dummy = 0.,arad = 0.,inp_min = 0.,inp_max = 0.;
  static float inpconst = 0.,bckgconst = 0.;
  static float inpfact = 1.,inpexp = 1.,bckgfact = 1.,normfact = 1.;
  static float azim_r0 = 0.,azim_a0 = 0.,azim_da = 1.,ave_scf = 1.;
  float tmpconst;

  static unsigned long head_min = 0;
  static long out_dvo = 0;
  static float i0,i1;
  static float azim_a1;
  static char histargs[] = "InputArg";
  static struct data_head cmd_head;
  struct data_head user_head;

  if(iverpr == 1) {
    prntvers(progname);
    iverpr = 0;
  }

  /*
   * If there are no input arguments, print out a help text and return.
   */
  if(argc == 0) {
    prmsg(MSG,("Usage: %s parameter=option [filenames]\n",progname));
    help_arg();
    goto analyse_args_return;
  }
  /*
   * All the input arguments are written into the history header of the output
   * data to describe the processing done with the input image.
   *
   * Technically, this is done by first writing them in an intermediate
   * "histargs" buffer for all information up to but excluding the source image
   * file names. Then the intermediate history buffer is copied to the output
   * history buffer and the source image file name added to it.
   *
   * The reason for this is that all information up to the source image file
   * name remains identical if several input files are processed. This part of
   * the history can thus be reused.
   *
   * The intermediate buffer is created at the start of the processing for an
   * input image or (offline only) for the sequence of input images specified
   * on the command line. The input arguments are written into it by the routine
   * scan_argument(). When analyse_arguments() returns after processing one
   * image or a series of images, "ihistnew" is reset to 1, and the next call
   * creates then a new history buffer.
   */
  if(ihistnew == 1) {
    edf_history_new(histargs);
    edf_history_argv(histargs,progname);
    ihistnew = 0;
  }

  /*
   * Loop over the input arguments, extract the values for the options specified
   * and set the program parameters accordingly.
   *
   * Options have the form "name=value" and are distinguished from other input
   * arguments by the presence of the equal sign "=".
   *
   * This loop ends with the first input argument that is not an option or when
   * all input arguments have been processed. Any input arguments that might be
   * left in the input line are considered to be file names for the input image
   * files.
   *
   * Note that it is therefore required that all options precede the input file
   * names, as any option following the first file name will also be considered
   * to be a file name.
   *
   * The order of the options is normally not relevant, e.g. the dark image
   * options can be specified before or after the flood field options. However,
   * some options are mutually exclusive (e.g., there are three options to
   * specify the dark image source, but they cannot all be specified at the same
   * time). In these cases, the order is important, and the last option given
   * is the one that is retained.
   */
  for(argn = 0; argn < argc; argn++) {
    /*
     * End loop with the first input argument that is not an option.
     */
    if(strchr(argv[argn],'=') == NULL)
      break;

    prmsg(DMSG,("command line %d. parameter = %s\n",argn,argv[argn]));

    /*
     * This section is for the setting of internal parameters.
     */
    if(scan_argument(argv[argn],"clear","%d",&clear)) {
      /*
       * The option "clear" resets all parameters to the startup values.
       */
      if(clear != 0) {
	floid = darkid = bckgid = corid = headid = azim_id = ave_id = -1;

	flofile = darkfile = bckgfile = corfile = maskfile = distfile = NULL;

	*out_type = *outdir = '\0';
	*distbuf = *xfbuf = *yfbuf = *xoutbuf = *youtbuf = *moutbuf = '\0';
	*dark_ext = *head_ext = *ave_ext = '\0';
        strcpy(azim_ext,".azim.edf");
	strcpy(basnam,"image");
	strcpy(src_ext,".edf");
	strcpy(cor_ext,".cor.edf");

        do_prerot = 0;
        norm_prerot = 1;
        raw_cmpr = UnCompressed;

	overf = 0;
	do_dist = flat_aft = flat_dist = verbose = azim_pass = 1;
        do_dark = do_dark_cmd = 0; // do_dark_cmd is set to 1 by the option do_dark
	headpass = norm_int = azimint = simul_flag = 0;
        azim_pro = IO_NoPro;
	save_dark = 2;
	psize_distort = 0;

	cmd_head.init = 0;
	head_min = out_dvo = 0;

	darkconst = bckgconst = inpconst = arad = inp_min = inp_max = 0.;
	dummy = 0.;
	inpfact = inpexp = bckgfact = normfact = 1.;
	bin_1 = bin_2 = 1;
	azim_r0 = azim_a0 = 0.;
	azim_da = ave_scf = 1.;
	azim_rnum = azim_anum = azim_a1fl = 0;
	dummy_set = 0;

	edf_free_data_file();
      }
    } else if(scan_argument(argv[argn],"version","%d",&iverpr)) {
      /*
       * Print the version number of the program if option "version" != 0.
       */
      if(iverpr != 0) {
	prntvers(progname);
	iverpr = 0;
      }
    } else if(scan_argument(argv[argn],"verbose","%d",&verbose)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"base_name","%s",basnam)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"outdir","%s",outdir)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"overflow","%lu",&overf)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"dummy","%f",&dummy)) {
      dummy_set = 1;
    } else if(scan_argument(argv[argn],"inp_min","%g",&inp_min)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"inp_max","%g",&inp_max)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"raw_cmpr","%s", tmpbuf1)) {
      raw_cmpr = edf_string2compression ( tmpbuf1 );
      if ( (raw_cmpr<UnCompressed)||(raw_cmpr>=EndDCompression) )
        raw_cmpr=UnCompressed;
    } else if(scan_argument(argv[argn],"do_prerotation","%d",&do_prerot)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"norm_prerotation","%d",&norm_prerot)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"do_distortion","%d",&do_dist)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"flat_distortion","%d",&flat_dist)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"flat_after","%d",&flat_aft)) {
      /* nothing */

    /*
     * Following section sets the data format of the output files
     */
    } else if(scan_argument(argv[argn],"type","%s",&out_type)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"dvo","%ld",&out_dvo)) {
      /* nothing */

    /*
     * Following section sets the binning factor if the images are to be binned.
     */
    } else if(scan_argument(argv[argn],"bin_1","%d",&bin_1)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"bin_2","%d",&bin_2)) {
      /* nothing */

    /*
     * Following section is for parameters for the scattering intensity
     * normalization.
     */
    } else if(scan_argument(argv[argn],"norm_int","%d",&norm_int)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"norm_factor","%g",&normfact)) {
      /* nothing */

    /*
     * Following section is for parameters for the azimuthal regrouping.
     */
    } else if(scan_argument(argv[argn],"azim_int","%d",&azimint)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_pro","%s",&azim_pro_buf)) {
      if(strlib_ncasecmp("Saxs",azim_pro_buf,4) == 0) azim_pro=IO_ProSaxs;
      else if(strlib_ncasecmp("Waxs",azim_pro_buf,4) == 0) azim_pro=IO_ProWaxs;
    } else if(scan_argument(argv[argn],"azim_pass","%d",&azim_pass)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_id","%d",&azim_id)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_ext","%s",azim_ext)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_r0","%f",&azim_r0)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_r_num","%d",&azim_rnum)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_a0","%f",&azim_a0)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_a1","%f",&azim_a1)) {
      azim_a1fl = 1;
    } else if(scan_argument(argv[argn],"azim_da","%f",&azim_da)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"azim_a_num","%d",&azim_anum)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"ave_id","%d",&ave_id)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"ave_ext","%s",ave_ext)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"ave_scf","%f",&ave_scf)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"mask_file","%s",maskbuf)) {
      maskfile = maskbuf;

    /*
     * Following section is for parameters for Andy Hammersley's correction.
     */

    /*
     * Calculate the distortion correction values for x and y from spline
     * function coefficients. The coefficients are read from the "distortion
     * file".
     *
     * Note that reading the distortion correction values directly from files
     * (parameters "xfile" and "yfile", see below) takes precedence over
     * calculating them from the spline function coefficients. Thus, if
     * "distortion_file" is specified at the same time as "xfile" and "yfile",
     * then "distortion_file" is ignored.
     */
    } else if(scan_argument(argv[argn],"distortion_file","%s",distbuf)) {
      distfile = distbuf;

    /*
     * Get the distortion correction values for x and y directly instead of
     * calculating them from the spline function coefficients.
     *
     * The two files defined here contain for each pixel the correction in
     * the x direction ("xfile") and y direction ("yfile") as floating point
     * numbers. The files are in EDF format and have each the same number of
     * data values as there are pixels in the image.
     *
     * It obviously does not make much sense to specify only one of them, as
     * the corrections in both directions are needed. If only one is given, the
     * program acts as if none had been specified.
     *
     * See the remark above for "distortion_file" concerning the precedence
     * of the options "xfile", "yfile" and "distortion_file".
     */
    } else if(scan_argument(argv[argn],"xfile","%s",xfbuf)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"yfile","%s",yfbuf)) {
      /* nothing */

    /*
     * Save the distortion correction values for x and y when they have been
     * calculated from the spline function coefficients.
     *
     * The two files contained here will receive the corrections in the x
     * direction ("xoutfile") and y direction ("youtfile"). For more details,
     * see the description of "xfile" and "yfile" above.
     */
    } else if(scan_argument(argv[argn],"xoutfile","%s",xoutbuf)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"youtfile","%s",youtbuf)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"moutfile","%s",moutbuf)) {
      /* nothing */

    /*
     * Defines a circular "active area" in the image.
     */
    } else if(scan_argument(argv[argn],"active_radius","%f",&arad)) {
      /* nothing */

    /*
     * This parameter causes certain parameters for the corrected image to be
     * taken from the distortion correction files.
     *
     * If it is 1, the pixel size is taken.
     *
     * If it is 2, the pixel size, center coordinates, sample distance,
     * projection and rotation for the corrected image are taken from the
     * distortion correction files.
     */
    } else if(scan_argument(argv[argn],"psize_distort","%d",&psize_distort)) {
      /* nothing */

    /*
     * These parameters, if set, define the following keywords in the data
     * header:
     * - cen_1    defines Center_1
     * - cen_2    defines Center_2
     * - i0       defines Intensity0
     * - i1       defines Intensity1
     * - off_1    defines Offset_1
     * - off_2    defines Offset_2
     * - pix_1    defines PSize_1
     * - pix_2    defines PSize_2
     * - bis_1    defines BSize_1
     * - bis_2    defines BSize_2
     * - ori      defines RasterOrientation
     * - dis      defines SampleDistance
     * - tit      defines Title
     * - wvl      defines WaveLength
     * - pro      defines ProjectionType
     * - rot_1    defines DetectorRotation_1
     * - rot_2    defines DetectorRotation_2
     * - rot_3    defines DetectorRotation_3
     */
    } else if(scan_argument(argv[argn],"cen_1","%f",&cmd_head.Center_1)) {
      cmd_head.init |= FL_CENT1;
    } else if(scan_argument(argv[argn],"cen_2","%f",&cmd_head.Center_2)) {
      cmd_head.init |= FL_CENT2;
    } else if(scan_argument(argv[argn],"i0","%f",&i0)) {
      sprintf(cmd_head.Intens_0,"%g",i0);
      cmd_head.init |= FL_INTE0;
    } else if(scan_argument(argv[argn],"i1","%f",&i1)) {
      sprintf(cmd_head.Intens_1,"%g",i1);
      cmd_head.init |= FL_INTE1;
    } else if(scan_argument(argv[argn],"off_1","%f",&cmd_head.Offset_1)) {
      cmd_head.init |= FL_OFFS1;
    } else if(scan_argument(argv[argn],"off_2","%f",&cmd_head.Offset_2)) {
      cmd_head.init |= FL_OFFS2;
    } else if(scan_argument(argv[argn],"pix_1","%f",&cmd_head.PSize_1)) {
      cmd_head.init |= FL_PSIZ1;
    } else if(scan_argument(argv[argn],"pix_2","%f",&cmd_head.PSize_2)) {
      cmd_head.init |= FL_PSIZ2;
    } else if(scan_argument(argv[argn],"bis_1","%f",&cmd_head.BSize_1)) {
      cmd_head.init |= FL_BSIZ1;
    } else if(scan_argument(argv[argn],"bis_2","%f",&cmd_head.BSize_2)) {
      cmd_head.init |= FL_BSIZ2;
    } else if(scan_argument(argv[argn],"dis","%f",&cmd_head.SamplDis)) {
      cmd_head.init |= FL_SAMDS;
    } else if(scan_argument(argv[argn],"ori","%d",&cmd_head.Orientat)) {
      cmd_head.init |= FL_ORIEN;
    } else if(scan_argument(argv[argn],"tit","%s",&cmd_head.Title)) {
      cmd_head.init |= FL_TITLE;
    } else if(scan_argument(argv[argn],"wvl","%f",&cmd_head.WaveLeng)) {
      cmd_head.init |= FL_WAVLN;
    } else if(scan_argument(argv[argn],"pro","%s",&cmd_head.ProjTyp)) {
      cmd_head.init |= FL_PRO;
    } else if(scan_argument(argv[argn],"rot_1","%f",&cmd_head.DetRot_1)) {
      cmd_head.init |= FL_ROT1;
    } else if(scan_argument(argv[argn],"rot_2","%f",&cmd_head.DetRot_2)) {
      cmd_head.init |= FL_ROT2;
    } else if(scan_argument(argv[argn],"rot_3","%f",&cmd_head.DetRot_3)) {
      cmd_head.init |= FL_ROT3;

    /*
     * If do_prerotation is set, the following parameters are used to include 
     * a rotation correction after the distortion correction. The prerotations 
     * align the detector perpendicular to the incident beam where all
     * detector rotations are 0, i.e. for rot_1=rot_2=rot_3=0. 
     * - precen_1    defines pre-rotation Center_1 (default calculated from cen_1)
     * - precen_2    defines pre-rotation Center_2 (default calculated from cen_2)
     * - predis      defines pre-rotation SampleDistance (default calculated from dis)
     * - prerot_1    defines pre-rotation DetectorRotation_1 (default 0)
     * - prerot_2    defines pre-rotation DetectorRotation_2 (default 0)
     * - prerot_3    defines pre-rotation DetectorRotation_3 (default 0)
     * This is also known as fit2d tilt-correction. Use sxparams to translate
     * between prerotation and tilts.
     */

    } else if(scan_argument(argv[argn],"precen_1","%f",&cmd_head.PreCenter_1)) {
      cmd_head.init |= FL_PRECEN1;
    } else if(scan_argument(argv[argn],"precen_2","%f",&cmd_head.PreCenter_2)) {
      cmd_head.init |= FL_PRECEN2;
    } else if(scan_argument(argv[argn],"predis","%f",&cmd_head.PreSamplDis)) {
      cmd_head.init |= FL_PREDIS;
    } else if(scan_argument(argv[argn],"prerot_1","%f",&cmd_head.PreDetRot_1)) {
      cmd_head.init |= FL_PREROT1;
    } else if(scan_argument(argv[argn],"prerot_2","%f",&cmd_head.PreDetRot_2)) {
      cmd_head.init |= FL_PREROT2;
    } else if(scan_argument(argv[argn],"prerot_3","%f",&cmd_head.PreDetRot_3)) {
      cmd_head.init |= FL_PREROT3;
 
    /*
     * This parameter causes a simulated grid to be produced and saved to the
     * corrected output file. No image correction processing will happen.
     */
    } else if(scan_argument(argv[argn],"simul_id","%d",&simul_flag)) {
      /* nothing */

    /*
     * The different dark image input options - end up with a buffer pointer.
     *
     * The dark image input can be:
     * - a shared memory segment;
     * - a dark image file;
     * - an overall constant to be applied to all pixels of the image data.
     *
     * Default is no shared memory segment nor dark image file defined and the
     * overall dark image constant 0. This implies that by default no dark image
     * correction is performed.
     *
     * However, even if a source for the dark image correction is specified, the
     * correction can still be suppressed with the option "do_dark=0". This
     * allows the specification and saving of a dark image buffer for later
     * processing. By default, "do_dark" is 1.
     *
     * Note that in principle only either a shared memory segment or an image
     * file can be specified; if both are specified in the input arguments, then
     * the one specified last will be used.
     *
     * The overall constant can be specified in addition to either of the two
     * other options.
     *
     * The (possibly dark image-corrected) values of the input image can then
     * be further adjusted with a multiplicative and an additive constant.
     */
    } else if(scan_argument(argv[argn],"dark_id","%d",&darkid) ||
      scan_argument(argv[argn],"bkg_id","%d",&darkid)) {
      darkfile = NULL;
      if ((darkid!=-1)&&(!do_dark_cmd)) do_dark=1;
    } else if(scan_argument(argv[argn],"dark_file","%s",darkbuf) ||
      scan_argument(argv[argn],"bkg_file","%s",darkbuf)) {
      darkid = -1;
      darkfile = darkbuf;
      if (!do_dark_cmd) do_dark=1;
    } else if(scan_argument(argv[argn],"dark_const","%g",&darkconst) ||
      scan_argument(argv[argn],"bkg_const","%f",&darkconst)) {
      if (!do_dark_cmd) do_dark=1;
      /* nothing */
    } else if(scan_argument(argv[argn],"do_dark","%d",&do_dark)) {
      do_dark_cmd = 1;
      /* nothing */
    } else if(scan_argument(argv[argn],"save_dark","%d",&save_dark)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"dark_ext","%s",dark_ext)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"inp_const","%g",&inpconst)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"inp_exp","%g",&inpexp)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"inp_factor","%g",&inpfact)) {
      /* nothing */

    /*
     * The different flood field input options - end up with a buffer pointer.
     *
     * The flood field input can be:
     * - a shared memory segment;
     * - a flood field image file.
     *
     * Default is no flood field correction, i.e. no shared memory segment nor
     * flood field file defined.
     *
     * Note that in principle only one flood field input source can be
     * specified; if more than one is specified in the input arguments, then
     * only the last one will be taken into account.
     */
    } else if(scan_argument(argv[argn],"flood_id","%d",&floid)) {
      flofile = NULL;
    } else if(scan_argument(argv[argn],"flood_file","%s",flobuf)) {
      floid = -1;
      flofile = flobuf;

    /*
     * The different scattering background input options - end up with a buffer
     * pointer.
     *
     * The scattering background input can be:
     * - a shared memory segment;
     * - a scattering background image file.
     *
     * Default is no scattering background correction, i.e. no shared memory
     * segment nor scattering background file defined.
     *
     * Note that in principle only one scattering background input source can be
     * specified; if more than one is specified in the input arguments, then
     * only the last one will be taken into account.
     *
     * The scattering background image, if it exists, can have a multiplicative
     * and an additive constant applied to the value of each pixel before it is
     * subtracted from the image to be corrected. These constants are obtained
     * here.
     */
    } else if(scan_argument(argv[argn],"bckg_id","%d",&bckgid) ||
      scan_argument(argv[argn],"scat_id","%d",&bckgid)) {
      bckgfile = NULL;
    } else if(scan_argument(argv[argn],"bckg_file","%s",bckgbuf) ||
      scan_argument(argv[argn],"scat_file","%s",bckgbuf)) {
      bckgid = -1;
      bckgfile = bckgbuf;
    } else if(scan_argument(argv[argn],"bckg_const","%f",&bckgconst) ||
      scan_argument(argv[argn],"scat_const","%g",&bckgconst)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"bckg_factor","%g",&bckgfact) ||
      scan_argument(argv[argn],"scat_factor","%g",&bckgfact)) {
      /* nothing */

    /*
     * The shared memory where user-defined header values are obtained from.
     */
    } else if(scan_argument(argv[argn],"header_id","%d",&headid)) {
      /* nothing */

    /*
     * Determine if the entire input source file header is passed to the output.
     */
    } else if(scan_argument(argv[argn],"pass","%d",&headpass)) {
      /* nothing */

    /*
     * Set the minimum header length for output files (if not set, take default
     * from EDF I/O routines).
     */
    } else if(scan_argument(argv[argn],"header_min","%lu",&head_min)) {
      if(head_min > 0)
	edf_set_minimumheadersize(head_min);

    /*
     * The filename extension of the output file where header of the input
     * source image is saved to (saving is done if this extension is not empty).
     */
    } else if(scan_argument(argv[argn],"header_ext","%s",head_ext)) {
      /* nothing */

    /*
     * The different options where to put the results.
     *
     * The options "cor_file" and "to_ext" are obsolete. They should be replaced
     * by "base_name" and "cor_ext".
     *
     * Note that a value of "-" (minus) for the "cor_file" parameter causes any
     * previously given output file name to be erased (the pointer to the file
     * name is set to NULL).
     */
    } else if(scan_argument(argv[argn],"cor_id","%d",&corid)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"cor_ext","%s",cor_ext)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"to_ext","%s",cor_ext)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"cor_file","%s",corbuf)) {
      corfile = NULL;
      if(strcmp(corbuf,"-") && *corbuf != '\0') {
	corfile = corbuf;
      }

    /*
     * The input image specification - end up with a buffer pointer.
     *
     * The source for the input image can be:
     *
     * - a shared memory segment;
     * - one (or several) files with input image data.
     *
     * The shared memory segment is used if an identifier for it is set with
     * the option "src_id".
     *
     * Data files with input image data have to be given on the input argument
     * line after all option arguments (see below).
     *
     * It is not possible to have a shared memory segment and files with input
     * image data at the same time. If both are specified, the shared memory
     * segment will be ignored. However, it is possible to specify several input
     * data files. They will all be processed, in the sequence given by the
     * input arguments.
     *
     * The option "from_ext" is obsolete. It should be replaced by "src_ext".
     */
    } else if(scan_argument(argv[argn],"src_id","%d",&srcid)) {
      srcfile = NULL;
    } else if(scan_argument(argv[argn],"src_ext","%s",src_ext)) {
      /* nothing */
    } else if(scan_argument(argv[argn],"from_ext","%s",src_ext)) {
      /* nothing */

    } else {
      errno = 0;
      prmsg(ERROR,("Unknown option: %s\n",argv[argn]));
    }
  }
  /*
   * End of the loop over the option arguments.
   */

  /*
   * Default value for "outdir": if not set, set it to the directory path of
   * "basnam" (if there is one).
   */
   if(*outdir == '\0' && (pos = strrchr((const char *)basnam,(int)'/')) != NULL)
     strncat(outdir,basnam,pos - basnam + 1);

  /*
   * If the simulation flag is set, do the grid simulation, write it to the
   * output file and return without any further processing.
   */
  if(simul_flag) {
    if(corid == -1) {
      cols = get_xsize();
      rows = get_ysize();
    } else {
      cols = 0;
      rows = 0;
    }
    if(get_buffer(corid,NULL,&cor_im,&rows,&cols,CORTYP) != -1) {
      prmsg(MSG,("Starting to simulate grid image [%d,%d]\n",cols,rows));
      make_grid(cor_im,1000.0,5.0,0);
      if(*cor_ext != '\0')
	outfile = outname(basnam,outdir,src_ext,cor_ext);
      else
	outfile = corfile;
      put_buffer(outfile,&cor_im,rows,cols,CORTYP);
    }
    clean_buffer(&cor_im,CORTYP,1);

    ihistnew = 1;
    goto analyse_args_return;
  }

  /*
   * Test if there are any more input arguments. If so, take the first and
   * interpret it as a file name for an input image data file.
   */
  if(argn < argc) {
    if(fnampat(srcbuf,sizeof(srcbuf),argc - argn,argv + argn) < 0)
      goto analyse_args_cleanup;
    srcfile = srcbuf;
    srcid = -1;
  }

  /*
   * If there is no input source specified, return without further action.
   *
   * This serves as a protection against incorrect use of the program: it
   * does not make sense to correct a non-existent input image.
   *
   * However, this is also used as a feature by the online-version of the
   * program. In this case, the command line parameters for the correction of
   * one image may be handed over in several subsequent calls to analyse_args().
   * Each call sets some parameters, and only the last one contains the input
   * source image and therefore starts the correction.
   */
  if(srcfile == NULL && srcid == -1)
    goto analyse_args_return;

  /*
   * Set the "input data busy" flag indicating that the program is getting the
   * input data now. This flag can be tested with the "getstate()" routine.
   */
  inptbusy = 1;

  /*
   * Default value for "dark_file": if not set, set it to "[DarkFileName]" if:
   * - a dark image correction is requested;
   * - there is a source image file (otherwise there can be no substitution).
   */
  if(darkfile == NULL && do_dark == 1 && srcfile != NULL)
    darkfile = strcpy(darkbuf,"[DarkFileName]");

  /*
   * Set globally the output data type and data value offset.
   */
  set_type(out_type);
  set_dvo(out_dvo);

  /*
   * Hand the parameters to the correction routines.
   */
  set_headval(cmd_head,CMDTYP);
  set_overflow(overf);
  set_inpmin(inp_min * bin_1 * bin_2);
  set_inpmax(inp_max * bin_1 * bin_2);
  set_dummy(dummy);
  set_dodark(do_dark);
  set_doflat(flat_dist);
  set_normint(norm_int,normfact);
  set_actrad(arad);

  dstrt_tmp = 0;
  if(psize_distort == 1)
    dstrt_tmp = FL_PSIZ1 + FL_PSIZ2;
  else if(psize_distort == 2)
    dstrt_tmp = FL_OFFS1 + FL_OFFS2 + FL_PSIZ1 + FL_PSIZ2 + FL_CENT1 + FL_CENT2
      + FL_SAMDS + FL_BSIZ1 + FL_BSIZ2 + FL_PRO + FL_ROT1 + FL_ROT2 + FL_ROT3;
  set_dstrtval(dstrt_tmp);

  /*
   * Apply linearity correction for dark constant, if necessary.
   *
   * Note that for the dark constant, the additional factor resulting from the
   * binning of the image must be the one of the source image, not of the dark
   * image (which might not even exist).
   */
  if(inpexp != 1. && bin_1 * bin_2 != 1)
    tmpconst = pow((double)darkconst / (double)(bin_1 * bin_2),(double)inpexp) *
      bin_1 * bin_2;
  else
    tmpconst = darkconst;
  set_drkconst(tmpconst);

  set_inpconst(inpconst);
  set_inpexp(inpexp);
  set_inpfact(inpfact);
  set_bckgconst(bckgconst);
  set_bckgfact(bckgfact);

  /*
   * If the image comes from shared memory, clear the flags of the SRCTYP header
   * structure (and therefore also the flags of the CORTYP header structure).
   * This is useful if there is an input image but no header for it - in this
   * case the previous header structure would be kept, which might not be what
   * was desired.
   *
   * If the image comes from a file, this situation cannot happen, as a file
   * always provides a header.
   */
  if(srcid != -1)
    user_head.init = 0;
  /*
   * Get a buffer for the user-defined online header.
   *
   * If the source image comes from shared memory, then the online header
   * contains the corresponding header values. 
   * Read them from the online header, update them with the command header,
   * and write them to the correction routines.
   *
   * Otherwise, the online header is intended as output for the header values
   * of the corrected image. Ignore its values at this point.
   */
  if(headid != -1) {
    cols = rows = 0;
    if((gbufstat = get_buffer(headid,NULL,&head_buf,&rows,&cols,HD_TYP)) == -1)
      goto analyse_args_cleanup;
    if(srcid != -1) {
      scanhead(HD_TYP,&user_head);
      set_headval(user_head,HD_TYP);
      /* 
       * upd_headvalcmd updates the online header with the command header
       */ 
      upd_headvalcmd( &user_head );
      set_headval(user_head,SRCTYP);
      if(user_head.init & FL_DUMMY)
	set_dummy(user_head.Dummy);
    }

  /*
   * If there is no online header for this image, free its buffers.
   */
  } else
    clean_buffer(&head_buf,HD_TYP,1);

  /*
   * Get a buffer for the input source image. This image must be acquired as the
   * first one, as the dimensions of all other images are checked against the
   * size of this image.
   */
  cols = rows = 0;
  if((gbufstat = get_buffer(srcid,srcfile,&src_im,&rows,&cols,SRCTYP)) == -1)
    goto analyse_args_cleanup;

  /*
   * Bin and linearity-correct the input image if requested. Afterwards, free
   * the old source buffer if it has changed and if it is not a shared memory.
   */
  if(gbufstat > 0)
    if(map_imag(src_im,&pnewbuf,(double)bin_1,(double)bin_2,SRCTYP) < 0)
      goto analyse_args_cleanup;
    if(src_im != pnewbuf) {
      if(srcid == -1 || current_shm_data[SRCTYP] != src_im)
	pfree(src_im);
      src_im = pnewbuf;
      get_headval(&user_head,SRCTYP);
      user_head.Dim_1 = cols /= bin_1;
      user_head.Dim_2 = rows /= bin_2;
      set_headval(user_head,SRCTYP);
    }
  set_xysize(cols,rows);

  /*
   * Get the filename for the dark image, and then get the dark image buffer.
   * Return an error if this fails.
   *
   * Also return an error if the dark file name is specified as keyword of the
   * source header but there is no source image file, as for the dark image the
   * keyword construct must not be used with source input from shared memory.
   */
  if(get_filnam(&darkfile,darkbuf,darktmp,DRKTYP) < 0) {
    iret |= SPD_ERRFLG;
    goto analyse_args_cleanup;
  }
  if(darkfile != NULL && strchr(darkbuf,'[') != NULL && srcfile == NULL) {
    prmsg(ERROR,("no dark_file keyword search allowed for shared memory\n"));
    iret |= SPD_ERRFLG;
    goto analyse_args_cleanup;
  }

  /*
   * If the dark file name does not contain a directory path, use the one from
   * the source image file name.
   */
  if(darkfile != NULL && srcfile != NULL && strchr(darkfile,'/') == NULL)
  {
    if((tmpnam1 = strrchr(srcfile,'/')) != NULL)
    {
      *tmpbuf1 = '\0';
      strncat(tmpbuf1,srcfile,tmpnam1 - srcfile + 1);
      strcat(tmpbuf1,darkfile);
      strcpy(darkfile,tmpbuf1);
    }
  }
  /*
   * Get the buffer for the dark image, and hand it to the correction routines.
   * The buffer is only acquired if it is also needed afterwards (for correction
   * or dark bffer saving).
   */
  if(do_dark != 0 || save_dark != 0) {
    if((gbufstat = get_buffer(darkid,darkfile,&dark_im,&rows,&cols,DRKTYP)) < 0)
      goto analyse_args_cleanup;
    /*
     * The dark image will only be saved if all of the following conditions are
     * fulfilled:
     * - the image comes from an online buffer;
     * - there is a file extension defined for it;
     * - the raw image is also to be saved;
     * - the "save_dark" command line argument is either 1 (save always) or it
     *   is 2 (save if new) and the dark image is new.
     */
    if(darkid != -1) {
      if(*dark_ext != '\0' && *src_ext != '\0' && (save_dark == 1 ||
	save_dark == 2 && gbufstat == 1)) {
	if((outfile = outname(basnam,outdir,NULL,dark_ext)) == NULL)
	  goto analyse_args_cleanup;
	if(put_buffer(outfile,NULL,rows,cols,DRKTYP) == 0)
	  iret |= SPD_DRKFIL | SPD_BASUSE;
	else
	  iret |= SPD_ERRFLG;
	strcpy(darkbuf,basename(outfile));
      }
      /*
       * Add the filename of the saved dark image buffer to the online header.
       * If this dark image buffer is not to be saved, the filename is the name
       * of the last saved dark image buffer.
       */
      edf_add_header_element(*(typestr + HD_TYP),"DarkFileName",darkbuf,&err,
	&status);
    }
    if(gbufstat == 1)
      set_imgbuf(dark_im,DRKTYP);
  }

  /*
   * If the command line option "src_ext" is set, save the input source image to
   * a file if it comes from a shared memory (if it comes from a file, there is
   * obviously no need to save it to a file again).
   *
   * Alternatively, if the command line option "head_ext" is set, save the
   * online header (only the header!) to a file. Again, this is not necessary if
   * the source image comes from a file, as a file always has its own header.
   */
  if(srcid != -1) {
    if(*src_ext != '\0') {
      if((outfile = outname(basnam,outdir,NULL,src_ext)) == NULL)
	goto analyse_args_cleanup;
      if(put_buffer(outfile,NULL,rows,cols,SRCTYP) == 0)
	iret |= SPD_SRCFIL | SPD_BASUSE;
      else
	iret |= SPD_ERRFLG;
    }
    else if(*head_ext != '\0') {
      if((outfile = outname(basnam,outdir,NULL,head_ext)) == NULL)
	goto analyse_args_cleanup;
      if(put_buffer(outfile,NULL,rows,cols,HD_TYP) != 0)
	iret |= SPD_ERRFLG;
    }
  }

  /*
   * Get the file name(s) for the distortion correction and hand them to the
   * correction routines. Return an error if this fails.
   * If successful, set the value of the distortion correction parameter.
   */
  if(do_dist) {
    if(*xfbuf != '\0' && *yfbuf != '\0')
      set_xycorin(xfbuf,yfbuf);
    else if(distfile != NULL && *distfile != '\0') {
      /*
       * Get the filename for the spline distortion coefficients.
       */
      if(get_filnam(&distfile,distbuf,tmpbuf1,DISTYP) < 0) {
	iret |= SPD_ERRFLG;
	goto analyse_args_cleanup;
      }
      set_splinfil(distfile);
    } else {
      if (!do_prerot)
        do_dist = 0;
    }
    if(*xoutbuf != '\0' && *youtbuf != '\0') {
      /*
       * Get the output filenames for the x and y spatial distortion
       * displacement values.
       */
      tmpnam1 = strcpy(tmpbuf1,outname(xoutbuf,outdir,NULL,NULL));
      tmpnam2 = strcpy(tmpbuf2,outname(youtbuf,outdir,NULL,NULL));
      set_xycorout(tmpnam1,tmpnam2);
    }
    if(*moutbuf != '\0') {
      /*
       * Get the output filenames for the x and y spatial distortion
       * displacement values.
       */
      tmpnam1 = strcpy(tmpbuf1,outname(moutbuf,outdir,NULL,NULL));
      set_moutfile(tmpnam1);
    }
  }
  if(do_dist == 1 && flat_aft == 0)
    do_dist = 2;
  set_dospd(do_dist);

  /*
   * Set the value of the prerotation parameter.
   */
  set_doprerot(do_prerot);

  /*
   * Set the value of the prerotation normalization parameter.
   */
  set_normprerot(norm_prerot);

  /*
   * Get the file names for the flood field and the scattering background image,
   * and then get the buffers and hand them to the correction routines.
   * Return an error if this fails.
   */
  if(get_filnam(&flofile,flobuf,flotmp,FLOTYP) < 0) {
    iret |= SPD_ERRFLG;
    goto analyse_args_cleanup;
  }
  if((gbufstat = get_buffer(floid,flofile,&flo_im,&rows,&cols,FLOTYP)) == -1)
    goto analyse_args_cleanup;
  if(gbufstat == 1)
    set_imgbuf(flo_im,FLOTYP);

  if((gbufstat = get_buffer(bckgid,bckgfile,&bckg_im,&rows,&cols,SBKTYP)) == -1)
    goto analyse_args_cleanup;
  if(gbufstat == 1)
    set_imgbuf(bckg_im,SBKTYP);

  /*
   * All input data have been acquired now. Reset the "input data busy" flag.
   * This flag can be tested with the "getstate()" routine.
   */
  inptbusy = 0;

  /*
   * Get a buffer for the corrected output image.
   */
  if((gbufstat = get_buffer(corid,NULL,&cor_im,&rows,&cols,CORTYP)) == -1)
    goto analyse_args_cleanup;

  /*
   * Get the buffers for the azimuthal regrouping and averaging:
   *
   * - one buffer for the azimuthal regrouped output image;
   * - one buffer for the azimuthal averaged output image;
   * - one buffer for the mask defining the pixels to ignore for the azimuthal
   *   calculations.
   *
   * The averaged output needs 4 rows in the buffer to provide the space for the
   * four azimuthal averaged values per radial value:
   *
   * - the "s" value;
   * - the average over all angular values;
   * - 2 values for the errors.
   *
   * If there are two start angles defined for the azimuthal averaging, then the
   * buffer needs 8 rows, 4 for each of the two angles. If, however, the shared
   * memory only provides space for 4 rows, the averaging is automatically set
   * to be performed only for one start angle.
   */
  if(azimint == 1) {
    if((gbufstat = get_buffer(azim_id,NULL,&azim_im,&azim_anum,&azim_rnum,
      AZITYP)) == -1)
      goto analyse_args_cleanup;

    if(azim_a1fl == 0)
      rowtmp = 4;
    else
      rowtmp = 8;
    if((gbufstat = get_buffer(ave_id,NULL,&ave_im,&rowtmp,&azim_rnum,AVETYP))
      == -1)
      goto analyse_args_cleanup;
    if(rowtmp == 4)
      azim_a1fl = 0;

    if(maskfile != NULL)
      if((gbufstat = get_buffer(-1,maskfile,&mask_im,&rows,&cols,MSKTYP)) == -1)
	goto analyse_args_cleanup;
  }

  /*
   * Get the buffer for the input image. Two cases are possible:
   * - input from file(s). There may be more than one input file specified;
   * - input from shared memory.
   *
   * Then do the corrections and (if requested) the azimuthal regrouping.
   */
  do {
    /*
     * Set the "Dummy" value.
     *
     * "Dummy" is the value used to mark invalid pixels in the output
     * image. It is defined by (in increasing order of precedence)
     * - the "Dummy" value in either:
     *   -- the input source image (for input from files)
     *   -- the online header (for input from shared memory);
     * - the value of the "dummy" parameter on the command line.
     *
     * If none of these is set, the default value is Dummy = 0.
     *
     * Note:
     * - for files, the input source image is read in read_esrf_file().
     *   This also gets and sets the header values and "Dummy";
     * - for shared memory, the "Dummy" value is obtained and set when the
     *   online header is read.
     *
     * Thus only the "dummy" from the command line needs to be dealt with
     * here.
     */
    if(dummy_set != 0)
      set_dummy(dummy);

    /*
     * If any of the command line options pix_1, pix_2, cen_1, cen_2, off_1,
     * off_2, bis_1, bis_2, dis, pro, rot_1, rot_2 or rot_3 were given, these
     * values take precedence over the values set in the distortion file, even
     * if psize_distort is set. Modify the value set for the correction
     * routine accordingly with the set_dstrtval() call.
     */
    if(cmd_head.init != 0 && psize_distort) {
      dstrt_tmp &= ~cmd_head.init;
      set_dstrtval(dstrt_tmp);
    }

    if(srcfile) {
      /*
       * Case 1: input from file(s). This is typically the case when the program
       * is run offline.
       *
       * There can be more than one file name specified in the input arguments.
       * The program loops over all input files, performs the corrections, and
       * writes the data to the output files.
       *
       * There is only one set of correction parameters for all input files,
       * thus in principle the same corrections are applied to all source files.
       * However, there are some exceptions to that:
       * - dark image subtraction;
       * - distortion correction;
       * - flood field correction.
       *
       * For these, the corresponding input argument can specify that the name
       * of the file containing the correction is to be obtained from a keyword
       * in the source image header. Thus these corrections could be different
       * for each input file. For more details, see description of the input
       * arguments "dark_file, "distortion_file" and "flood_file".
       *
       * The header values for the corrected image are pre-set by the file
       * header of the input source image. Any online header is ignored.
       *
       * The names of the output files are derived from the names of the
       * corresponding input files by replacing the string contained in
       * "src_ext" with the string in "cor_ext". These two strings can be
       * defined by the user. For details about the generation of the name, see
       * routine outname().
       *
       * The output file name that might have been defined by the user with the
       * (obsolete) option "cor_file" is not used.
       *
       * Note that it is in principle possible to have the program write the
       * output data also to shared memory. However, if there is more than one
       * input file to be processed, the second output image will overwrite the
       * first, etc., as there is only one shared memory segment for output.
       *
       * If the correction routines return an error for at least one of the
       * input files, analyse_args() will return an error. However, processing
       * of files does not stop when the error is encountered, thus subsequent
       * input files might be corrected successfully.
       */
      prmsg(DMSG,("command line input file = %s\n",srcfile));
      tmpnam1 = strrchr((const char *)srcfile,(int)'/');
      if(tmpnam1 == NULL)
	tmpnam1 = srcfile;
      else
	tmpnam1++;

      /*
       * Do not try to do corrections if there is no input.
       */
      if(src_im == NULL)
	continue;

      /*
       * Generate output file name, respecting the following conditions:
       *
       * - do not store corrected image if "cor_ext" is an empty string;
       * - signal error if no output file name could be created;
       * - ignore cases where the input file name would be identical to the
       *   output file name, i.e. where the input file would be overwritten.
       */
      if(*cor_ext == '\0')
	outfile = NULL;
      else if((outfile = outname(tmpnam1,outdir,src_ext,cor_ext)) == NULL)
	iret |= SPD_ERRFLG;
      else if(strcmp(outfile,tmpnam1) == 0)
	continue;
      prmsg(MSG,("Correcting: %s\n        --> %s\n",srcfile,outfile));

    } else if(srcid != -1) {
      /*
       * Case 2: input from shared memory. This is typically the case when the
       * program is run online. Only one input image is then possible for each
       * call to "analyse_args()". The output can be to a file, a shared memory,
       * or both.
       * The name of the output file is derived from "base_name" with the
       * extension "cor_ext". Both can be set by the user.
       *
       * The name of the output file can also be set with the obsolete option
       * "cor_file".
       *
       * Note that the flags of user_head have been cleared and set earlier. If
       * there is no online header, the input and output header values will just
       * have the default values. No "old" header values from previous images
       * will be kept.
       */

      prmsg(DMSG,("Correcting online image [%d,%d]\n",rows,cols));
      outfile = corfile;
      tmpnam1 = basnam;
      if(*cor_ext != '\0' && (outfile = outname(tmpnam1,outdir,src_ext,cor_ext))
	== NULL)
	iret |= SPD_ERRFLG;

    } else
      /*
       * Neither input files nor input from shared memory. Ignore, probably
       * user error.
       */
      break;

    prmsg(DMSG,("Image buffers: cor = %p, src = %p, drk = %p, flo = %p\n",
      cor_im,src_im,dark_im,flo_im));

    /*
     * Do the correction.
     *
     * If successful:
     *
     * - copy the history information assembled in the intermediate "histsave"
     *   buffer to the "corrected" history block;
     * - if the source image input is from a file, add its filename;
     * - add the history information from the input image header;
     * - write the corrected image and all its history information to output.
     *
     * If not successful, print an error message and set the return value of
     * the routine to indicate an error.
     */
    if(correct_image(cor_im,src_im) == 0) {
      edf_history_copy(*(typestr + CORTYP),histargs);
      if(srcfile) {
	strcpy(tmpbuf2,tmpnam1);
	edf_history_argv(*(typestr + CORTYP),basename(tmpbuf2));
      }
      if(!edf_history_read_header(*(typestr + HD_TYP),*(typestr + CORTYP),&err,
	&status) || status != status_success) {
	prmsg(ERROR,("error reading history from header data\n"));
	iret |= SPD_ERRFLG;
      }
      if(put_buffer(outfile,&cor_im,rows,cols,CORTYP) != 0)
	iret |= SPD_ERRFLG;
      else if(outfile != NULL) {
	iret |= SPD_CORFIL;
	if(srcid != -1)
	  iret |= SPD_BASUSE;
      }

      /*
       * Azimuthal regrouping and averaging.
       */
      if(azimint == 1) {

	/*
	 * Transfer the dummy values from the mask file to the input file for
	 * the azimuthal calculations.
	 */
	if(maskfile != NULL)
	  mark_overflow_nocorr(mask_im,cor_im,NULL,MSKTYP);

	/*
	 * Do averaging over one or two angle ranges as requested. The second
	 * averaging goes into the same buffer with an offset of 4 rows.
	 */
	if((azim_int(cor_im,azim_im,ave_im,azim_r0,azim_rnum,
          azim_a0*NUM_PI/180.0, azim_da*NUM_PI/180.0,
	  azim_anum,azim_pro,ave_scf,verbose) != 0) || (azim_a1fl == 1 &&
	  azim_int(cor_im,azim_im,ave_im + 4 * azim_rnum * sizeof(float),
	  azim_r0,azim_rnum,azim_a1*NUM_PI/180.0,azim_da*NUM_PI/180.0,
          azim_anum,azim_pro,ave_scf,verbose)!=0)){
	  /*
	   * Error for azimuthal regrouping and averaging.
	   */
	  iret |= SPD_ERRFLG;
	  prmsg(ERROR,("regrouping routine failed\n"));
	} else {
	  edf_history_copy(*(typestr + AZITYP),*(typestr + CORTYP));
	  edf_add_header_element(*(typestr + AZITYP),"AxisType_2","Angle",&err,
	    &status);
	  outfile = NULL;
	  if(*azim_ext != '\0' && (outfile = outname(tmpnam1,outdir,src_ext,
	    azim_ext)) == NULL)
	    iret |= SPD_ERRFLG;
	  if(put_buffer(outfile,&azim_im,azim_anum,azim_rnum,AZITYP) != 0)
	    iret |= SPD_ERRFLG;
	  else if(outfile != NULL) {
	    iret |= SPD_AZIFIL;
	    if(srcid != -1)
	      iret |= SPD_BASUSE;
	  }
	  outfile = NULL;
	  if(*ave_ext != '\0' && (outfile = outname(tmpnam1,outdir,src_ext,
	    ave_ext)) == NULL)
	    iret |= SPD_ERRFLG;
	  if(put_buffer(outfile,&ave_im,rowtmp,azim_rnum,AVETYP) != 0)
	    iret |= SPD_ERRFLG;
	  else if(outfile != NULL) {
	    iret |= SPD_AVEFIL;
	    if(srcid != -1)
	      iret |= SPD_BASUSE;
	  }
	}
      }
    } else {
      iret |= SPD_ERRFLG;
      prmsg(ERROR,("correction routines failed\n"));
    }

    /*
     * End of processing for this input image.
     *
     * If there are no more file names on the input line, then the processing
     * stops here ("lcont" is false).
     *
     * If there are still file names on the input line, read the next image.
     * If this results in an error (get_buffer() < 0), then skip this file and
     * continue with the remaining ones (while there are any left).
     *
     * If a file has been successfully read, then process it. If this is a newly
     * acquired buffer (get_buffer() > 0), then it may need to be mapped first.
     * If mapping fails, skip this file and continue with the remaining ones.
     */
    while(lcont = fnampat(srcbuf,sizeof(srcbuf),argc - argn,argv + argn) == 0) {
      if((gbufstat = get_buffer(-1,srcfile,&src_im,&rows,&cols,SRCTYP)) < 0)
	continue;
      if(gbufstat > 0) {
	if(map_imag(src_im,&pnewbuf,(double)bin_1,(double)bin_2,SRCTYP) < 0)
	  continue;
	if(src_im != pnewbuf) {
	  clean_buffer(&src_im,SRCTYP,1);
	  src_im = pnewbuf;
	  get_headval(&user_head,SRCTYP);
	  user_head.Dim_1 = cols /= bin_1;
	  user_head.Dim_2 = rows /= bin_2;
	  set_headval(user_head,SRCTYP);
	}
	set_xysize(cols,rows);
      } // if(gbufstat > 0)

      /*
       * Get a buffer for the corrected output image.
       */
      if(get_buffer(-1,NULL,&cor_im,&rows,&cols,CORTYP) == -1)
	goto analyse_args_cleanup;

      /*
       * Get the image file name for the dark image or flood file correction
       * from the keyword in the source file header, if requested.
       *
       * Then test if the corresponding buffer needs to be re-allocated. This
       * could be the case due to a different binning of the source image, even
       * if the dark or flood file has not changed.
       *
       * If any of this fails, continue with the next source image.
       */
      if(get_filnam(&darkfile,darkbuf,darktmp,DRKTYP) < 0)
	continue;
      /*
       * If the dark file name does not contain a directory path, use the one
       * from the source image file name.
       */
      if(darkfile != NULL && strchr(darkfile,'/') == NULL) {
        if((tmpnam1 = strrchr(srcfile,'/')) != NULL)
        {
          *tmpbuf1 = '\0';
          strncat(tmpbuf1,srcfile,tmpnam1 - srcfile + 1);
          strcat(tmpbuf1,darkfile);
          strcpy(darkfile,tmpbuf1);
        }
      } // if(darkfile ...
      if((gbufstat = get_buffer(-1,darkfile,&dark_im,&rows,&cols,DRKTYP)) < 0)
	continue;
      if(gbufstat == 1)
	set_imgbuf(dark_im,DRKTYP);

      if(get_filnam(&flofile,flobuf,flotmp,FLOTYP) < 0)
	continue;
      if((gbufstat = get_buffer(-1,flofile,&flo_im,&rows,&cols,FLOTYP)) < 0)
	continue;
      if(gbufstat == 1)
	set_imgbuf(flo_im,FLOTYP);

      /*
       * Test if the scattering background image buffer needs to be
       * re-allocated (same argument as for dark and flood file above).
       */
      if((gbufstat = get_buffer(-1,bckgfile,&bckg_im,&rows,&cols,SBKTYP)) < 0)
	continue;
      if(gbufstat == 1)
	set_imgbuf(bckg_im,SBKTYP);

      /*
       * Get (if required) the filename for the spline distortion coefficients
       * from a keyword in the source file header and hand it to the correction
       * routines.
       *
       * If this fails, continue with the next source image.
       */
      if(do_dist) {
	if(get_filnam(&distfile,distbuf,tmpbuf1,DISTYP) < 0)
	  continue;
	set_splinfil(distfile);
      } // if(do_dist)

      /*
       * All requested files obtained for this image, do the correction.
       */
      break;
    }
  } while(lcont);

analyse_args_cleanup:
  /*
   * We detach from shared memory here.
   *
   * In addition, the buffers will be freed for all output data types.
   */
  if(gbufstat == -1)
    iret |= SPD_ERRFLG;
  clean_buffer(&src_im,SRCTYP,0);
  clean_buffer(&cor_im,CORTYP,1);
  clean_buffer(&dark_im,DRKTYP,0);
  clean_buffer(&flo_im,FLOTYP,0);
  clean_buffer(&bckg_im,SBKTYP,0);
  clean_buffer(&head_buf,HD_TYP,0);
  clean_buffer(&azim_im,AZITYP,1);
  clean_buffer(&ave_im,AVETYP,1);
  clean_buffer(&mask_im,MSKTYP,0);

  print_memsize();

  ihistnew = 1;

analyse_args_return:

  if((iret & SPD_ERRFLG) != 0) {
    errno = 0;
    prmsg(ERROR,("image processing did not succeed\n"));
  }

  prmsg(DMSG,("analyse_args return status is %#x\n",iret));

  return(iret);

} /* analyse_args */

/*==============================================================================
 * Print out a help text describing the command line options of the program.
 *
 * Input:  none
 * Output: none
 * Return: none
 */

void help_arg(void)
{
#define PRNTHELP(x) prmsg(MSG,("    " x "\n"))
#define PRNTHLPN(x) prmsg(MSG,("\n    " x "\n"))

  PRNTHELP("src_id=<source image shared memory id>");
  PRNTHELP("src_ext=<source image file extension> (default none)");

  PRNTHLPN("cor_id=<shared memory id of corrected image>");
  PRNTHELP("cor_ext=<corrected image file extension> (default: \".cor.edf\")");

  PRNTHLPN("type=<output data type> (default \"FloatIEEE32\")");
  PRNTHELP("dvo=<value> data value offset (default 0)");

  PRNTHLPN("dark_id=<dark image shared memory id>");
  PRNTHELP("dark_file=<dark image file name>");
  PRNTHELP("dark_const=<value> subtract constant dark image value");
  PRNTHELP("    (default: no dark image subtraction done)");
  PRNTHELP("dark_ext=<dark image file extension> (default none)");
  PRNTHELP("do_dark=0|1 if 0, suppress dark image correction (default 0)");
  PRNTHELP("save_dark=0|1|2 save dark image memory to file");
  PRNTHELP("    0: never, 1: always, 2: only if new (default 2)");
  PRNTHELP("inp_const=<value> add input image constant (default 0.)");
  PRNTHELP("inp_exp=<value> apply exponent to input image (default 1.)");
  PRNTHELP("inp_factor=<value> multiply with input image factor (default 1.)");
  PRNTHELP("raw_cmpr=\"none\"|\"gzip\"|\"z\" compression of raw & dark images (default none) ");

  PRNTHLPN("flood_id=<flood field image shared memory id>");
  PRNTHELP("flood_file=<flood field image file name>");
  PRNTHELP("    (default: no flood field used)");

  PRNTHLPN("bckg_id=<scattering background image shared memory id>");
  PRNTHELP("bckg_file=<scattering background image file name>");
  PRNTHELP("    (default: no scattering background used)");
  PRNTHELP("bckg_const=<additive scattering constant> (default 0.)");
  PRNTHELP("bckg_fact=<constant scattering factor> (default 1.)");

  PRNTHLPN("header_id=<shared memory id for data header> (default: not used)");
  PRNTHELP("pass=0|1 pass input file header to output (default 0)");
  PRNTHELP("header_min=<minimum header length for output file> (default 0)");
  PRNTHELP("header_ext=<extension for header output file> (default none)");

  PRNTHLPN("distortion_file=<file> (default \"spatial.dat\")");
  PRNTHELP("xfile=<file> (x distortion read from edf file)");
  PRNTHELP("yfile=<file> (y distortion read from edf file)");
  PRNTHELP("xoutfile=<file> (x distortion saved to edf file)");
  PRNTHELP("youtfile=<file> (y distortion saved to edf file)");
  PRNTHELP("active_radius=<value> (values outside will not be corrected)");

  PRNTHELP("precen_1=xxx for pre-rotation center_1 (default calculated)");
  PRNTHELP("precen_2=xxx for pre-rotation center_2 (default calculated)");
  PRNTHELP("predis=xxx for pre-rotation distance (default calculated)");
  PRNTHELP("prerot_1=<angle (rad) for pre-rotation around axis 1> (default 0.)");
  PRNTHELP("prerot_2=<angle (rad) for pre-rotation around axis 2> (default 0.)");
  PRNTHELP("prerot_3=<angle (rad) for pre-rotation around axis 3> (default 0.)");

  PRNTHLPN("psize_distort=0|1|2 take image params from distortion file");
  PRNTHELP("    0: none, 1: pix, 2: pix, cen, dis, proj, rot (default 0)");
  PRNTHELP("cen_1=xxx if set, defines Center_1 header value");
  PRNTHELP("cen_2=xxx if set, defines Center_2 header value");
  PRNTHELP("i0=xxx if set, defines Intensity0 header value");
  PRNTHELP("i1=xxx if set, defines Intensity1 header value");
  PRNTHELP("off_1=xxx if set, defines Offset_1 header value");
  PRNTHELP("off_2=xxx if set, defines Offset_2 header value");
  PRNTHELP("pix_1=xxx if set, defines PSize_1 header value");
  PRNTHELP("pix_2=xxx if set, defines PSize_2 header value");
  PRNTHELP("bis_1=xxx if set, defines BSize_1 header value");
  PRNTHELP("bis_2=xxx if set, defines BSize_2 header value");
  PRNTHELP("dis=xxx if set, defines SampleDistance header value");
  PRNTHELP("ori=xxx if set, defines RasterOrientation header value");
  PRNTHELP("tit=xxx if set, defines Title header value");
  PRNTHELP("wvl=xxx if set, defines WaveLength header value");
  PRNTHELP("pro=\"Saxs\"|\"Waxs\" projection type of image (default Saxs)");
  PRNTHELP("rot_1=<angle (rad) for detector rotation plane 1> (default 0.)");
  PRNTHELP("rot_2=<angle (rad) for detector rotation plane 2> (default 0.)");
  PRNTHELP("rot_3=<angle (rad) for detector rotation plane 3> (default 0.)");

  PRNTHLPN("base_name=<name> (default \"image\")");
  PRNTHLPN("outdir=<pathname> directory for output files (default: base_name)");
  PRNTHELP("verbose=-1|0|1|2 message printing level (low -> high, default 1)");
  PRNTHELP("version=0|1 print version string of the program if != 0");
  PRNTHELP("simul=0|1 (default 0)");
  PRNTHELP("do_distortion=0|1|2|3|4 distortion correction (default 1)");
  PRNTHELP("    (0: none, 1: after dark, 2: after flat, 3: after norm, 4: after background)");
  PRNTHELP("flat_distortion=0|1 normalize to flat image (default 1)");
  PRNTHELP("do_prerotation=0|1|2 pre-rotation correction (default 0)");
  PRNTHELP("    (0: none, 1: after, 2: without distortion correction)");
  PRNTHELP("norm_prerotation=0|1 renormalization to spherical angle (default 1)");

  PRNTHELP("norm_int=0|1|2|3 intensity normalization (default 0)");
  PRNTHELP("    (0: none, 1: full, 2: to Intensity1, 3: to spherical angle");

  PRNTHELP("norm_factor=<intensity normalization factor> (default 1.)");

  PRNTHLPN("overflow=xxx (default 0 = not set)");
  PRNTHELP("dummy=<value> (default 0. = not set)");
  PRNTHELP("inp_min=<value> (default 0. = not set)");
  PRNTHELP("inp_max=<value> (default 0. = not set)");

  PRNTHELP("bin_1=<value> factor for x-binning (default 1 = no binning)");
  PRNTHELP("bin_2=<value> factor for y-binning (default 1 = no binning)");

  PRNTHLPN("azim_int=0|1 azimuthal regrouping (default 0 = no)");
  PRNTHLPN
    ("azim_pass=0|1 pass full header to azimuthal regrouping (def. 1 = yes)");
  PRNTHELP
    ("azim_pro=\"Saxs\"|\"Waxs\" project. type of azim. regrp. (default Saxs)");
 
  PRNTHELP("azim_id=<shared memory id of regrouped image> (default -1)");
  PRNTHELP("azim_ext=<file extension of regrouped image> (default: \".azim.edf\')");
  PRNTHELP("azim_r0=<value> minimum regrouping radius (default 0.)");
  PRNTHELP("azim_r_num=<value> radial output size (default 0)");
  PRNTHELP("azim_a0=<degrees> 1st regrouping start angle (default 0.)");
  PRNTHELP("azim_a1=<degrees> 2nd regrouping start angle (default: not used)");
  PRNTHELP("azim_da=<degrees> angular regrouping interval (default 1.)");
  PRNTHELP("azim_a_num=<value> angular output size (default 0)");
  PRNTHELP("ave_id=<shared memory id of averaged image> (default -1)");
  PRNTHELP("ave_ext=<file extension of averaged image> (default none)");
  PRNTHELP("ave_scf=<scale factor for \"s\" values> (default 1.)");
  PRNTHELP("mask_file=<azimuthal regrouping mask file name>");
  PRNTHELP("    (default: not used)");
  PRNTHLPN("clear=0|1 reset all command options (default 0)");

  PRNTHLPN("--server switch to server mode, i.e. wait for");
  PRNTHELP("    new command/image to process on stdin");
  PRNTHLPN("--exit quit the program when we are on server mode.");
}

/*==============================================================================
 * Write header and data to an output data file.
 *
 * The data contained in the input argument buffer "data" is written to the
 * output file with the name "fname". The raw data is preceded by the data file
 * header.
 *
 * The header always contains at least the following keywords: EDF_DataBlockID,
 * EDF_BinarySize, HeaderID, ByteOrder, DataType, Dim_1, Dim_2.
 * Additional keywords may be contained in the input string "header", and if
 * so, will be written to the data file header as well.
 *
 * If any of the following operations fails, the routine will return an error:
 * - the output file cannot be opened;
 * - there is an error adding the additional keywords from the "header" input
 *   argument to the output data file;
 * - there is an error during the writing of the data.
 *
 *
 * Input : fname: name of the output data file
 *         data:  buffer containing the data to be written to the output file
 *         rows:  number of elements in the second dimension of the image (this
 *                is the "slow-moving" index, i.e. the first index in a two-
 *                dimensional C data buffer)
 *         cols:  number of elements in the first dimension of the image (this
 *                is the "fast-moving" index, i.e. the second index in a two-
 *                dimensional C data buffer)
 *         type : type of file to be written (source, flood field, ...)
 *         mtype: type of data to be written. For possible values, see the enum
 *                variable "MType" in "edfio.h"
 * Output: none
 * Return: -1  if error
 *          0  else
 */

int save_esrf_file(char *fname,void *data,int rows,int cols,int type,int mtype)
{
  int err,stream,status;
  long DataNumber = 1;
  long dim[3];

  /*
   * Create a new output file or, if it already exists, overwrite it. Then open
   * the file.
   *
   * Return with error if this fails.
   */

  if((stream = edf_open_data_file(fname,"new",&err,&status)) == -1) {
    prmsg(ERROR,("Error opening file \"%s\".\n",fname));
    return(-1);
  }

  /*
   * Write the header and the data to the data file.
   *
   * The following keywords are automatically written by edf_write_data():
   *
   * - EDF_DataBlockID (and the old keyword Image, which is equivalent to the
   *   "sequence" part of EDF_DataBlockID)
   * - EDF_BinarySize (and the old equivalent keyword Size)
   * - EDF_HeaderSize
   * - the old keyword HeaderID
   * - ByteOrder
   * - DataType
   * - Dim_1
   * - Dim_2
   * - Compression
   * - Image
   *
   * Additional keywords may be contained in the internal header buffer. This
   * is filled from the header shared memory for online source images or from
   * the file header for input source files. It also contains the history
   * keywords describing the actions that this correction program has performed
   * with the data.
   *
   * The internal header buffer will be written here. Return with error if this
   * fails.
   */

  if(!edf_write_header(stream,DataNumber,1,*(typestr + type),&err,&status) &&
    err != CouldNotFindKeyword) {
    prmsg(ERROR,("Error obtaining %s data header from internal buffer.\n",
      *(typestr + type)));
    goto save_esrf_file_error;
  }

  /*
   * Write the data. Return with error if this fails.
   */
  if(data != NULL) {
    dim[0] = 2;
    dim[1] = cols;
    dim[2] = rows;
    if( ((type == SRCTYP) || (type == DRKTYP)) ) {
      edf_set_datacompression(raw_cmpr);
      if (raw_cmpr!=UnCompressed)
        prmsg(DMSG,("Writing %s to file \"%s\" with compression %s.\n",
          *(typestr + type),fname,edf_compression2string(raw_cmpr)));
    }
    edf_write_data(stream,DataNumber,1,dim,data,mtype,&err,&status);
    edf_set_datacompression(UnCompressed);
    if(status) {
      prmsg(ERROR,("Error writing image data to file: \"%s\".\n",fname));
      goto save_esrf_file_error;
    }
  }

  edf_close_data_file(stream,&err,&status);
  return(0);

save_esrf_file_error:
  prmsg(ERROR,("Error producing output data file: \"%s\".\n",fname));
  edf_close_data_file(stream,&err,&status);
  return(-1);
} /* save_esrf_file */

/*==============================================================================
 * Read header and data of an input data file.
 *
 * The data file header is read to obtain the information on the number of
 * rows and columns of the input image, the total size in bytes and the byte
 * ordering scheme used when storing the data.
 *
 * The dimensions of the buffer ("rows" and "columns") are set from the
 * dimensions of the input image.
 *
 * Then the image data are read from the input file into a data buffer that is
 * returned to the calling program. If a buffer of suitable size is provided in
 * the input arguments, it is used, else a buffer will be allocated.
 *
 * If any of these operations fails, the routine will return an error
 * indication and the "err" variable will be set to reflect the error that
 * occurred.
 *
 * Input : fname: name of the input data file
 *         type : type of file to be read (source, flood field, ...)
 *         mtype: requested type of data to be read. For possible values, see
 *                the enum variable "MType" in "edfio.h"
 * Output: data: new buffer containing the data read from the input data file
 *         rows: number of elements in the second dimension of the image (this
 *               is the "slow-moving" index, i.e. the first index in a two-
 *               dimensional C data buffer)
 *         cols: number of elements in the first dimension of the image (this is
 *               the "fast-moving" index, i.e. the second index in a two-
 *               dimensional C data buffer)
 *         err:  error indicator. Only meaningful if there is an error. Values:
 *               -  1 if the data header could not be obtained correctly
 *               -  3 if the data record could not be obtained correctly
 * Return: -1  if error
 *          0  else
 */

int read_esrf_file(char *fname,void **data,int *rows,int *cols,int type,
  int mtype,int *err)
{
  static char bytebuf[EdfMaxLinLen + 1];

  char *ptypestr = *(typestr + type);
  int stream,status;
  long *dim = NULL;
  long MinNum,MaxNum;
  size_t datasize;
  struct data_head user_head;

  /*
   * Open data file and read the file header. Return with error if this fails.
   */
  if((stream = edf_open_data_file(fname,"read",err,&status)) == -1) {
    prmsg(ERROR,("Error opening file \"%s\".\n",fname));
    return(-1);
  }

  edf_search_minmax_number(stream,1,&MinNum,&MaxNum,err,&status);

  /*
   * Get the values for a certain number of keywords from the header.
   *
   * The values for the following keywords are required:
   *
   * - DIM_1      number of elements (of type DATATYPE) in the first dimension
   *              of the input data array (= number of columns)
   * - DIM_2      number of elements (of type DATATYPE) in the second dimension
   *              of the input data array (= number of rows)
   *
   * If the keywords cannot be found or the values not be read, return an error.
   *
   */

  /*
   * This call gets the values of DIM_1 and DIM_2 and returns them in the array
   * elements dim[1] and dim[2], and it gets the total size in byte of the data
   * to be read.
   */
  edf_read_data_dimension(stream,MinNum,1,&dim,&datasize,err,&status);
  if(status != status_success)
  {
    goto badheader;
  }

  if(!edf_read_header_line(stream,MinNum,1,"DATATYPE",bytebuf,err,&status) ||
    status != status_success)
    goto badheader;
  datasize = datasize * edf_machine_sizeof(mtype) /
    edf_data_sizeof(edf_string2datatype(bytebuf));

  /*
   * Clear the internal header and history buffers and then read the entire file
   * header and history of the input file into them.
   *
   * For the source type image, also put the file header and history into the
   * internal buffers of type HD_TYP. This is to make it analogous to the
   * processing of the "spec" online header, which is always put in the HD_TYP
   * buffer. They will then be written to the corrected output image file.
   */
  if(!edf_new_header(ptypestr) || !edf_read_header(stream,MinNum,1,ptypestr,err,
    &status) || status != status_success ||
    !edf_history_new(ptypestr) || !edf_read_header_history(stream,MinNum,1,
    ptypestr,err,&status) || status != status_success)
    goto badheader;

  if(type == SRCTYP) {
    if(!edf_new_header(*(typestr + HD_TYP)) || !edf_read_header(stream,MinNum,1,
      *(typestr + HD_TYP),err,&status) || status != status_success ||
      !edf_history_new(*(typestr + HD_TYP)) || !edf_read_header_history(stream,
      MinNum,1,*(typestr + HD_TYP),err,&status) || status != status_success)
      goto badheader;
  }

  /*
   * Search the header for the values to be put in the header structure for the
   * image "type".
   */
  scanhead(type,&user_head);

  /*
   * Transfer header structure to the correction routines.
   *
   * If a particular keyword was found and the value is valid, then it sets the
   * corresponding value in the global variables. If not, then the previous
   * value of the global variable is kept. This is not an error condition.
   */
  set_headval(user_head,type);

  /*
   * Set the "dummy" value for the output image from the input source image, if
   * "dummy" is defined there.
   */
  if(type == SRCTYP && user_head.init & FL_DUMMY)
    set_dummy(user_head.Dummy);

  /*
   * If the output data buffer is NULL, allocate a data buffer with the required
   * size as determined in the call to edf_read_data_dimension() above.
   *
   * Then read the data.
   *
   * Return with error if any of these operations fails.
   */
  if(*data == NULL && (*data = (void *)pmalloc(datasize)) == NULL) {
    prmsg(ERROR,("Cannot allocate %d bytes\n",datasize));
    *err = 3;
    edf_close_data_file(stream,err,&status);
    return(-1);
  }

  edf_read_data(stream,MinNum,1,&dim,&datasize,data,mtype,err,&status);
  if(status) {
    *err = 3;
    prmsg(ERROR,("Error reading file \"%s\".\n",fname));
    edf_close_data_file(stream,err,&status);
    return(-1);
  }

  *cols = dim[1];
  *rows = dim[2];

  edf_close_data_file(stream,err,&status);

  return(0);

  /*
   * Error return in case of problems with the header.
   */
badheader:
    *err = 1;
    edf_close_data_file(stream,err,&status);
    return(-1);
}
